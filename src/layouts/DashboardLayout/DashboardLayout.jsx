import React, { useEffect, useRef } from "react";

import { makeStyles } from "@material-ui/styles";

import BaseLayout from "../BaseLayout";
import UserSidebar from "../../components/UserSidebar";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";

const useStyles = makeStyles({
  button: {
    position: "fixed",
    right: 5,
    top: 60,
    width: 45,
    height: 45,
    zIndex: 1
  }
});

const DashboardLayout = ({ children }) => {
  const classes = useStyles();

  const node = useRef(null);

  const openUserSidebar = () => {
    node.current.style.width = "280px";
  };

  const closeUserSidebar = () => {
    node.current.style.width = "0";
  };

  const handleClick = (e) => {
    if (!node.current.contains(e.target)) closeUserSidebar();
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClick, false);
    return () => {
      document.removeEventListener("mousedown", handleClick, false);
    };
  });
  return (
    <BaseLayout>
      {/* open sidenav btn */}
      <button
        className={`${classes.button} btn text-muted`}
        onClick={openUserSidebar}
      >
        <FontAwesomeIcon icon={faBars} size="2x" />
      </button>

      {/* user sidebar */}
      <UserSidebar closeUserSidebar={closeUserSidebar} refProp={node} />
      {children}
    </BaseLayout>
  );
};

export default DashboardLayout;
