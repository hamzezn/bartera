import React from "react";
import { makeStyles } from "@material-ui/styles";

// components
import Container from "react-bootstrap/Container";
import Navbar from "../../components/Navbar";
import Footer from "../../components/Footer";

// styles
const useStyles = makeStyles({
  root: {
    height: "100%",
    padding: 0,
    margin: 0
  },
  main: {
    width: "100%",
    paddingTop: "3.5rem",
    minHeight: "90vh"
  }
});

const BaseLayout = ({ children }) => {
  const classes = useStyles();
  return (
    <Container fluid className={classes.root}>
      <Navbar />
      <main className={classes.main}>{children}</main>
      <Footer />
    </Container>
  );
};

export default BaseLayout;
