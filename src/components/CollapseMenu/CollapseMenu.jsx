import React, { useState } from "react";

import { makeStyles } from "@material-ui/styles";

// components
import Accordion from "react-bootstrap/Accordion";
import Card from "react-bootstrap/Card";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";

const useStyles = makeStyles({
  card: {
    borderRadius: 0
  },
  cardBody: {
    padding: ".8rem"
  },
  cardHeader: {
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    cursor: "pointer",
    "& h4": {
      color: "#555",
      fontSize: "1rem",
      lineHeight: "0.2",
      userSelect: "none"
    },
    "&.open > svg": {
      transform: "rotate(180deg)"
    }
  },
  arrow: {
    fontSize: 18,
    transition: "all .3s"
  }
});

const CollapseMenu = ({ title, children }) => {
  const classes = useStyles();
  const [openMenu, setOpenMenu] = useState(true);

  const toggleMenu = () => {
    setOpenMenu(!openMenu);
  };
  return (
    <Accordion className="mt-2" defaultActiveKey="0">
      <Card className={classes.card}>
        <Accordion.Toggle
          as={Card.Header}
          className={`${classes.cardHeader} ${openMenu ? "open" : ""}`}
          eventKey="0"
          onClick={toggleMenu}
        >
          <h4>{title}</h4>
          <FontAwesomeIcon icon={faChevronDown} className={classes.arrow} />
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="0">
          <Card.Body className={classes.cardBody}>{children}</Card.Body>
        </Accordion.Collapse>
      </Card>
    </Accordion>
  );
};

export default CollapseMenu;
