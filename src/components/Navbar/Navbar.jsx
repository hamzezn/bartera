import React, { useEffect, useState } from "react";

import { makeStyles } from "@material-ui/styles";
import { NavLink, Link } from "react-router-dom";

// Redux
import { connect } from "react-redux";
import * as actions from "../../store/actions";

// components
import Navbar from "react-bootstrap/Navbar";
import Badge from "react-bootstrap/Badge";
import Nav from "react-bootstrap/Nav";
import Button from "react-bootstrap/Button";
import Dropdown from "react-bootstrap/Dropdown";

// images
import batraIcon from "../../img/BatraLogo.svg";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBell, faBars } from "@fortawesome/free-solid-svg-icons";

const useStyles = makeStyles({
  brand: {
    marginRight: 0,
    paddingBottom: 0,
    paddingTop: 0,
    "& > img": {
      height: 40
    }
  },
  navbar: {
    padding: ".6rem 1rem"
  },
  toggle: {
    position: "relative"
  },
  list: {
    marginLeft: "auto",
    fontSize: ".9rem",
    marginRight: 80,
    textAlign: "right",
    "& > li > a:hover": {
      color: "#007600 !important"
    }
  },
  buttonsBox: {
    display: "flex",
    flexFlow: "row wrap",
    alignItems: "center"
  },
  categoryBtn: {
    backgroundColor: "#159e15",
    padding: "8px 10px",
    fontSize: ".8rem"
  },
  loginBtn: {
    backgroundColor: "#fff",
    padding: "8px 10px",
    fontSize: ".8rem",
    color: "#ff4500",
    border: "1px solid #ff4500"
  },
  userBtn: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    fontSize: ".8rem",
    border: "1px solid #ff4500",
    borderRadius: 5,
    color: "#616161",
    backgroundColor: "#fff",
    padding: "8px 10px",
    position: "relative",
    "&:hover": {
      color: "#159e15",
      backgroundColor: "#fff",
      borderColor: "#ff4500"
    }
  },
  dropDownMenu: {
    paddingBottom: 0,
    textAlign: "right",
    display: "block",
    visibility: "hidden",
    opacity: 0,
    transform: "translateY(30px)",
    transition: ".4s ease all",
    "& a": {
      fontSize: ".9rem",
      color: "#616161",
      paddingRight: "1rem",
      paddingLeft: "2.5rem"
    },
    "&.show": {
      display: "block",
      visibility: "visible",
      opacity: 1,
      transform: "translateY(0px)",
      transition: ".4s ease all"
    }
  },
  bellBadge: {
    position: "absolute !important",
    top: "-8px !important",
    left: "-14px",
    borderRadius: "50%",
    padding: "3px 5px"
  },
  bell: {
    animationName: "$notification-alarm",
    animationDuration: ".3s",
    animationIterationCount: "infinite",
    animationDirection: "alternate-reverse"
  },
  "@keyframes notification-alarm": {
    from: {
      transform: "rotate(-30deg)"
    },
    to: {
      transform: "rotate(30deg)"
    }
  }
});

function NavigationBar({ isAuthenticated, fullName, getBarterStatistics }) {
  const classes = useStyles();

  const [stats, setStats] = useState(null);
  const [hasNotif, setHasNotif] = useState(true);
  useEffect(() => {
    if (isAuthenticated)
      getBarterStatistics({
        onSuccess: (data) => {
          setStats(data);
          if (
            data["Number of requests pending my approval"] === 0 &&
            data["Number of orders received pending approval"] === 0 &&
            data["Unread Messages:"] === 0
          ) {
            setHasNotif(false);
          }
        }
      });
  }, [isAuthenticated, getBarterStatistics]);

  return (
    <header>
      {/* Navigation Bar */}

      <Navbar
        bg="white"
        fixed="top"
        expand="lg"
        className={`shadow-sm ${classes.navbar}`}
      >
        <Navbar.Brand as={Link} to="/" className={classes.brand}>
          <img src={batraIcon} alt="bartra-icon" className="ml-2" />
          باترا
        </Navbar.Brand>
        <Navbar.Toggle
          aria-controls="navbarSupportedContent"
          className={classes.toggle}
        >
          {hasNotif && isAuthenticated && (
            <Badge variant="secondary" className={classes.bellBadge}>
              <FontAwesomeIcon icon={faBell} className={classes.bell} />
            </Badge>
          )}

          <FontAwesomeIcon icon={faBars} />
        </Navbar.Toggle>
        <Navbar.Collapse id="navbarSupportedContent">
          <Nav as="ul" className={classes.list}>
            <Nav.Item as="li">
              <Nav.Link as={NavLink} exact to="/">
                خانه
              </Nav.Link>
            </Nav.Item>
            <Nav.Item as="li">
              <Nav.Link as={NavLink} to="/manual">
                راهنمای استفاده
              </Nav.Link>
            </Nav.Item>
            <Nav.Item as="li">
              <Nav.Link as={NavLink} to="/FAQ">
                سوالات متداول
              </Nav.Link>
            </Nav.Item>
          </Nav>
          <div className={`${classes.buttonsBox} my-2 my-lg-0`}>
            <Button
              as={Link}
              to="/choose_category"
              className={`${classes.categoryBtn} my-2 my-sm-0`}
            >
              ثبت رایگان آگهی
            </Button>

            {isAuthenticated ? null : (
              <Button
                as={Link}
                to="/login_register"
                variant="secondary"
                className={`${classes.loginBtn} my-2 mr-2 my-sm-0`}
              >
                ورود / ثبت نام
              </Button>
            )}

            {!isAuthenticated ? null : (
              <Dropdown>
                <Dropdown.Toggle
                  variant="secondary"
                  className={`${classes.userBtn} my-2 mr-2 my-sm-0`}
                >
                  {fullName}
                  {hasNotif && isAuthenticated && (
                    <Badge variant="secondary" className={classes.bellBadge}>
                      <FontAwesomeIcon
                        icon={faBell}
                        size="2x"
                        className={classes.bell}
                      />
                    </Badge>
                  )}
                </Dropdown.Toggle>

                <Dropdown.Menu className={classes.dropDownMenu}>
                  <Dropdown.Item as={Link} to="/dashboard/profile">
                    پروفایل کاربری
                  </Dropdown.Item>
                  <Dropdown.Item as={Link} to="/dashboard/myRequests">
                    درخواست های من
                    {isAuthenticated && stats && (
                      <Badge
                        style={{ backgroundColor: "#68BBE3", color: "#fff" }}
                        className="mr-1"
                      >
                        {stats["Number of requests pending my approval"]}
                      </Badge>
                    )}
                  </Dropdown.Item>
                  <Dropdown.Item as={Link} to="/dashboard/received_orders">
                    سفارشات دریافتی
                    {isAuthenticated && stats && (
                      <Badge variant="secondary" className="mr-1">
                        {stats["Number of orders received pending approval"]}
                      </Badge>
                    )}
                  </Dropdown.Item>
                  <Dropdown.Item as={Link} to="/dashboard/messages">
                    پیام ها
                    {isAuthenticated && stats && (
                      <Badge variant="secondary" className="mr-1">
                        {stats["Unread Messages:"]}
                      </Badge>
                    )}
                  </Dropdown.Item>
                  <Dropdown.Divider />
                  <Dropdown.Item as={Link} to="/logout">
                    خروج
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            )}
          </div>
        </Navbar.Collapse>
      </Navbar>
      {/* End Navigation Bar  */}
    </header>
  );
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated:
      state.auth.token !== null && state.auth.refreshToken !== null,
    fullName: state.auth.fullName
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getBarterStatistics: (callback) =>
      dispatch(actions.getBarterStatistics(callback))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NavigationBar);
