import React, { useState, useEffect } from "react";

// redux
import { connect } from "react-redux";
import * as actions from "../../store/actions/index";

// components
import Form from "react-bootstrap/Form";
import { toast } from "react-toastify";

const CityChoose = (props) => {
  const [cities, setCities] = useState(null);

  props.setCity(null);

  const { getProvince } = props;
  const { handleCityChoose } = props;

  useEffect(() => {
    getProvince();
  }, [getProvince]);

  const provinceOnChange = (event) => {
    let selectedIndex;
    if (event.target) {
      selectedIndex = event.target.options.selectedIndex;
      if (selectedIndex === 0) {
        setCities(null);
        props.setCity(null);
        props.getAssets();
        return;
      }
      const cities = props.resultProvince[selectedIndex - 1].cities;
      props.setCity(cities.map((city) => city.id));
      props.getAssets();
      setCities(props.resultProvince[selectedIndex - 1].cities);
    }
  };

  const cityOnChange = (event) => {
    const selectedIndex = event.target.options.selectedIndex;
    if (handleCityChoose) {
      if (selectedIndex === 0) {
        toast.warn("یک شهر را انتخاب کنید");
        return;
      }
      handleCityChoose(cities[selectedIndex - 1].id);
      return;
    }
    if (selectedIndex === 0) {
      props.setCity(cities.map((city) => city.id));
      props.getAssets();
      return;
    }
    props.setCity(cities[selectedIndex - 1].id);
    props.getAssets();
  };

  return (
    <>
      <Form.Control
        as="select"
        custom={true ? 1 : 0}
        defaultValue="-1"
        className={props.classes}
        onChange={provinceOnChange}
      >
        <option value="-1" className="text-muted">
          همه استان ها
        </option>
        {!props.loadingProvince && props.resultProvince
          ? props.resultProvince.map((item) => {
              return (
                <option
                  value={item.name}
                  key={item.slug}
                  data-key={item.slug}
                  onClick={() => provinceOnChange(item.slug)}
                >
                  {item.name}
                </option>
              );
            })
          : null}
      </Form.Control>

      <Form.Control
        as="select"
        custom={true ? 1 : 0}
        defaultValue="-1"
        className={props.classes}
        onChange={cityOnChange}
      >
        <option value="-1" className="text-muted">
          همه شهر ها
        </option>

        {!props.loadingProvince && props.resultProvince && cities
          ? cities.map((item) => {
              return (
                <option value={item.name} key={item.slug}>
                  {item.name}
                </option>
              );
            })
          : null}
      </Form.Control>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    // Province
    loadingProvince: state.province.loading,
    errorProvince: state.province.error,
    resultProvince: state.province.result,

    // City
    city: state.asset.city
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getProvince: () => dispatch(actions.getProvinces()),
    getAssets: () => dispatch(actions.getAssets()),
    setCity: (city) => dispatch(actions.setCity(city))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CityChoose);
