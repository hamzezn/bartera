import React from "react";

import { makeStyles } from "@material-ui/styles";

// lodash debounce
import debounce from "lodash.debounce";

// components
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

// redux
import { connect } from "react-redux";
import * as actions from "../../store/actions/index";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

import { toast } from "react-toastify";

const useStyles = makeStyles({
  root: {
    position: "relative",
    marginTop: 40,
    height: 200,
    width: "calc(100% - 30px)"
  },
  img: {
    position: "absolute",
    right: "7%",
    top: "50%",
    opacity: 0.3,
    transform: "translate(-7%, -50%)",
    maxWidth: 150,
    "@media only screen and (max-width:992px)": {
      display: "none"
    }
  },
  form: {
    maxWidth: 600,
    position: "absolute",
    left: "38%",
    top: "50%",
    transform: "translate(-25%, -50%)",
    border: "1px solid #159e15",
    borderRadius: 5,
    "@media only screen and (max-width: 768px)": {
      maxWidth: "95%",
      border: 0,
      display: "flex",
      flexDirection: "column"
    },
    "@media only screen and (max-width: 992px)": {
      left: "50%",
      top: "50%",
      transform: "translate(-50%, -50%)"
    }
  },
  select: {
    height: 55,
    maxWidth: 160,
    border: 0,
    borderRadius: "0px 5px 5px 0px",
    borderLeft: 0,
    "@media only screen and (max-width: 768px)": {
      height: 45,
      maxWidth: "100%",
      borderRadius: "3px !important",
      border: "1px solid #159e15"
    }
  },
  input: {
    height: 55,
    width: 300,
    border: 0,
    borderRight: "1px solid #ccddcc",
    borderLeft: 0,
    "@media only screen and (max-width: 768px)": {
      marginTop: 3,
      height: 45,
      borderRadius: 3,
      border: "1px solid #159e15",
      width: "100%"
    }
  },
  button: {
    height: 55,
    width: 55,
    borderRadius: "5px 0px 0px 5px",
    fontSize: "1.5rem",
    "@media only screen and (max-width: 768px)": {
      marginTop: 3,
      height: 45,
      width: "100%",
      borderRadius: 3
    }
  }
});

const SearchBox = ({
  getAssets,
  setSearch,
  setCategory,
  categoryLoading,
  categoryResult,
  cat_id
}) => {
  const classes = useStyles();

  if (cat_id) {
    setCategory(cat_id);
    setSearch(null);
  } else setCategory(null);

  const onCategoryChange = (event) => {
    const selectedIndex = event.target.options.selectedIndex;
    if (selectedIndex === 0) setCategory(null);
    else setCategory(categoryResult[selectedIndex - 1].id);
    getAssets();
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    toast.success("نتایج یافت شدند.");
  };

  const handleSearchOnChange = (e) => {
    searchForTheResult(e.target.value);
  };

  const searchForTheResult = debounce((q) => {
    setSearch(q);
    getAssets();
  }, 500);

  return (
    <Container fluid className={`${classes.root} bg-white border rounded`}>
      <form
        className={`${classes.form} input-group`}
        onSubmit={handleFormSubmit}
      >
        <Form.Control
          as="select"
          className={classes.select}
          custom={true ? 1 : 0}
          defaultValue="1"
          onChange={onCategoryChange}
        >
          <option
            value="all"
            className="text-muted"
            id="default-category-selection"
          >
            انتخاب دسته‌بندی
          </option>
          {!categoryLoading && categoryResult
            ? categoryResult.map((category) => {
                return (
                  <option value={category.title} key={category.id}>
                    {category.title}
                  </option>
                );
              })
            : null}
        </Form.Control>
        <Form.Control
          type="text"
          className={classes.input}
          placeholder="نویسنده، نام کتاب و .. را وارد نمائید"
          onChange={handleSearchOnChange}
        />
        <Button className={classes.button} type="submit">
          <FontAwesomeIcon icon={faSearch} />
        </Button>
      </form>
    </Container>
  );
};

const mapStateToProps = (state) => {
  return {
    categoryLoading: state.category.loading,
    categoryResult: state.category.result,
    categoryError: state.category.error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAssets: () => dispatch(actions.getAssets()),
    setSearch: (search) => dispatch(actions.setSearch(search)),
    setCategory: (category) => dispatch(actions.setCategory(category)),
    getCategories: () => dispatch(actions.getCategories())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchBox);
