import React, { useState } from "react";
import { makeStyles } from "@material-ui/styles";

// componenets
import Accordion from "react-bootstrap/Accordion";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faMinus } from "@fortawesome/free-solid-svg-icons";

const useStyles = makeStyles({
  header: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    cursor: "pointer",
    userSelect: "none"
  },
  body: {
    paddingTop: ".5rem",
    display: "flex",
    flexDirection: "column"
  }
});

const CollapseSubMenu = ({ title, children }) => {
  const classes = useStyles();
  const [openMenu, setOpenMenu] = useState(false);

  const toggleMenu = () => {
    setOpenMenu(!openMenu);
  };

  return (
    <Accordion className="py-3">
      <Accordion.Toggle
        eventKey="5"
        as="div"
        className={classes.header}
        onClick={toggleMenu}
      >
        <span>{title}</span>
        <FontAwesomeIcon icon={openMenu ? faMinus : faPlus} />
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="5">
        <div className={classes.body}>{children}</div>
      </Accordion.Collapse>
    </Accordion>
  );
};

export default CollapseSubMenu;
