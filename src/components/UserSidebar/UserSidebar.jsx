import React, { useEffect } from "react";

import { makeStyles } from "@material-ui/styles";
import { NavLink } from "react-router-dom";

// redux
import { connect } from "react-redux";
import * as actions from "../../store/actions";

// helper
import { getPhoneNumber } from "../../helpers";

// components
import Container from "react-bootstrap/Container";
import ListGroup from "react-bootstrap/ListGroup";
import Spinner from "../Spinner";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTimes,
  faUser,
  faAd,
  faUpload,
  faDownload,
  faCommentAlt,
  faBookmark,
  faUserEdit,
  faSignOutAlt
} from "@fortawesome/free-solid-svg-icons";

// images
import avatarMale from "../../img/avatar4.png";
import avatarFemale from "../../img/avatar3.png";

const items = [
  {
    label: "پروفایل",
    address: "/dashboard/profile",
    icon: faUser
  },
  {
    label: "آگهی‌های من",
    address: "/dashboard/myAdvertisements",
    icon: faAd
  },
  {
    label: "درخواست‌های من",
    address: "/dashboard/myRequests",
    icon: faUpload
  },
  {
    label: "سفارشات دریافتی",
    address: "/dashboard/received_orders",
    icon: faDownload
  },
  {
    label: "پیام‌ها",
    address: "/dashboard/messages",
    icon: faCommentAlt
  },
  {
    label: "لیست علاقه‌مندی‌ها",
    address: "/dashboard/bookmarks",
    icon: faBookmark
  },
  {
    label: "ویرایش پروفایل",
    address: "/dashboard/edit_profile",
    icon: faUserEdit
  },
  {
    label: "خروج",
    address: "/logout",
    icon: faSignOutAlt
  }
];

const useStyles = makeStyles({
  root: {
    height: "100%",
    width: 0,
    position: "fixed",
    zIndex: 10,
    top: 0,
    right: 0,
    backgroundColor: "#31352e",
    overflowX: "hidden",
    transition: "0.3s",
    paddingTop: 80
  },
  closeBtn: {
    position: "absolute",
    right: 10,
    top: 60,
    borderRadius: "50%",
    width: 45,
    height: 45
  },
  avatar: {
    width: 100,
    height: 100,
    marginTop: 40,
    border: "2px solid #c9cfc9",
    borderRadius: "50%"
  },
  icon: {
    marginLeft: 8,
    width: 20
  },
  items: {
    backgroundColor: "#31352e",
    color: "#b2beba",
    fontSize: ".8rem",
    border: "1px solid rgba(0,0,0,.125)",
    "&:hover": {
      backgroundColor: "#222520",
      color: "#f1f1f1",
      textDecoration: "none"
    },
    "&.active": {
      backgroundColor: "#222520",
      color: "#f1f1f1",
      borderColor: "rgba(0,0,0,.125)"
    }
  }
});

const UserSidebar = ({
  closeUserSidebar,
  result,
  loading,
  getProfileInfo,
  refProp
}) => {
  const classes = useStyles();

  useEffect(() => {
    getProfileInfo();
  }, [getProfileInfo]);

  return (
    <aside className={classes.root} ref={refProp}>
      {/* close btn  */}
      <button
        className={`${classes.closeBtn} btn text-light`}
        onClick={closeUserSidebar}
      >
        <FontAwesomeIcon icon={faTimes} size="2x" />
      </button>
      {/* /close btn */}

      <Container className="text-center">
        {!loading && result ? (
          <>
            <img
              src={
                result.avatar
                  ? result.avatar
                  : result.gender === "F"
                  ? avatarFemale
                  : avatarMale
              }
              alt="avatar"
              className={classes.avatar}
            />
            <h6 className="text-light mt-2">{result.name}</h6>
            <span className="text-light small">
              {getPhoneNumber(result.phone_number)}
            </span>
          </>
        ) : (
          <Spinner animation="grow" />
        )}
      </Container>

      <ListGroup className="mt-5" variant="flush">
        {items.map((item, index) => {
          return (
            <ListGroup.Item
              key={index}
              as={NavLink}
              to={item.address}
              className={classes.items}
            >
              <div className="d-flex align-items-center">
                <FontAwesomeIcon
                  icon={item.icon}
                  size="lg"
                  className={classes.icon}
                />
                {item.label}
              </div>
            </ListGroup.Item>
          );
        })}
      </ListGroup>
    </aside>
  );
};

const mapStateToProps = (state) => {
  return {
    result: state.account.result,
    loading: state.account.loading
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getProfileInfo: () => dispatch(actions.getProfileInfo())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserSidebar);
