import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import { Link } from "react-router-dom";

// components
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Dropdown from "react-bootstrap/Dropdown";
import Tooltip from "../Tooltip";
import Card from "react-bootstrap/Card";
import Pagination from "../Pagination";
import Spinner from "../../components/Spinner";
import LoadingBox from "../../components/LoadingBox";

// redux
import { connect } from "react-redux";
import * as actions from "../../store/actions/index";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFilter } from "@fortawesome/free-solid-svg-icons";

// images
import bookmarkChecked from "../../img/bookmark-checked.png";
import bookmarkUnchecked from "../../img/bookmark-unchecked.png";
import avatarMale from "../../img/avatar4.png";
import noImage from "../../img/404.png";

const DESCENDING = "-created_time";
const ASCENDING = "created_time";

const useStyles = makeStyles({
  button: {
    backgroundColor: "#fff",
    padding: "8px 10px",
    fontSize: ".8rem",
    color: "#159e15",
    border: "1px solid #159e15",
    margin: ".5rem .5rem .5rem 0"
  },
  filterBtn: {
    display: "none",
    "@media only screen and (max-width: 992px)": {
      display: "block"
    }
  },
  card: {
    minHeight: 340,
    transition: ".3s all",
    border: "1px solid #f1f1f1",
    borderRadius: 0,
    "&:hover": {
      boxShadow: "0px 0px 10px 3px rgba(235,235,235,1)",
      zIndex: 10
    }
  },
  bookmark: {
    position: "absolute",
    left: 10,
    top: 0,
    "& > a": {
      textDecoration: "none",
      "& > img": {
        color: "#005e38",
        maxWidth: 20
      }
    }
  },
  imgContainer: {
    width: 190,
    height: 190,
    position: "relative",
    margin: "10px auto 0 auto",
    "& > a": {
      textDecoration: "none",
      "& > img": {
        maxWidth: 170,
        maxHeight: 170,
        position: "absolute",
        left: "50%",
        top: "50%",
        transform: "translate(-50%, -50%)"
      }
    }
  },
  avatar: {
    maxWidth: 40,
    maxHeight: 40,
    backgroundColor: "#fcae91",
    borderRadius: "50%"
  },
  link: {
    color: "#616161",
    textDecoration: "none"
  }
});

const Advertisements = ({
  openSidebar,
  loading,
  result,
  getAssets,
  setOrdering,
  addToFavorite,
  removeFromFavorite,
  search,
  setSearch,
  actionLoading
}) => {
  const classes = useStyles();
  const [currentPage, setCurrentPage] = useState(1);
  const [slug, setSlug] = useState(null);

  useEffect(() => {
    getAssets();
  }, [getAssets]);

  const handleOrderingOnClick = (ordering) => {
    setOrdering(ordering);
    getAssets();
  };

  const handleAddFavorite = (e, slug) => {
    e.preventDefault();
    setSlug(slug);
    addToFavorite(slug, currentPage);
  };

  const handleRemoveFavorite = (e, slug) => {
    e.preventDefault();
    setSlug(slug);
    removeFromFavorite(slug, currentPage);
  };

  let results;

  if (result && result.results) {
    results = result.results.map((result, index) => {
      return (
        <Col sm={6} md={4} xl={3} key={index} className="col-12 px-0">
          <Card className={classes.card}>
            <div className={classes.bookmark}>
              {result.is_favorite ? (
                <>
                  {actionLoading && result.slug === slug ? (
                    <Spinner />
                  ) : (
                    <Tooltip
                      id={`tooltip-${index + 1}`}
                      title="حذف از علاقه مندی ها"
                    >
                      <a
                        onClick={(e) => handleRemoveFavorite(e, result.slug)}
                        href="##"
                      >
                        <img src={bookmarkChecked} alt="bookmark-checked" />
                      </a>
                    </Tooltip>
                  )}
                </>
              ) : (
                <>
                  {actionLoading && result.slug === slug ? (
                    <Spinner />
                  ) : (
                    <Tooltip
                      id={`tooltip-${index + 1}`}
                      title="افزودن به علاقه مندی ها"
                    >
                      <a
                        onClick={(e) => handleAddFavorite(e, result.slug)}
                        href="##"
                      >
                        <img src={bookmarkUnchecked} alt="bookmark-unchecked" />
                      </a>
                    </Tooltip>
                  )}
                </>
              )}
            </div>

            <div className={classes.imgContainer}>
              <Link to={`/advertise_details/${result.slug}`}>
                <img src={result.thumbnail || noImage} alt="book" />
              </Link>
            </div>

            <Card.Body className="px-0 mx-3">
              <div className="mt-1">
                <Link
                  className={`${classes.link} small`}
                  to={`/advertise_details/${result.slug}`}
                >
                  <h6>{result.title}</h6>
                </Link>
              </div>

              <div className="d-flex align-items-center mt-4">
                <Link to={`/other_profiles/${result.owner_id}`}>
                  <img
                    src={result.owner_avatar || avatarMale}
                    alt="avatar"
                    className={classes.avatar}
                  />
                </Link>

                <Link
                  to={`/other_profiles/${result.owner_id}`}
                  className="mr-2 pt-3 small"
                  style={{ color: "#616161" }}
                >
                  {result.owner_name}
                </Link>

                <span className="mr-5 pt-3 small" style={{ color: "#616161" }}>
                  {result?.city_name}
                </span>
              </div>
            </Card.Body>
          </Card>
        </Col>
      );
    });
  }

  return (
    <Col as="section" lg={9} xl={10} className="col-12">
      {!loading && result && search ? (
        <Container>
          <Row className="justify-content-between">
            <Row className="pr-3">
              <h6 className="small ml-1">نتایج جستجو برای:</h6>
              <span className="small font-weight-bold">{search}</span>
            </Row>
            <span className="small font-weight-bold">
              {!loading && result ? `${result.count} نتیجه` : <Spinner />}
            </span>
          </Row>
        </Container>
      ) : null}

      <Container className="bg-white border">
        <Row>
          {/* filter button for mobile view  */}
          <Button
            className={[classes.button, classes.filterBtn]}
            onClick={openSidebar}
          >
            <FontAwesomeIcon icon={faFilter} />
            فیلترها
          </Button>
          {/* /filter button for mobile view  */}

          {/* sorting dropdown  */}
          <Dropdown alignRight>
            <Dropdown.Toggle variant="success" className={classes.button}>
              مرتب سازی براساس
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item
                className="text-right"
                onClick={() => handleOrderingOnClick(DESCENDING)}
              >
                جدیدترین ها
              </Dropdown.Item>
              <Dropdown.Item
                className="text-right"
                onClick={() => handleOrderingOnClick(ASCENDING)}
              >
                قدیمی ترین ها
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
          {/* /sorting dropdown  */}
        </Row>

        {/* items */}
        <Row>{!loading && result ? results : <LoadingBox />}</Row>
        {/* end items */}

        {!loading && result && (result.next || result.previous) ? (
          <Pagination
            current={currentPage}
            setCurrent={setCurrentPage}
            count={result.count}
            getData={getAssets}
          />
        ) : null}
      </Container>
    </Col>
  );
};

const mapStateToProps = (state) => {
  return {
    loading: state.asset.loading,
    result: state.asset.result,
    search: state.asset.search,
    actionLoading: state.asset.actionLoading
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAssets: (page) => dispatch(actions.getAssets(page)),
    setOrdering: (ordering) => dispatch(actions.setOrdering(ordering)),
    setSearch: (search) => dispatch(actions.setSearch(search)),
    addToFavorite: (slug, page) =>
      dispatch(actions.addAssetToFavorite(slug, page)),
    removeFromFavorite: (slug, page) =>
      dispatch(actions.removeFromFavorite(slug, page))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Advertisements);
