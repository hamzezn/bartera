import React from "react";

import { makeStyles } from "@material-ui/styles";

// components
import FormCheck from "react-bootstrap/FormCheck";

const useStyles = makeStyles({
  checkbox: {
    paddingRight: 0,
    width: "100%",
    paddingTop: 5,
    paddingBottom: 5,
    "& label": {
      fontSize: (small) => (small ? ".8rem" : "1rem"),
      cursor: "pointer",
      userSelect: "none",
      paddingRight: "1.5rem",
      width: "100%",
      paddingTop: 3,
      color: "#555",
      "&:hover": {
        color: "#159e15"
      }
    }
  }
});

const Checkbox = ({ id, small, children, onChange, ...props }) => {
  const classes = useStyles(small);
  return (
    <FormCheck custom id={id} className={classes.checkbox}>
      <FormCheck.Input id={id} onChange={(e) => onChange(e, id)} {...props} />
      <FormCheck.Label htmlFor={id}>{children}</FormCheck.Label>
    </FormCheck>
  );
};

Checkbox.defaultProps = {
  onChange: () => {
    return;
  }
};

export default Checkbox;
