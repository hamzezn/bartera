import React, { useEffect } from "react";

import { makeStyles } from "@material-ui/styles";

// components
import Checkbox from "../Checkbox";
import CollapseMenu from "../CollapseMenu";
import CollapseSubMenu from "../CollapseSubMenu";
import CityChoose from "../CityChoose";
import Col from "react-bootstrap/Col";
import Spinner from "../Spinner";

// redux
import { connect } from "react-redux";
import * as actions from "../../store/actions/index";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

const useStyles = makeStyles({
  root: {
    "@media only screen and (max-width: 992px)": {
      display: "block",
      position: "fixed",
      right: 0,
      top: 55,
      zIndex: 1000,
      maxWidth: 400,
      width: 0,
      height: "100vh",
      backgroundColor: "rgba(0,0,0,0.5)",
      paddingTop: 50,
      transition: ".3s"
    }
  },
  closeBtn: {
    display: "none",
    "@media only screen and (max-width: 992px)": {
      display: "block !important",
      position: "absolute",
      top: 10,
      left: 10,
      color: "rgb(255, 255, 255)",
      "& span": {
        paddingRight: 3
      }
    }
  },
  form: {
    position: "sticky",
    top: "70px",
    "@media only screen and (max-width: 992px)": {
      position: "static",
      overflowY: "scroll !important"
    }
  },
  select: {
    fontSize: ".8rem",
    marginBottom: ".5rem"
  }
});

const SideBarFilter = ({
  closeSidebar,
  loading,
  result,
  getCategories,
  setHasImage,
  getAssets,
  setCategory,
  cat_id
}) => {
  const classes = useStyles();
  setHasImage(null);

  if (cat_id) setCategory(cat_id);
  else setCategory(null);

  const categoryArray = [];

  useEffect(() => {
    getCategories();
  }, [getCategories]);

  const handleFormSubmit = (event) => {
    event.preventDefault();
    alert("form submitted");
  };

  const handleCheckboxChange = (e, id) => {
    setHasImage(e.target.checked ? true : null);
    getAssets();
  };

  const handleCategoryCheckBoxOnChange = (e) => {
    const id = e.target.checked && parseInt(e.target.id);
    if (id) {
      categoryArray.push(id);
    } else {
      categoryArray.splice(categoryArray.indexOf(id), 1);
    }
    setCategory(categoryArray);
    getAssets();
    return;
  };

  return (
    <Col
      as="aside"
      id="sidebarFilter"
      lg={3}
      xl={2}
      className={`${classes.root} pl-0 small pr-0`}
    >
      {/* close button */}
      <div className={classes.closeBtn} onClick={closeSidebar}>
        <div className="d-flex align-items-center justify-content-center p-1">
          <FontAwesomeIcon icon={faTimes} size="lg" />
          <span> بستن </span>
        </div>
      </div>
      {/* end close button */}
      <form
        action="#"
        method="get"
        onSubmit={handleFormSubmit}
        className={classes.form}
      >
        {/* only with image & around me  */}
        <div className="bg-white py-3 px-3 border">
          <Checkbox id="only-img" onChange={handleCheckboxChange}>
            فقط آگهی‌های عکس‌دار
          </Checkbox>
          {/* <Checkbox id="around-me">اطراف من</Checkbox> */}
        </div>
        {/* end only with image & around me  */}

        {/* categories */}
        <CollapseMenu title="دسته بندی ها">
          {!loading && result ? (
            result.map((category) => {
              return (
                <CollapseSubMenu title={category.title} key={category.id}>
                  {category.children.map((child) => {
                    return (
                      <Checkbox
                        id={child.id}
                        key={child.id}
                        onChange={handleCategoryCheckBoxOnChange}
                      >
                        {child.title}
                      </Checkbox>
                    );
                  })}
                </CollapseSubMenu>
              );
            })
          ) : (
            <Spinner />
          )}
        </CollapseMenu>
        {/* end categories */}

        {/* province and city */}
        <CollapseMenu title="استان و شهر">
          <CityChoose classes={classes.select} />
        </CollapseMenu>
        {/* end province and city */}
      </form>
    </Col>
  );
};

const mapStateToProps = (state) => {
  return {
    loading: state.category.loading,
    error: state.category.error,
    result: state.category.result
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCategories: () => dispatch(actions.getCategories()),
    setHasImage: (hasImage) => dispatch(actions.setHasImage(hasImage)),
    getAssets: (page) => dispatch(actions.getAssets()),
    setCategory: (category) => dispatch(actions.setCategory(category))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SideBarFilter);
