import React from 'react';

import { Redirect } from 'react-router-dom';

// redux
import { connect } from 'react-redux';

const RequireAuth = ({
    isAuthenticated,
    child,
}) => {

    return (
        <React.Fragment>
            {
                isAuthenticated
                    ?
                    child
                    :
                    <Redirect to="/login_register" />
            }
        </React.Fragment>
    );
};

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token && state.auth.refreshToken,
    }
};

export default connect(mapStateToProps)(RequireAuth);