import React from "react";

// redux
import { connect } from "react-redux";

// router
import { Route, Redirect } from "react-router-dom";

const PrivateRoute = ({ isAuthenticated, path, component }) => {
  return (
    <React.Fragment>
      {isAuthenticated ? (
        <Route exact path={path} component={component} />
      ) : (
        <Redirect to="/login_register" />
      )}
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.token && state.auth.refreshToken
  };
};

export default connect(mapStateToProps)(PrivateRoute);
