import React from "react";
import { makeStyles } from "@material-ui/styles";

import { Link } from "react-router-dom";

import BsBreadcrumb from "react-bootstrap/Breadcrumb";

const useStyles = makeStyles({
  root: {
    "& > ol": {
      padding: 0,
      margin: "0 -15px 5px 0",
      backgroundColor: "#F5F5F5",
      fontSize: ".8rem",
      "& li": {
        "&::before": {
          paddingRight: 0,
          paddingLeft: ".5rem"
        }
      },
      "& a": {
        textDecoration: "none"
      }
    }
  }
});

const Breadcrumb = ({ category, category_id, category_parents }) => {
  const classes = useStyles();
  return (
    <BsBreadcrumb className={classes.root}>
      <BsBreadcrumb.Item active>
        <Link to="/">خانه</Link>
      </BsBreadcrumb.Item>
      {category_parents?.map((item) => (
        <BsBreadcrumb.Item key={item.id} active>
          <Link
            to={{
              pathname: "/",
              state: {
                cat_id: item.id
              }
            }}
          >
            {item.title}
          </Link>
        </BsBreadcrumb.Item>
      ))}
      <BsBreadcrumb.Item active>
        <Link
          to={{
            pathname: "/",
            state: {
              cat_id: category_id
            }
          }}
        >
          {category}
        </Link>
      </BsBreadcrumb.Item>
    </BsBreadcrumb>
  );
};

export default Breadcrumb;
