import React, { useState, useEffect } from "react";

const Timer = ({ time = 120, handleCloseTimer }) => {
  const [counter, setCounter] = useState(time);

  useEffect(() => {
    if (counter === 0) handleCloseTimer();
    const timer =
      counter > 0 && setInterval(() => setCounter(counter - 1), 1000);
    return () => clearInterval(timer);
  }, [counter, handleCloseTimer]);

  return (
    <span className="red_orange" style={{ width: 30 }}>
      {counter}
    </span>
  );
};
export default Timer;
