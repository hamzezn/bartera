import React from "react";

import BsPagination from "react-bootstrap/Pagination";

const Pagination = ({ current, setCurrent, count, getData }) => {
  const totalPages = Math.ceil(count / 20);

  const handleFirst = () => {
    window.scrollTo(0, 0);
    getData(1);
    setCurrent(1);
  };

  const handelPrev = () => {
    window.scrollTo(0, 0);
    getData(current - 1);
    setCurrent(current - 1);
  };

  const handleNext = () => {
    window.scrollTo(0, 0);
    getData(current + 1);
    setCurrent(current + 1);
  };

  const handle2Next = () => {
    window.scrollTo(0, 0);
    getData(current + 2);
    setCurrent(current + 2);
  };

  const handleLast = () => {
    window.scrollTo(0, 0);
    getData(totalPages);
    setCurrent(totalPages);
  };

  return (
    <BsPagination className="mt-3 justify-content-center pr-0">
      <BsPagination.First
        onClick={handleFirst}
        disabled={current === 1 ? true : false}
      />
      <BsPagination.Item
        disabled={current === 1 ? true : false}
        onClick={handelPrev}
      >
        قبلی
      </BsPagination.Item>
      <BsPagination.Item active>{current}</BsPagination.Item>
      {current + 1 <= totalPages && (
        <BsPagination.Item onClick={handleNext}>
          {current + 1}
        </BsPagination.Item>
      )}
      {current + 2 <= totalPages && (
        <BsPagination.Item onClick={handle2Next}>
          {current + 2}
        </BsPagination.Item>
      )}
      <BsPagination.Item
        disabled={current === totalPages ? true : false}
        onClick={handleNext}
      >
        بعدی
      </BsPagination.Item>
      <BsPagination.Last
        onClick={handleLast}
        disabled={current === totalPages ? true : false}
      />
    </BsPagination>
  );
};

export default Pagination;
