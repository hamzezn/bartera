import React from "react";

import Spinner from "react-bootstrap/Spinner";

const wrapper = {
  width: "100%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  height: "100%",
  padding: "2rem",
  background: "#e3e3e3"
};

const LoadingBox = () => {
  return (
    <div style={wrapper}>
      <Spinner animation="grow" role="status" variant="primary">
        <span className="sr-only">Loading...</span>
      </Spinner>
      <span style={{ marginRight: ".5rem", color: "#159e15" }}>
        درحال دریافت اطلاعات
      </span>
    </div>
  );
};

export default LoadingBox;
