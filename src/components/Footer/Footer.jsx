import React from "react";

import { makeStyles } from "@material-ui/styles";
import { Link } from "react-router-dom";

// components
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faInstagram,
  faTelegramPlane,
  faTwitter
} from "@fortawesome/free-brands-svg-icons";

const useStyles = makeStyles({
  root: {
    marginTop: 50,
    backgroundColor: "#31352e",
    color: "white",
    textAlign: "right",
    "& p": {
      color: "#c9cfc9"
    },
    "& hr": {
      backgroundColor: "#6c757d"
    },
    "& li": {
      listStyle: "none",
      "& a": {
        textDecoration: "none",
        color: "#b2beba",
        lineHeight: "2rem",
        fontSize: ".9rem",
        "&:hover": {
          color: "#007600"
        }
      }
    }
  },
  socials: {
    textDecoration: "none",
    color: "#d3d2d2",
    lineHeight: "2rem",
    fontSize: ".9rem",
    "&:hover": {
      color: "#007600"
    }
  }
});

export default function Footer() {
  const classes = useStyles();
  return (
    <footer className={classes.root}>
      <Container fluid>
        <Container>
          <Row className="pt-5 pb-4">
            <Col md={4} lg={4} className="col-12">
              <h6>معرفی باترا</h6>
              <p className="small">
                باترا پلتفرم آنلاین معاوضه و تبادل دارایی ها است. بستری که افراد
                دارایی های مورد نیاز خود را با دارایی های بدون استفاده دیگران
                معاوضه می کنند . در واقع باترا یک موتور تطبیق دهنده راحت و سریعِ
                خواسته ها با دارایی هاست.{" "}
              </p>
            </Col>

            <Col
              md={3}
              lg={2}
              className="col-6 text-center text-md-right mt-4 mt-md-0"
            >
              <ul>
                <li>
                  <h6 className="small font-weight-bold text-light">باترا</h6>
                </li>
                <li>
                  <Link to="/about_us">درباره باترا</Link>
                </li>
                <li>
                  <Link to="/barter_story">داستان تهاتر</Link>
                </li>
                <li>
                  <Link to="/contact_us">تماس با ما</Link>
                </li>
              </ul>
            </Col>

            <Col
              md={2}
              lg={3}
              className="col-6 text-center text-md-right mt-4 mt-md-0"
            >
              <ul>
                <li>
                  <h6 className="small font-weight-bold text-light">
                    راهنمای کاربرای
                  </h6>
                </li>
                <li>
                  <Link to="/safe_swap">معاوضه امن</Link>
                </li>
                <li>
                  <Link to="/terms_and_conditions">شرایط و قوانین</Link>
                </li>
                <li>
                  <Link to="/privacy">حریم خصوصی</Link>
                </li>
              </ul>
            </Col>

            <Col md={3} lg={3} className="col-12 mt-3 text-center"></Col>
          </Row>
        </Container>
      </Container>
      <hr className="p-0 m-0" />
      <Container fluid>
        <Container>
          <div className="d-block text-center d-sm-flex justify-content-between align-items-center pt-3 pb-2">
            <h6 className="small text-light">
              تمامی حقوق این سامانه متعلق به باترا می‌باشد
            </h6>
            <div className="d-flex justify-content-center">
              <Link className={`${classes.socials} mx-2`} to="##">
                <FontAwesomeIcon icon={faInstagram} size="2x" />
              </Link>
              <Link className={`${classes.socials} mx-2`} to="##">
                <FontAwesomeIcon icon={faTelegramPlane} size="2x" />
              </Link>
              <Link className={`${classes.socials} mx-2`} to="##">
                <FontAwesomeIcon icon={faTwitter} size="2x" />
              </Link>
            </div>
          </div>
        </Container>
      </Container>
    </footer>
  );
}
