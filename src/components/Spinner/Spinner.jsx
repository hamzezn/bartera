import React from "react";

import BsSpinner from "react-bootstrap/Spinner";

const Spinner = ({ animation = "border", size = "sm", ...props }) => (
  <BsSpinner
    animation={animation}
    role="status"
    aria-hidden="true"
    size={size}
    {...props}
  >
    <span className="sr-only">Loading...</span>
  </BsSpinner>
);

export default Spinner;
