import React from "react";

import { makeStyles } from "@material-ui/styles";

// components
import FormCheck from "react-bootstrap/FormCheck";

const useStyles = makeStyles({
  radio: {
    paddingRight: 0,
    width: (inline) => (inline ? "auto" : "100%"),
    paddingTop: 5,
    paddingBottom: 5,
    "& label": {
      fontSize: ".8rem",
      cursor: "pointer",
      userSelect: "none",
      paddingRight: "1.5rem",
      width: "100%",
      paddingTop: 3,
      color: "#555",
      "&:hover": {
        color: "#159e15"
      }
    }
  }
});

const Radio = ({ id, inline, children, ...props }) => {
  const classes = useStyles(inline);
  return (
    <FormCheck
      type="radio"
      custom
      inline={inline}
      id={id}
      className={classes.radio}
    >
      <FormCheck.Input type="radio" {...props} />
      <FormCheck.Label htmlFor={id}>{children}</FormCheck.Label>
    </FormCheck>
  );
};

export default Radio;
