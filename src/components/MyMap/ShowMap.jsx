import React from "react";

import { Map, Marker, Popup, TileLayer } from "react-leaflet";

const styles = {
  color: "#159e15",
  fontWeight: 700,
  fontFamily: "IRANSans"
};

const ShowMap = ({ center, hasLocation }) => {
  return (
    <Map center={center} zoom={12}>
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      />
      {hasLocation && (
        <Marker position={center}>
          <Popup>
            <p style={styles}>مکان فعلی آگهی</p>
          </Popup>
        </Marker>
      )}
    </Map>
  );
};

export default ShowMap;
