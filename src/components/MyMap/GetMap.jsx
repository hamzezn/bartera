import React, { useState } from "react";

import { Map, Marker, Popup, TileLayer } from "react-leaflet";

const styles = {
  color: "#159e15",
  fontWeight: 700,
  fontFamily: "IRANSans"
};

const MyMarker = (props) => {
  const initMarker = (ref) => {
    if (ref) {
      ref.leafletElement.openPopup();
    }
  };

  return <Marker ref={initMarker} {...props} />;
};

const GetMap = ({ center, zoom, getPos, edit }) => {
  const [currentPos, setCurrentPos] = useState(edit ? center : null);

  const handleClick = (e) => {
    setCurrentPos(e.latlng);
    getPos(e.latlng);
  };

  return (
    <Map center={center} zoom={zoom} onClick={handleClick}>
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      />
      {currentPos && (
        <MyMarker position={currentPos}>
          <Popup position={currentPos}>
            <p style={styles}>مکان آگهی</p>
          </Popup>
        </MyMarker>
      )}
    </Map>
  );
};

export default GetMap;
