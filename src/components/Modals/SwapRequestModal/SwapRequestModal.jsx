import React, { useEffect, useState, useRef } from "react";

// redux
import { connect } from "react-redux";
import * as actions from "../../../store/actions";

import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Pagination from "../../Pagination";
import Spinner from "../../Spinner";

const SwapRequestModal = ({
  show,
  handleClose,
  result,
  getMyAssets,
  loading,
  requested,
  barter,
  barterLoading,
  auth
}) => {
  const [currentPage, setCurrentPage] = useState(1);
  const mySelect = useRef(null);

  useEffect(() => {
    if (auth) getMyAssets();
  }, [getMyAssets, auth]);

  const handleFormSubmit = (e) => {
    e.preventDefault();
    const data = {
      applicant_asset: mySelect.current.value.toString(),
      requested_asset: requested.toString()
    };
    barter(data, {
      onSuccess: () => {
        handleClose();
      }
    });
  };

  return (
    <Modal show={show} onHide={handleClose} size="lg">
      <Modal.Body className="p-4">
        لطفا یکی از دارایی‌های خود را جهت معاوضه انتخاب نمائید:
        <Form onSubmit={handleFormSubmit}>
          <div className="my-3 mr-2">
            <Form.Group controlId="myAds">
              <Form.Control as="select" custom={true ? 1 : 0} ref={mySelect}>
                {!loading &&
                  result &&
                  result.results.map((item, index) => (
                    <React.Fragment key={index}>
                      {item.is_active === true && item.bartered === false && (
                        <option value={item.slug}>{item.title}</option>
                      )}
                    </React.Fragment>
                  ))}
              </Form.Control>
            </Form.Group>
          </div>

          {/* pagination */}
          {!loading && result && result.count > 0 ? (
            <Pagination
              current={currentPage}
              setCurrent={setCurrentPage}
              count={result.count}
              getData={getMyAssets}
            />
          ) : null}

          <div className="d-flex justify-content-end mt-4">
            <Button
              type="button"
              variant="secondary"
              className="btn-sm mx-1"
              onClick={handleClose}
            >
              انصراف
            </Button>
            <Button
              type="submit"
              className="btn-sm mx-1"
              disabled={barterLoading}
            >
              ارسال درخواست
              {barterLoading && <Spinner />}
            </Button>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  );
};

const mapStateToProps = (state) => {
  return {
    result: state.myAsset.result,
    loading: state.myAsset.loading,
    barterLoading: state.barter.actionLoading,
    auth: state.auth.token !== null && state.auth.refreshToken !== null
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getMyAssets: (page) => dispatch(actions.getMyAssets(page)),
    barter: (data, callback) => dispatch(actions.barter(data, callback))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SwapRequestModal);
