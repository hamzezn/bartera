import React from "react";

import Modal from "react-bootstrap/Modal";

const CropModal = ({ show, handleClose, children }) => {
  return (
    <Modal show={show} onHide={handleClose} centered size="lg">
      <Modal.Body className="p-4">{children}</Modal.Body>
    </Modal>
  );
};

export default CropModal;
