import React, { useEffect } from "react";

// form validation
import { Formik } from "formik";
import * as Yup from "yup";

// redux
import { connect } from "react-redux";
import * as actions from "../../../store/actions";

import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Radio from "../../Radio";
import Spinner from "../../Spinner";

const Schema = Yup.object({
  reason: Yup.string().required("باید یک گزینه انتخاب کنید")
});

const RejectRequestModal = ({
  show,
  handleClose,
  loading,
  action,
  id,
  result,
  getReasons
}) => {
  useEffect(() => {
    getReasons();
  }, [getReasons]);

  return (
    <Modal show={show} onHide={handleClose} centered>
      <Modal.Body className="p-4">
        لطفا دلیل خود را برای رد درخواست انتخاب نمائید:
        <Formik
          validationSchema={Schema}
          initialValues={{
            reason: ""
          }}
          onSubmit={(values) => {
            action(id, parseInt(values.reason));
          }}
        >
          {({ handleSubmit, handleChange, values, errors }) => (
            <form className="mt-3" onSubmit={handleSubmit}>
              {result &&
                result.map((item, index) => (
                  <Radio
                    key={index}
                    id={`reject-reason-${index + 1}`}
                    value={item.id}
                    name="reason"
                    onChange={handleChange}
                    checked={values.reason === item.id.toString()}
                  >
                    {item.title}
                  </Radio>
                ))}

              <Form.Control.Feedback
                type="invalid"
                className="text-right"
                style={{ display: errors.reason ? "block" : "none" }}
              >
                {errors.reason}
              </Form.Control.Feedback>

              <div className="d-flex justify-content-end ml-4 mb-2">
                <Button
                  type="submit"
                  variant="success"
                  className="px-5"
                  disabled={loading}
                >
                  تائید
                  {loading && <Spinner />}
                </Button>
              </div>
            </form>
          )}
        </Formik>
      </Modal.Body>
    </Modal>
  );
};

const mapStateToProps = (state) => {
  return {
    result: state.reason.result
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getReasons: () => dispatch(actions.getReasons())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RejectRequestModal);
