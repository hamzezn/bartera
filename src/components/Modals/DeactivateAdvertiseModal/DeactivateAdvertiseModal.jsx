import React from "react";

import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Spinner from "../../Spinner";

const DeactivateAdvertiseModal = ({ show, handleClose, loading, action }) => {
  return (
    <Modal show={show} onHide={handleClose} centered>
      <Modal.Body className="p-4">
        اگر آگهی خود را به حالت معاوضه شده در بیاورید دیگر نمیتوانید آن را به
        حالت عادی برگردانید. آیا از انجام این کار اطمینان دارید؟
        <div className="d-flex justify-content-end mt-4">
          <Button
            className="mx- px-4"
            variant="outline-dark"
            onClick={handleClose}
            disabled={loading}
          >
            انصراف
          </Button>
          <Button
            variant="info"
            className="mx-1 px-4"
            disabled={loading}
            onClick={action}
          >
            مطمئنم
            {loading ? <Spinner /> : null}
          </Button>
        </div>
      </Modal.Body>
    </Modal>
  );
};

export default DeactivateAdvertiseModal;
