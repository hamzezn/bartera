import React from "react";

import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Spinner from "../../Spinner";

const RequestCancelModal = ({ show, handleClose, loading, action }) => {
  return (
    <Modal show={show} onHide={handleClose} centered>
      <Modal.Body className="p-4">
        آیا می‌خواهید درخواست خود را لغو نمائید؟
        <div className="d-flex justify-content-end mt-4">
          <Button
            className="ml-2 px-4"
            variant="outline-dark"
            onClick={handleClose}
          >
            نمی‌خواهم
          </Button>
          <Button className="mx-1 px-4" onClick={action} disabled={loading}>
            می‌خواهم
            {loading && <Spinner />}
          </Button>
        </div>
      </Modal.Body>
    </Modal>
  );
};

export default RequestCancelModal;
