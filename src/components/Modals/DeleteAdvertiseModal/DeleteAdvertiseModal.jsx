import React from "react";

import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Spinner from "../../Spinner";

const DeleteAdvertiseModal = ({ show, handleClose, action, loading }) => {
  return (
    <Modal show={show} onHide={handleClose} centered>
      <Modal.Body className="p-4">
        آیا از بابت حذف آگهی خود اطمینان دارید؟
        <div className="d-flex justify-content-end mt-4">
          <Button
            className="mx- px-4"
            variant="outline-dark"
            onClick={handleClose}
            disabled={loading}
          >
            انصراف
          </Button>
          <Button
            variant="info"
            className="mx-1 px-4"
            onClick={action}
            disabled={loading}
          >
            مطمئنم
            {loading ? <Spinner /> : null}
          </Button>
        </div>
      </Modal.Body>
    </Modal>
  );
};

export default DeleteAdvertiseModal;
