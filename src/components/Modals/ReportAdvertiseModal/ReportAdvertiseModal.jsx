import React from "react";

// form validation
import { Formik } from "formik";
import * as Yup from "yup";

// components
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Spinner from "../../Spinner";

const Schema = Yup.object({
  reason: Yup.string()
    .max(2000, "متن شما نباید بیشتر از 2000 حرف باشد")
    .required("لطفا دلیل خود را بنویسید")
});

const ReportAdvertiseModal = ({ show, handleClose, loading, action, slug }) => {
  return (
    <Modal show={show} onHide={handleClose} size="lg" centered>
      <Modal.Body className="p-4">
        دلیل خود را برای گزارش این آگهی بنویسید:
        <Formik
          validationSchema={Schema}
          initialValues={{
            reason: ""
          }}
          onSubmit={(values) => {
            action(slug, values.reason);
          }}
        >
          {({ handleSubmit, handleChange, values, errors }) => (
            <Form onSubmit={handleSubmit}>
              <Form.Group className="mt-4">
                <Form.Control
                  name="reason"
                  as="textarea"
                  rows="3"
                  value={values.reason}
                  onChange={handleChange}
                  isInvalid={!!errors.reason}
                ></Form.Control>
                <Form.Control.Feedback type="invalid" className="text-right">
                  {errors.reason}
                </Form.Control.Feedback>
              </Form.Group>

              <div className="d-flex justify-content-end mt-4">
                <Button
                  className="btn_outline_green mx-1"
                  type="button"
                  onClick={handleClose}
                >
                  انصراف
                </Button>
                <Button
                  type="submit"
                  className="btn_green mx-1"
                  disabled={loading}
                >
                  ارسال درخواست
                  {loading ? <Spinner /> : null}
                </Button>
              </div>
            </Form>
          )}
        </Formik>
      </Modal.Body>
    </Modal>
  );
};

export default ReportAdvertiseModal;
