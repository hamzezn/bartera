import React from "react";

// redux
import { connect } from "react-redux";

import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import LoadingBox from "../../LoadingBox";

const OrdersModal = ({ show, handleClose, loading, result }) => {
  let results;
  if (result?.results?.length) {
    results = (
      <>
        {" "}
        {result.results.map((item, index) => (
          <p key={index}>
            {index + 1} - {item.applicant_asset.title}
          </p>
        ))}
        <div className="d-flex justify-content-end mt-4">
          <Button
            variant="info"
            className="mx-1 px-4"
            as={Link}
            to="/dashboard/received_orders"
          >
            مشاهده همه
          </Button>
        </div>
      </>
    );
  }
  return (
    <Modal show={show} onHide={handleClose} centered>
      <Modal.Body className="p-4">
        سفارشات مربوط به این آگهی:
        {!loading && result ? (
          <>
            {results ?? (
              <div className="d-flex justify-content-center p-3">
                هیچ سفارشی موجود نیست
              </div>
            )}
          </>
        ) : (
          <LoadingBox />
        )}
      </Modal.Body>
    </Modal>
  );
};

const mapStateToProps = (state) => {
  return {
    loading: state.myAsset.actionLoading,
    result: state.myAsset.actionResult
  };
};

export default connect(mapStateToProps)(OrdersModal);
