import React from "react";

import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Spinner from "../../Spinner";

const ChatUploadModal = ({
  show,
  handleClose,
  imageForUpload,
  message,
  handleMessageChange,
  action,
  loading
}) => {
  return (
    <Modal show={show} onHide={handleClose} size="lg">
      <Modal.Body className="p-4">
        <div className="d-flex justify-content-center">
          <img
            src={imageForUpload ? URL.createObjectURL(imageForUpload) : null}
            alt="xyz"
            style={{ width: "60%", height: "auto" }}
          />
        </div>

        <Form.Control
          as="textarea"
          rows="3"
          custom={true ? 1 : 0}
          value={message}
          onChange={handleMessageChange}
          className="my-2"
          placeholder="متن پیام..."
        ></Form.Control>
        <div className="d-flex justify-content-end">
          <Button
            variant="outline-secondary"
            className="mx-1"
            onClick={handleClose}
            disabled={loading}
          >
            انصراف
          </Button>
          <Button
            variant="outline-primary"
            className="mx-1"
            onClick={action}
            disabled={loading}
          >
            ارسال
            {loading && <Spinner />}
          </Button>
        </div>
      </Modal.Body>
    </Modal>
  );
};

export default ChatUploadModal;
