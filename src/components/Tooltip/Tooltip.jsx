import React from "react";

import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import BsTooltip from "react-bootstrap/Tooltip";

const Tooltip = ({ placement = "top", id, title, children }) => {
  return (
    <OverlayTrigger
      placement={placement}
      overlay={<BsTooltip id={id}>{title}</BsTooltip>}
    >
      {children}
    </OverlayTrigger>
  );
};

export default Tooltip;
