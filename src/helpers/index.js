// get lat & lng from server and pass it to map component
export const getLocation = (str) => {
  if (!str) return "location input is empty!";
  let arr = str.split(" ");
  let lat = parseFloat(arr[1].slice(1, 14));
  let lng = parseFloat(arr[2].slice(0, 13));

  return [lng, lat];
};

// get user's phone number and remove country code from it
export const getPhoneNumber = (phone) => {
  if (!phone) return "phone input is empty!";
  return "0" + phone.slice(3);
};

// generate a random number of 10 digits
export const randomNumber = () => {
  let num = Math.random() * 10000000000;
  return num.toString().slice(0, 10);
};

// convert Persian or Arabic numbers to English
let persianNumbers = [
  /۰/g,
  /۱/g,
  /۲/g,
  /۳/g,
  /۴/g,
  /۵/g,
  /۶/g,
  /۷/g,
  /۸/g,
  /۹/g
];
let arabicNumbers = [
  /٠/g,
  /١/g,
  /٢/g,
  /٣/g,
  /٤/g,
  /٥/g,
  /٦/g,
  /٧/g,
  /٨/g,
  /٩/g
];
export const numbersToEnglish = (str) => {
  if (typeof str === "string") {
    let i;
    for (i = 0; i < 10; i++) {
      str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
    }
  }
  return str;
};

// map swap offers items to react-select options
export const mapToSelectItems = (arr) => {
  if (arr.length > 0) {
    let newArr = [];
    let item;
    for (item of arr) {
      newArr.push({
        value: item.slug,
        label: `${item?.title} - [${item.category ? item.category : "-"}]`
      });
    }
    return newArr;
  }
  return [];
};

// create custom setInterval to repeat n times
export const setIntervalX = (callback, delay, repetitions) => {
  let x = 0;
  let intervalID = setInterval(() => {
    callback();
    if (++x === repetitions) clearInterval(intervalID);
  }, delay);
};
