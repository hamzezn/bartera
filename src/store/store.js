import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";

// Reducers
import authReducer from "./reducers/auth";
import accountReducer from "./reducers/account";
import assetReducer from "./reducers/asset";
import provinceReducer from "./reducers/province";
import categoryReducer from "./reducers/category";
import myAssetReducer from "./reducers/myAsset";
import singleAssetReducer from "./reducers/singleAsset";
import favoriteReducer from "./reducers/favorite";
import barterReducer from "./reducers/barter";
import statisticReducer from "./reducers/statistic";
import orderReducer from "./reducers/order";
import reasonReducer from "./reducers/reason";
import profileReducer from "./reducers/profile";
import chatReducer from "./reducers/chat";
import swapOffersReducer from "./reducers/swapOffers";
import imageReducer from "./reducers/image";

//static pages reducers
import aboutUsReducer from "./reducers/static pages/aboutUs";
import FAQReducer from "./reducers/static pages/FAQ";
import barterStoryReducer from "./reducers/static pages/barterStory";
import contactUsReducer from "./reducers/static pages/contactUs";
import manualReducer from "./reducers/static pages/manual";
import privacyReducer from "./reducers/static pages/privacy";
import safeBarterReducer from "./reducers/static pages/safeBarter";
import termsReducer from "./reducers/static pages/terms";

// TODO: "process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose" is the better way
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
  auth: authReducer,
  account: accountReducer,
  asset: assetReducer,
  province: provinceReducer,
  category: categoryReducer,
  myAsset: myAssetReducer,
  singleAsset: singleAssetReducer,
  favorite: favoriteReducer,
  barter: barterReducer,
  statistic: statisticReducer,
  order: orderReducer,
  reason: reasonReducer,
  profile: profileReducer,
  chat: chatReducer,
  swapOffers: swapOffersReducer,
  image: imageReducer,
  // static pages:
  aboutUs: aboutUsReducer,
  FAQ: FAQReducer,
  barterStory: barterStoryReducer,
  contactUs: contactUsReducer,
  manual: manualReducer,
  privacy: privacyReducer,
  safeBarter: safeBarterReducer,
  terms: termsReducer
});

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);

export default store;
