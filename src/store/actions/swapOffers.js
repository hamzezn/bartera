import axios from "../../shared/axios";
import store from "../store";

import * as actionTypes from "./actionTypes";

const loading = () => {
  return {
    type: actionTypes.SWAP_OFFERS_START
  };
};

const success = (result) => {
  return {
    type: actionTypes.SWAP_OFFERS_SUCCESS,
    result
  };
};

const failed = (error) => {
  return {
    type: actionTypes.SWAP_OFFERS_FAIL,
    error
  };
};

export const setSwapSearch = (search) => {
  return {
    type: actionTypes.SWAP_OFFERS_SET_SEARCH,
    search
  };
};

// get all swap offers
export const getSwapOffers = (callback) => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/swap_offer/", {
        params: {
          search: store.getState().swapOffers.search
        }
      })
      .then((res) => {
        dispatch(success(res.data));
        callback && callback.onSuccess && callback.onSuccess(res.data);
      })
      .catch((err) => {
        dispatch(failed(err));
        callback && callback.onError && callback.onError(err);
      });
  };
};
