import axios from "../../shared/axios";

import * as actionTypes from "./actionTypes";
import { toast } from "react-toastify";

const loading = () => {
  return {
    type: actionTypes.FETCH_FAVORITES_START
  };
};

const gettingDataSuccess = (data) => {
  return {
    type: actionTypes.FETCH_FAVORITES_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.FETCH_FAVORITES_FAIL,
    error: error
  };
};

export const getMyFavorites = (page) => {
  return (dispatch) => {
    dispatch(loading());
    dispatch(getFavoriteWithoutLoading(page));
  };
};

export const deleteFavorite = (slug, page = 1) => {
  return (dispatch) => {
    axios
      .delete(`/favorite/${slug}/remove-from-favorite/`)
      .then((response) => {
        toast.success("با موفقیت از لیست علاقه مندی ها حذف شد");
        dispatch(getFavoriteWithoutLoading(page));
      })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  };
};

const getFavoriteWithoutLoading = (page) => {
  return (dispatch) => {
    axios
      .get("/favorite/", { params: { page } })
      .then((response) => {
        dispatch(gettingDataSuccess(response.data));
      })
      .catch((error) => {
        dispatch(failed(error));
      });
  };
};
