import axios from "../../shared/axios";

import * as actionTypes from "./actionTypes";

const loading = () => {
  return {
    type: actionTypes.GET_CHATS_START
  };
};

const success = (result) => {
  return {
    type: actionTypes.GET_CHATS_SUCCESS,
    result
  };
};

const fail = (error) => {
  return {
    type: actionTypes.GET_CHATS_FAIL,
    error
  };
};

const actionLoading = (loading) => {
  return {
    type: actionTypes.CHATS_ACTION_LOADING,
    loading
  };
};

const singleChatLoading = () => {
  return {
    type: actionTypes.SINGLE_CHAT_START
  };
};

const singleChatSuccess = (result) => {
  return {
    type: actionTypes.SINGLE_CHAT_SUCCESS,
    result
  };
};

const singleChatFail = (error) => {
  return {
    type: actionTypes.SINGLE_CHAT_FAIL,
    error
  };
};

export const getAllChats = (callback) => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/chat")
      .then((res) => {
        dispatch(success(res.data));
        callback && callback.onSuccess && callback.onSuccess(res.data);
      })
      .catch((err) => {
        dispatch(fail(err));
        callback && callback.onError && callback.onError(err.response);
      });
  };
};

export const getChatRoom = (id, callback) => {
  return (dispatch) => {
    dispatch(singleChatLoading());
    axios
      .get(`/chat/messages/${id}`)
      .then((res) => {
        dispatch(singleChatSuccess(res.data));
        callback && callback.onSuccess && callback.onSuccess(res.data);
      })
      .catch((err) => {
        dispatch(singleChatFail(err));
        callback && callback.onError && callback.onError(err.response);
      });
  };
};

export const reportChatRoom = (id, callback) => {
  return (dispatch) => {
    dispatch(actionLoading(true));
    axios
      .get(`/chat/messages/${id}/block-report`)
      .then((res) => {
        dispatch(actionLoading(false));
        callback && callback.onSuccess && callback.onSuccess(res.data);
      })
      .catch((err) => {
        dispatch(actionLoading(err));
        callback && callback.onError && callback.onError(err.response);
      });
  };
};

export const addToChat = (slug, callback) => {
  return (dispatch) => {
    dispatch(actionLoading(true));
    axios
      .get(`/asset/${slug}/add-to-chat`)
      .then((res) => {
        dispatch(actionLoading(false));
        callback && callback.onSuccess && callback.onSuccess(res.data);
      })
      .catch((err) => {
        dispatch(actionLoading(false));
        callback && callback.onError && callback.onError(err.response);
      });
  };
};

export const readMessages = (id) => {
  return (dispatch) => {
    axios.get(`/chat/messages/${id}/read/`);
  };
};
