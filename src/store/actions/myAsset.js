import axios from "../../shared/axios";

import * as actionTypes from "./actionTypes";

import { toast } from "react-toastify";

const loading = () => {
  return {
    type: actionTypes.FETCH_MYASSETS_START
  };
};

const gettingDataSuccess = (data) => {
  return {
    type: actionTypes.FETCH_MYASSETS_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.FETCH_MYASSETS_FAIL,
    error: error
  };
};

// for actions
const actionLoading = (loading) => {
  return {
    type: actionTypes.MYASSET_ACTION_LOADING,
    loading
  };
};

const actionSuccess = (result) => {
  return {
    type: actionTypes.MYASSET_ACTION_SUCCESS,
    result
  };
};

const actionError = (error) => {
  return {
    type: actionTypes.MYASSET_ACTION_ERROR,
    error
  };
};

export const getMyAssets = (page, callback) => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/asset/my-assets/", {
        params: {
          page
        }
      })
      .then((response) => {
        dispatch(gettingDataSuccess(response.data));
        callback && callback.onSuccess && callback.onSuccess(response.data);
      })
      .catch((error) => {
        dispatch(failed(error));
      });
  };
};

export const deleteMyAsset = (slug, page = 1, callback) => {
  return (dispatch) => {
    dispatch(actionLoading(true));
    axios
      .delete(`/asset/${slug}/delete_my_asset/`)
      .then(() => {
        dispatch(actionLoading(false));
        dispatch(getMyAssets(page));
        toast.success("آگهی با موفقیت حذف شد");
        callback && callback.onSuccess && callback.onSuccess();
      })
      .catch((error) => {
        toast.error("انجام عملیات با خطا مواجه شد");
        dispatch(actionError(error));
      });
  };
};

export const myAssetBartered = (slug, callback) => {
  return (dispatch) => {
    dispatch(actionLoading(true));
    axios
      .get(`/asset/${slug}/bartered/`)
      .then(() => {
        dispatch(actionLoading(false));
        dispatch(getMyAssets());
        toast.info("وضعیت آگهی به معاوضه شده تغییر یافت");
        callback && callback.onSuccess && callback.onSuccess();
      })
      .catch((error) => {
        toast.error("انجام عملیات با خطا مواجه شد");
        dispatch(actionError(error));
      });
  };
};

export const myAssetOrders = (slug) => {
  return (dispatch) => {
    dispatch(actionLoading(true));
    axios
      .get(`/asset/${slug}/requests_for_this_asset/`)
      .then((res) => {
        dispatch(actionLoading(false));
        dispatch(actionSuccess(res.data));
      })
      .catch((error) => {
        toast.error("انجام عملیات با خطا مواجه شد");
        dispatch(actionError(error));
      });
  };
};
