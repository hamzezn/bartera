import axios from "../../shared/axios";

import * as actionTypes from "./actionTypes";

const loading = () => {
  return {
    type: actionTypes.FETCH_CATEGORY_START
  };
};

const success = (data) => {
  return {
    type: actionTypes.FETCH_CATEGORY_SUCCESS,
    result: data
  };
};

const fail = (error) => {
  return {
    type: actionTypes.FETCH_CATEGORY_FAIL,
    error: error
  };
};

const subLoading = () => {
  return {
    type: actionTypes.FETCH_SUB_CATEGORY_START
  };
};

const subSuccess = (data) => {
  return {
    type: actionTypes.FETCH_SUB_CATEGORY_SUCCESS,
    result: data
  };
};

const subFail = (error) => {
  return {
    type: actionTypes.FETCH_SUB_CATEGORY_FAIL,
    error: error
  };
};

export const getCategories = () => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/category/")
      .then((response) => {
        dispatch(success(response.data));
      })
      .catch((error) => {
        dispatch(fail(error));
      });
  };
};

export const getSubCategory = (id, callback) => {
  return (dispatch) => {
    dispatch(subLoading());
    axios
      .get(`/category/${id}`)
      .then((response) => {
        dispatch(subSuccess(response.data));
        callback && callback.onSuccess && callback.onSuccess(response.data);
      })
      .catch((error) => {
        dispatch(subFail(error));
      });
  };
};
