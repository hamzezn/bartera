import axios from "../../shared/axios";

import * as actionTypes from "./actionTypes";

export const loading = () => {
  return {
    type: actionTypes.PROFILE_START
  };
};

const gettingDataSuccess = (data) => {
  return {
    type: actionTypes.PROFILE_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.PROFILE_FAIL,
    error: error
  };
};

// const actionLoading = (loading) => {
//   return {
//     type: actionTypes.PROFILE_ACTION_LOADING,
//     loading
//   };
// };

export const getOtherProfile = (id, callback) => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get(`/accounts/profile/${id}`)
      .then((res) => {
        dispatch(gettingDataSuccess(res.data));
        callback && callback.onSuccess && callback.onSuccess();
      })
      .catch((err) => {
        dispatch(failed(err));
        callback && callback.onError && callback.onError();
      });
  };
};
