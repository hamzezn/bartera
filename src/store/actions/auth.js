import axios from "../../shared/axios";

import * as actionTypes from "./actionTypes";

import { toast, Slide } from "react-toastify";

const authStart = () => {
  return {
    type: actionTypes.AUTH_START
  };
};

const authSuccess = (phoneNumber) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    loading: false,
    phoneNumber: phoneNumber
  };
};

const verifySuccess = (token, refreshToken) => {
  return {
    type: actionTypes.VERIFY_SUCCESS,
    token: token,
    refreshToken: refreshToken,
    loading: false
  };
};

export const setToken = (token) => {
  return {
    type: actionTypes.SET_TOKEN,
    token: token
  };
};

const setFullName = (fullName) => {
  return {
    type: actionTypes.SET_FULL_NAME,
    fullName: fullName
  };
};

const setUserId = (id) => {
  return {
    type: actionTypes.SET_USER_ID,
    id
  };
};

const authFail = (error) => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: error
  };
};

export const logout = () => {
  localStorage.removeItem("token");
  localStorage.removeItem("refreshToken");
  localStorage.removeItem("fullName");
  localStorage.removeItem("userID");
  return {
    type: actionTypes.AUTH_LOGOUT
  };
};

export const setAuthRedirectPath = (path) => {
  return {
    type: actionTypes.SET_AUTH_REDIRECT_PATH,
    path: path
  };
};

export const register = (fullName, phoneNumber) => {
  return (dispatch) => {
    dispatch(authStart());
    const data = {
      name: fullName,
      phone_number: phoneNumber
    };
    axios
      .post("/accounts/verification/register/", data)
      .then((response) => {
        localStorage.setItem("fullName", fullName);
        dispatch(authSuccess(phoneNumber));
        toast.info(response.data.message);
      })
      .catch((error) => {
        dispatch(authFail(error));
        toast.error(error.response.data.message);
      });
  };
};

export const login = (phoneNumber) => {
  return (dispatch) => {
    dispatch(authStart());
    const data = {
      phone_number: phoneNumber
    };
    axios
      .post("/accounts/verification/login/", data)
      .then((response) => {
        dispatch(authSuccess(phoneNumber));
        toast.info(response.data.message);
      })
      .catch((error) => {
        dispatch(authFail(error));
        toast.warn(error.response.data.message, {
          position: "top-center",
          autoClose: 8000,
          transition: Slide
        });
      });
  };
};

export const verify = (phoneNumber, code, from) => {
  return (dispatch) => {
    dispatch(authStart());
    const data = {
      phone_number: phoneNumber,
      security_code: code
    };
    axios
      .post("/accounts/verification/verify/", data)
      .then((response) => {
        const access = response.data.access;
        const refresh = response.data.refresh;
        localStorage.setItem("token", access);
        localStorage.setItem("refreshToken", refresh);
        dispatch(verifySuccess(access, refresh));
        axios
          .get("/accounts/profile/view/")
          .then((response) => {
            localStorage.setItem("fullName", response.data.name);
            localStorage.setItem("userID", response.data.id);
            dispatch(setFullName(response.data.name));
            dispatch(setUserId(response.data.id));
            if (from === "register")
              toast.info("ثبت نام شما با موفقیت انجام شد");
            else toast.info("خوش آمدید");
          })
          .catch((error) => {
            dispatch(authFail(error));
            toast.error(error.response.data.message);
          });
      })
      .catch((error) => {
        dispatch(authFail(error));
        toast.error(error?.response?.data?.message);
      });
  };
};
