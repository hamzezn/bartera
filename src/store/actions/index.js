export { register, logout, verify, login, setToken } from "./auth";

export { getProfileInfo, sendProfileInfo } from "./account";

export {
  getAssets,
  setCategory,
  setCity,
  setHasImage,
  setOrdering,
  setSearch,
  addAssetToFavorite,
  removeFromFavorite,
  createAsset
} from "./asset";

export {
  getSingleAsset,
  addSingleAssetToFavorite,
  removeSingleFromFavorite,
  reportAsset,
  editSingleAsset
} from "./singleAsset";

export { getProvinces } from "./province";

export { getCategories, getSubCategory } from "./category";

export {
  getMyAssets,
  deleteMyAsset,
  myAssetBartered,
  myAssetOrders
} from "./myAsset";

export { getMyFavorites, deleteFavorite } from "./favorite";

export { listMyRequests, cancelRequest, barter } from "./barter";

export { getBarterStatistics } from "./statistic";

export { listOrders, acceptOrder, rejectOrder } from "./order";

export { getReasons } from "./reason";

export { getOtherProfile } from "./profile";

// static pages
export { getAboutUsContent } from "./static pages/aboutUs";
export { getFAQContent } from "./static pages/FAQ";
export { getBarterStoryContent } from "./static pages/barterStory";
export { getContactUsContent } from "./static pages/contactUs";
export { getManualContent } from "./static pages/manual";
export { getPrivacyContent } from "./static pages/privacy";
export { getSafeBarterContent } from "./static pages/safeBarter";
export { getTermsContent } from "./static pages/terms";

export {
  getAllChats,
  getChatRoom,
  reportChatRoom,
  addToChat,
  readMessages
} from "./chat";

export { setSwapSearch, getSwapOffers } from "./swapOffers";

export { uploadImage, uploadChatImage } from "./image";
