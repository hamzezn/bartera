/*****
 * Auth
 */
export const AUTH_START = "AUTH_START";
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const VERIFY_SUCCESS = "VERIFY_SUCCESS";
export const SET_TOKEN = "SET_TOKEN";
export const SET_FULL_NAME = "SET_FULL_NAME";
export const AUTH_FAIL = "AUTH_FAIL";
export const AUTH_LOGOUT = "AUTH_LOGOUT";
export const SET_AUTH_REDIRECT_PATH = "SET_AUTH_REDIRECT_PATH";
export const SET_USER_ID = "SET_USER_ID";

/******
 * Account
 */
export const ACCOUNT_PROFILE_START = "ACCOUNT_PROFILE_VIEW_START";
export const ACCOUNT_PROFILE_SUCCESS = "ACCOUNT_PROFILE_VIEW_SUCCESS";
export const ACCOUNT_PROFILE_FAIL = "ACCOUNT_PROFILE_VIEW_FAIL";

export const ACCOUNT_ACTION_LOADING = "ACCOUNT_ACTION_LOADING";

/******
 * Asset
 */
//  for all assets
export const FETCH_ASSETS_START = "FETCH_ASSETS_START";
export const FETCH_ASSETS_SUCCESS = "FETCH_ASSETS_SUCCESS";
export const FETCH_ASSETS_FAIL = "FETCH_ASSETS_FAIL";
// assets filtering
export const SET_ASSET_ORDERING = "SET_ASSET_ORDERING";
export const SET_ASSET_CITY = "SET_ASSET_CITY";
export const SET_ASSET_CATEGORY = "SET_ASSET_CATEGORY";
export const SET_ASSET_HAS_IMAGE = "SET_ASSET_HAS_IMAGE";
export const SET_ASSET_SEARCH = "SET_ASSET_SEARCH";
// create asset
export const ASSET_CREATE_LOADING = "ASSET_CREATE_LOADING";
export const ASSET_CREATE = "ASSET_CREATE";
export const ASSET_CREATE_FAIL = "ASSET_CREATE_FAIL";
// asset actions
export const ASSET_ACTION_LOADING = "ASSET_ACTION_LOADING";

/*****
 * Province
 */
export const FETCH_PROVINCES_START = "FETCH_PROVINCES";
export const FETCH_PROVINCES_SUCCESS = "FETCH_PROVINCES_SUCCESS";
export const FETCH_PROVINCES_FAIL = "FETCH_PROVINCES_FAIL";

/*****
 * Category
 */

export const FETCH_CATEGORY_START = "FETCH_CATEGORY_START";
export const FETCH_CATEGORY_SUCCESS = "FETCH_CATEGORY_SUCCESS";
export const FETCH_CATEGORY_FAIL = "FETCH_CATEGORY_FAIL";

export const FETCH_SUB_CATEGORY_START = "FETCH_SUB_CATEGORY_START";
export const FETCH_SUB_CATEGORY_SUCCESS = "FETCH_SUB_CATEGORY_SUCCESS";
export const FETCH_SUB_CATEGORY_FAIL = "FETCH_SUB_CATEGORY_FAIL";

/******
 * MyAsset
 */
export const FETCH_MYASSETS_START = "FETCH_MYASSETS_START";
export const FETCH_MYASSETS_SUCCESS = "FETCH_MYASSETS_SUCCESS";
export const FETCH_MYASSETS_FAIL = "FETCH_MYASSETS_FAIL";

export const MYASSET_ACTION_LOADING = "MYASSET_ACTION_LOADING";
export const MYASSET_ACTION_SUCCESS = "MYASSET_ACTION_SUCCESS";
export const MYASSET_ACTION_ERROR = "MYASSET_ACTION_ERROR";

/******
 * SingleAsset
 */
export const FETCH_ASSET_START = "FETCH_ASSET_START";
export const FETCH_ASSET_SUCCESS = "FETCH_ASSET_SUCCESS";
export const FETCH_ASSET_FAIL = "FETCH_ASSET_FAIL";

export const SINGLE_ASSET_ACTION_LOADING = "SINGLE_ASSET_ACTION_LOADING";
export const SINGLE_ASSET_ACTION_SUCCESS = "SINGLE_ASSET_ACTION_SUCCESS";
export const SINGLE_ASSET_ACTION_ERROR = "SINGLE_ASSET_ACTION_ERROR";

/******
 * favorites
 */
export const FETCH_FAVORITES_START = "FETCH_FAVORITES_START";
export const FETCH_FAVORITES_SUCCESS = "FETCH_FAVORITES_SUCCESS";
export const FETCH_FAVORITES_FAIL = "FETCH_FAVORITES_FAIL";

/******
 * barter
 */
export const FETCH_BARTER_START = "FETCH_BARTER_START";
export const FETCH_BARTER_SUCCESS = "FETCH_BARTER_SUCCESS";
export const FETCH_BARTER_FAIL = "FETCH_BARTER_FAIL";
export const FETCH_ACTION_BARTER_START = "FETCH_ACTION_BARTER_START";

/******
 * statistics
 */
export const FETCH_STATS_START = "FETCH_STATS_START";
export const FETCH_STATS_SUCCESS = "FETCH_STATS_SUCCESS";
export const FETCH_STATS_FAIL = "FETCH_STATS_FAIL";

/******
 * orders
 */
export const FETCH_ORDERS_START = "FETCH_ORDERS_START";
export const FETCH_ORDERS_SUCCESS = "FETCH_ORDERS_SUCCESS";
export const FETCH_ORDERS_FAIL = "FETCH_ORDERS_FAIL";
export const ORDERS_ACTION_LOADING = "ORDERS_ACTION_LOADING";

/******
 * reasons
 */
export const FETCH_REASONS_START = "FETCH_REASONS_START";
export const FETCH_REASONS_SUCCESS = "FETCH_REASONS_SUCCESS";
export const FETCH_REASONS_FAIL = "FETCH_REASONS_FAIL";

/******
 * other profiles
 */
export const PROFILE_START = "PROFILE_START";
export const PROFILE_SUCCESS = "PROFILE_SUCCESS";
export const PROFILE_FAIL = "PROFILE_FAIL";
export const PROFILE_ACTION_LOADING = "PROFILE_ACTION_LOADING";

/*** static pages ***/

/*** about us ***/
export const ABOUT_US_START = "ABOUT_US_START";
export const ABOUT_US_SUCCESS = "ABOUT_US_SUCCESS";
export const ABOUT_US_FAIL = "ABOUT_US_FAIL";

/*** FAQ ***/
export const FAQ_START = "FAQ_START";
export const FAQ_SUCCESS = "FAQ_SUCCESS";
export const FAQ_FAIL = "FAQ_FAIL";

/*** barter story ***/
export const BARTER_STORY_START = "BARTER_STORY_START";
export const BARTER_STORY_SUCCESS = "BARTER_STORY_SUCCESS";
export const BARTER_STORY_FAIL = "BARTER_STORY_FAIL";

/*** contact us ***/
export const CONTACT_US_START = "CONTACT_US_START";
export const CONTACT_US_SUCCESS = "CONTACT_US_SUCCESS";
export const CONTACT_US_FAIL = "CONTACT_US_FAIL";

/*** manual ***/
export const MANUAL_START = "MANUAL_START";
export const MANUAL_SUCCESS = "MANUAL_SUCCESS";
export const MANUAL_FAIL = "MANUAL_FAIL";

/*** privacy ***/
export const PRIVACY_START = "PRIVACY_START";
export const PRIVACY_SUCCESS = "PRIVACY_SUCCESS";
export const PRIVACY_FAIL = "PRIVACY_FAIL";

/*** safe barter ***/
export const SAFE_BARTER_START = "SAFE_BARTER_START";
export const SAFE_BARTER_SUCCESS = "SAFE_BARTER_SUCCESS";
export const SAFE_BARTER_FAIL = "SAFE_BARTER_FAIL";

/*** terms ***/
export const TERMS_START = "TERMS_START";
export const TERMS_SUCCESS = "TERMS_SUCCESS";
export const TERMS_FAIL = "TERMS_FAIL";

// ----------------------
// chat and messages
// ----------------------
export const GET_CHATS_START = "GET_CHATS_START";
export const GET_CHATS_SUCCESS = "GET_CHATS_SUCCESS";
export const GET_CHATS_FAIL = "GET_CHATS_FAIL";

export const SINGLE_CHAT_START = "SINGLE_CHAT_START";
export const SINGLE_CHAT_SUCCESS = "SINGLE_CHAT_SUCCESS";
export const SINGLE_CHAT_FAIL = "SINGLE_CHAT_FAIL";

export const CHATS_ACTION_LOADING = "CHATS_ACTION_LOADING";

// ----------------------
// swap offers
// ----------------------
export const SWAP_OFFERS_START = "SWAP_OFFERS_START";
export const SWAP_OFFERS_SUCCESS = "SWAP_OFFERS_SUCCESS";
export const SWAP_OFFERS_FAIL = "SWAP_OFFERS_FAIL";
export const SWAP_OFFERS_SET_SEARCH = "SWAP_OFFERS_SET_SEARCH";

// ----------------------
// images (upload and delete)
// ----------------------
export const IMAGE_START = "IMAGE_START";
export const IMAGE_SUCCESS = "IMAGE_SUCCESS";
export const IMAGE_FAIL = "IMAGE_FAIL";
export const IMAGE_ACTION_LOADING = "IMAGE_ACTION_LOADING";
