import axios from '../../shared/axios';

import * as actionTypes from './actionTypes';

const loading = () => {
    return {
        type: actionTypes.FETCH_PROVINCES_START,
    };
};

const success = (data) => {
    return {
        type: actionTypes.FETCH_PROVINCES_SUCCESS,
        result: data,
    };
};

const fail = (error) => {
    return {
        type: actionTypes.FETCH_PROVINCES_FAIL,
        error: error,
    };
};

export const getProvinces = () => {
    return dispatch => {
        dispatch(loading());
        axios.get('/area/province/')
            .then(response => {
                dispatch(success(response.data));
            })
            .catch(error => {
                dispatch(fail(error));
            });
    };
};