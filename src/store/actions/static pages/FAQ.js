import axios from "../../../shared/axios";

import * as actionTypes from "../actionTypes";

const loading = () => {
  return {
    type: actionTypes.FAQ_START
  };
};

const success = (data) => {
  return {
    type: actionTypes.FAQ_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.FAQ_FAIL,
    error: error
  };
};

export const getFAQContent = () => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/pages/FAQ")
      .then((response) => {
        dispatch(success(response.data));
      })
      .catch((error) => {
        dispatch(failed(error));
      });
  };
};
