import axios from "../../../shared/axios";

import * as actionTypes from "../actionTypes";

const loading = () => {
  return {
    type: actionTypes.TERMS_START
  };
};

const success = (data) => {
  return {
    type: actionTypes.TERMS_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.TERMS_FAIL,
    error: error
  };
};

export const getTermsContent = () => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/pages/terms")
      .then((response) => {
        dispatch(success(response.data));
      })
      .catch((error) => {
        dispatch(failed(error));
      });
  };
};
