import axios from "../../../shared/axios";

import * as actionTypes from "../actionTypes";

const loading = () => {
  return {
    type: actionTypes.BARTER_STORY_START
  };
};

const success = (data) => {
  return {
    type: actionTypes.BARTER_STORY_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.BARTER_STORY_FAIL,
    error: error
  };
};

export const getBarterStoryContent = () => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/pages/barte_rstory")
      .then((response) => {
        dispatch(success(response.data));
      })
      .catch((error) => {
        dispatch(failed(error));
      });
  };
};
