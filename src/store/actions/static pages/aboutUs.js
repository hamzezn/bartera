import axios from "../../../shared/axios";

import * as actionTypes from "../actionTypes";

const loading = () => {
  return {
    type: actionTypes.ABOUT_US_START
  };
};

const success = (data) => {
  return {
    type: actionTypes.ABOUT_US_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.ABOUT_US_FAIL,
    error: error
  };
};

export const getAboutUsContent = () => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/pages/about")
      .then((response) => {
        dispatch(success(response.data));
      })
      .catch((error) => {
        dispatch(failed(error));
      });
  };
};
