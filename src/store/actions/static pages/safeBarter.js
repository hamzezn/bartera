import axios from "../../../shared/axios";

import * as actionTypes from "../actionTypes";

const loading = () => {
  return {
    type: actionTypes.SAFE_BARTER_START
  };
};

const success = (data) => {
  return {
    type: actionTypes.SAFE_BARTER_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.SAFE_BARTER_FAIL,
    error: error
  };
};

export const getSafeBarterContent = () => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/pages/safe_barter")
      .then((response) => {
        dispatch(success(response.data));
      })
      .catch((error) => {
        dispatch(failed(error));
      });
  };
};
