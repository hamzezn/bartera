import axios from "../../../shared/axios";

import * as actionTypes from "../actionTypes";

const loading = () => {
  return {
    type: actionTypes.MANUAL_START
  };
};

const success = (data) => {
  return {
    type: actionTypes.MANUAL_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.MANUAL_FAIL,
    error: error
  };
};

export const getManualContent = () => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/pages/manual")
      .then((response) => {
        dispatch(success(response.data));
      })
      .catch((error) => {
        dispatch(failed(error));
      });
  };
};
