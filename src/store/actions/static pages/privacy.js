import axios from "../../../shared/axios";

import * as actionTypes from "../actionTypes";

const loading = () => {
  return {
    type: actionTypes.PRIVACY_START
  };
};

const success = (data) => {
  return {
    type: actionTypes.PRIVACY_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.PRIVACY_FAIL,
    error: error
  };
};

export const getPrivacyContent = () => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/pages/privacy")
      .then((response) => {
        dispatch(success(response.data));
      })
      .catch((error) => {
        dispatch(failed(error));
      });
  };
};
