import axios from "../../../shared/axios";

import * as actionTypes from "../actionTypes";

const loading = () => {
  return {
    type: actionTypes.CONTACT_US_START
  };
};

const success = (data) => {
  return {
    type: actionTypes.CONTACT_US_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.CONTACT_US_FAIL,
    error: error
  };
};

export const getContactUsContent = () => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/pages/contact_us")
      .then((response) => {
        dispatch(success(response.data));
      })
      .catch((error) => {
        dispatch(failed(error));
      });
  };
};
