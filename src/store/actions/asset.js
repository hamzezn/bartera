import axios from "../../shared/axios";
import store from "../store";

import * as actionTypes from "./actionTypes";
import { toast } from "react-toastify";

// history
import history from "../../shared/history";

// all assets
const loading = () => {
  return {
    type: actionTypes.FETCH_ASSETS_START
  };
};

const gettingDataSuccess = (data) => {
  return {
    type: actionTypes.FETCH_ASSETS_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.FETCH_ASSETS_FAIL,
    error: error
  };
};

// filter all assets
export const setCity = (city) => {
  return {
    type: actionTypes.SET_ASSET_CITY,
    city
  };
};

export const setCategory = (category) => {
  return {
    type: actionTypes.SET_ASSET_CATEGORY,
    category
  };
};

export const setOrdering = (ordering) => {
  return {
    type: actionTypes.SET_ASSET_ORDERING,
    ordering
  };
};

export const setHasImage = (has_image) => {
  return {
    type: actionTypes.SET_ASSET_HAS_IMAGE,
    has_image
  };
};

export const setSearch = (search) => {
  return {
    type: actionTypes.SET_ASSET_SEARCH,
    search
  };
};

// asset actions
const actionLoading = (loading) => {
  return {
    type: actionTypes.ASSET_ACTION_LOADING,
    loading
  };
};

// get all assets
export const getAssets = (page) => {
  return (dispatch) => {
    dispatch(loading());
    dispatch(getAssetsWithoutLoading(page));
  };
};

export const addAssetToFavorite = (slug, page = 1) => {
  return (dispatch) => {
    dispatch(actionLoading(true));
    axios
      .get(`/asset/${slug}/add-to-favorite`)
      .then((response) => {
        toast.info("به علاقه مندی های شما اضافه شد");
        dispatch(getAssetsWithoutLoading(page));
      })
      .catch((error) => {
        dispatch(actionLoading(false));
        toast.error(error.response.data.message);
      });
  };
};

export const removeFromFavorite = (slug, page = 1) => {
  return (dispatch) => {
    dispatch(actionLoading(true));
    axios
      .delete(`/favorite/${slug}/remove-from-favorite/`)
      .then((response) => {
        toast.info("با موفقیت از لیست علاقه مندی ها حذف شد");
        dispatch(getAssetsWithoutLoading(page));
      })
      .catch((error) => {
        toast.error(error.response.data.message);
        dispatch(actionLoading(false));
      });
  };
};

const getAssetsWithoutLoading = (page) => {
  return (dispatch) => {
    const city = store.getState().asset.city;
    const category = store.getState().asset.category;
    const has_image = store.getState().asset.has_image;
    const ordering = store.getState().asset.ordering;
    const search = store.getState().asset.search;

    const params = new URLSearchParams();
    if (page) params.append("page", page);
    if (has_image) params.append("has_image", has_image);
    if (search) params.append("search", search);
    if (ordering) params.append("ordering", ordering);

    if (city) {
      if (typeof city === "number") {
        params.append("city", city);
      } else if (typeof city === "object") {
        city.forEach((city) => {
          params.append("city", city);
        });
      }
    }

    if (category) {
      if (typeof category === "number") {
        params.append("category", category);
      } else if (typeof category === "object") {
        category.forEach((category) => {
          params.append("category", category);
        });
      }
    }

    axios
      .get("/asset/", { params })
      .then((response) => {
        dispatch(actionLoading(false));
        dispatch(gettingDataSuccess(response.data));
      })
      .catch((error) => {
        dispatch(failed(error));
        dispatch(actionLoading(false));
      });
  };
};

// creating a new asset
const createLoading = () => {
  return {
    type: actionTypes.ASSET_CREATE_LOADING
  };
};

const createSuccess = (data) => {
  return {
    type: actionTypes.ASSET_CREATE,
    result: data
  };
};

const createFailed = (error) => {
  return {
    type: actionTypes.ASSET_CREATE_FAIL,
    error: error
  };
};

export const createAsset = (data) => {
  return (dispatch) => {
    dispatch(createLoading());
    axios
      .post("/asset/", data)
      .then((res) => {
        dispatch(createSuccess(res.data));
        toast.info("آگهی شما با موفقیت ثبت شد، منتظر تایید آگهی خود بمانید");
        history.push("/");
      })
      .catch((e) => {
        dispatch(createFailed(e));
      });
  };
};
