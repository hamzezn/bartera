import axios from "../../shared/axios";

import * as actionTypes from "./actionTypes";
import { toast } from "react-toastify";

import history from "../../shared/history";

// single asset
const loading = () => {
  return {
    type: actionTypes.FETCH_ASSET_START
  };
};

const gettingDataSuccess = (data) => {
  return {
    type: actionTypes.FETCH_ASSET_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.FETCH_ASSET_FAIL,
    error: error
  };
};

const actionLoading = (loading) => {
  return {
    type: actionTypes.SINGLE_ASSET_ACTION_LOADING,
    loading
  };
};

export const getSingleAsset = (slug, callback) => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get(`/asset/${slug}`)
      .then((response) => {
        dispatch(gettingDataSuccess(response.data));
        callback && callback.onSuccess && callback.onSuccess(response.data);
      })
      .catch((error) => {
        dispatch(failed(error));
      });
  };
};

export const addSingleAssetToFavorite = (slug) => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get(`/asset/${slug}/add-to-favorite`)
      .then((response) => {
        toast.success("به علاقه مندی های شما اضافه شد");
        dispatch(getSingleAsset(slug));
      })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  };
};

export const removeSingleFromFavorite = (slug) => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .delete(`/favorite/${slug}/remove-from-favorite/`)
      .then((response) => {
        toast.success("با موفقیت از لیست علاقه مندی ها حذف شد");
        dispatch(getSingleAsset(slug));
      })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  };
};

export const reportAsset = (slug, reason) => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .post(`/asset/${slug}/report/`, { reason })
      .then((response) => {
        toast.success("آگهی مورد نظر گزارش شد");
        dispatch(getSingleAsset(slug));
      })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  };
};

export const editSingleAsset = (slug, data) => {
  return (dispatch) => {
    dispatch(actionLoading(true));
    axios
      .patch(`/asset/${slug}/`, data)
      .then((res) => {
        toast.info("تغییرات اعمال شد");
        dispatch(actionLoading(false));
        history.push("/dashboard/myAdvertisements");
      })
      .catch((err) => {
        dispatch(actionLoading(false));
        toast.error("عملیات با خطا مواجه شد");
      });
  };
};
