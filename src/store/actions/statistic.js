import axios from "../../shared/axios";

import * as actionTypes from "./actionTypes";
// import { toast } from "react-toastify";

export const loading = () => {
  return {
    type: actionTypes.FETCH_STATS_START
  };
};

const gettingDataSuccess = (data) => {
  return {
    type: actionTypes.FETCH_STATS_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.FETCH_STATS_FAIL,
    error: error
  };
};

export const getBarterStatistics = (callback) => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/barter/statistics/")
      .then((response) => {
        dispatch(gettingDataSuccess(response.data));
        callback && callback.onSuccess && callback.onSuccess(response.data);
      })
      .catch((error) => {
        dispatch(failed(error));
      });
  };
};
