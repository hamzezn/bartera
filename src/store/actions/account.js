import axios from "../../shared/axios";

import * as actionTypes from "./actionTypes";
import { toast } from "react-toastify";

const loading = () => {
  return {
    type: actionTypes.ACCOUNT_PROFILE_START
  };
};

const gettingDataSuccess = (data) => {
  return {
    type: actionTypes.ACCOUNT_PROFILE_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.ACCOUNT_PROFILE_FAIL,
    error: error
  };
};

const actionLoading = (loading) => {
  return {
    type: actionTypes.ACCOUNT_ACTION_LOADING,
    loading
  };
};

export const getProfileInfo = () => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/accounts/profile/view")
      .then((response) => {
        dispatch(gettingDataSuccess(response.data));
      })
      .catch((error) => {
        dispatch(failed(error));
      });
  };
};

export const sendProfileInfo = (data) => {
  return (dispatch) => {
    dispatch(actionLoading(true));
    axios
      .patch("/accounts/profile/edit/", data, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      })
      .then((response) => {
        window.scrollTo(0, 0);
        toast.info("اطلاعات شما با موفقیت بروزرسانی شد");
        localStorage.setItem("fullName", response.data.name);
        dispatch(actionLoading(false));
        dispatch(getProfileInfo());
      })
      .catch((error) => {
        dispatch(failed(error));
        dispatch(actionLoading(false));
      });
  };
};
