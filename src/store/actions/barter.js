import axios from "../../shared/axios";

import * as actionTypes from "./actionTypes";
import { toast } from "react-toastify";

export const loading = () => {
  return {
    type: actionTypes.FETCH_BARTER_START
  };
};

const gettingDataSuccess = (data) => {
  return {
    type: actionTypes.FETCH_BARTER_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.FETCH_BARTER_FAIL,
    error: error
  };
};

export const actionLoading = (loading) => {
  return {
    type: actionTypes.FETCH_ACTION_BARTER_START,
    loading
  };
};

export const listMyRequests = (page) => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/barter/my_requests/", { params: { page } })
      .then((response) => {
        dispatch(gettingDataSuccess(response.data));
      })
      .catch((error) => {
        dispatch(failed(error));
      });
  };
};

export const cancelRequest = (id, callback) => {
  return (dispatch) => {
    dispatch(actionLoading(true));
    axios
      .delete(`/barter/${id}/cancel_request/`)
      .then((response) => {
        toast.info("درخواست موردنظر لغو شد");
        dispatch(actionLoading(false));
        dispatch(listMyRequests());
        callback && callback.onSuccess && callback.onSuccess();
      })
      .catch((error) => {
        toast.error(error.response.data.message);
        dispatch(actionLoading(false));
      });
  };
};

export const barter = (data, callback) => {
  return (dispatch) => {
    dispatch(actionLoading(true));
    axios
      .post("/barter/", data)
      .then((response) => {
        dispatch(actionLoading(false));
        toast.info("درخواست شما برای معاوضه ثبت شد");
        callback && callback.onSuccess && callback.onSuccess();
      })
      .catch((error) => {
        dispatch(actionLoading(false));
        if (error.response.status === 500) toast.warn("درخواست شما تکراری است");
        if (error.response.status === 400)
          toast.warn(error.response.data.applicant_asset[0]);
      });
  };
};
