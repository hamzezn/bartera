import axios from "../../shared/axios";

import * as actionTypes from "./actionTypes";
import { toast } from "react-toastify";

export const loading = () => {
  return {
    type: actionTypes.FETCH_ORDERS_START
  };
};

const gettingDataSuccess = (data) => {
  return {
    type: actionTypes.FETCH_ORDERS_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.FETCH_ORDERS_FAIL,
    error: error
  };
};

const actionLoading = (loading) => {
  return {
    type: actionTypes.ORDERS_ACTION_LOADING,
    loading
  };
};

export const listOrders = (page) => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/barter/my_received/", { params: { page } })
      .then((response) => {
        dispatch(gettingDataSuccess(response.data));
      })
      .catch((error) => {
        dispatch(failed(error));
      });
  };
};

export const acceptOrder = (id, callback) => {
  return (dispatch) => {
    dispatch(actionLoading(true));
    axios
      .post(`/barter/${id}/accept_or_reject/`, { accept: true })
      .then((response) => {
        toast.success("سفارش موردنظر تایید شد");
        dispatch(actionLoading(false));
        dispatch(listOrders());
        callback && callback.onSuccess && callback.onSuccess();
      })
      .catch((error) => {
        toast.warn(error.response.data.message);
        dispatch(actionLoading(false));
      });
  };
};

export const rejectOrder = (id, reason, callback) => {
  return (dispatch) => {
    dispatch(actionLoading(true));
    axios
      .post(`/barter/${id}/accept_or_reject/`, { accept: false, reason })
      .then((response) => {
        toast.info("سفارش موردنظر رد شد");
        dispatch(actionLoading(false));
        dispatch(listOrders());
        callback && callback.onSuccess && callback.onSuccess();
      })
      .catch((error) => {
        toast.warn(error.response.data.message);
        dispatch(actionLoading(false));
      });
  };
};
