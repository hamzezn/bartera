import axios from "../../shared/axios";

import * as actionTypes from "./actionTypes";

export const loading = () => {
  return {
    type: actionTypes.FETCH_REASONS_START
  };
};

const gettingDataSuccess = (data) => {
  return {
    type: actionTypes.FETCH_REASONS_SUCCESS,
    result: data
  };
};

const failed = (error) => {
  return {
    type: actionTypes.FETCH_REASONS_FAIL,
    error: error
  };
};

export const getReasons = () => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .get("/barter/reject_reasons/")
      .then((response) => {
        dispatch(gettingDataSuccess(response.data));
      })
      .catch((error) => {
        dispatch(failed(error));
      });
  };
};
