import axios from "../../shared/axios";

import * as actionTypes from "./actionTypes";
// import { toast } from "react-toastify";

export const loading = () => {
  return {
    type: actionTypes.IMAGE_START
  };
};

const success = (result) => {
  return {
    type: actionTypes.IMAGE_SUCCESS,
    result
  };
};

const failed = (error) => {
  return {
    type: actionTypes.IMAGE_FAIL,
    error
  };
};

// const actionLoading = (loading) => {
//   return {
//     type: actionTypes.IMAGE_ACTION_LOADING,
//     loading
//   };
// };

export const uploadImage = (data, callback) => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .post("/asset/upload_image/", data, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      })
      .then((res) => {
        dispatch(success(res.data));
        callback && callback.onSuccess && callback.onSuccess(res.data);
      })
      .catch((err) => {
        dispatch(failed(err));
        callback && callback.onError && callback.onError(err);
      });
  };
};

export const uploadChatImage = (data, callback) => {
  return (dispatch) => {
    dispatch(loading());
    axios
      .post("/chat/upload_image/", data, {
        headers: { "Content-Type": "multipart/form-data" }
      })
      .then((res) => {
        dispatch(success(res.data));
        callback && callback.onSuccess && callback.onSuccess(res.data);
      })
      .catch((err) => {
        dispatch(failed(err));
        callback && callback.onError && callback.onError(err);
      });
  };
};
