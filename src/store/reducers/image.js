import * as actionTypes from "../actions/actionTypes";

const initialState = {
  result: null,
  loading: false,
  error: null,
  actionLoading: false
};

const imageReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.IMAGE_START:
      return { ...state, loading: true, result: null, error: null };
    case actionTypes.IMAGE_SUCCESS:
      return { ...state, loading: false, result: action.result, error: null };
    case actionTypes.IMAGE_FAIL:
      return { ...state, loading: false, error: action.error };
    case actionTypes.IMAGE_ACTION_LOADING:
      return { ...state, actionLoading: action.loading };
    default:
      return state;
  }
};

export default imageReducer;
