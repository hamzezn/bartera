import * as actionTypes from "../actions/actionTypes";

const initialState = {
  result: null,
  loading: false,
  error: null,

  // for actions:
  actionLoading: null,
  actionResult: null,
  actionError: null
};

const myAssetReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_MYASSETS_START:
      return { ...state, loading: true, result: null, error: null };
    case actionTypes.FETCH_MYASSETS_SUCCESS:
      return { ...state, loading: false, result: action.result, error: null };
    case actionTypes.FETCH_MYASSETS_FAIL:
      return { ...state, loading: false, result: null, error: action.error };

    // for actions
    case actionTypes.MYASSET_ACTION_LOADING:
      return {
        ...state,
        actionLoading: action.loading
      };
    case actionTypes.MYASSET_ACTION_SUCCESS:
      return {
        ...state,
        actionLoading: false,
        actionResult: action.result,
        actionError: null
      };
    case actionTypes.MYASSET_ACTION_ERROR:
      return {
        ...state,
        actionLoading: false,
        actionError: action.error,
        actionResult: null
      };

    default:
      return state;
  }
};

export default myAssetReducer;
