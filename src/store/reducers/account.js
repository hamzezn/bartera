import * as actionTypes from "../actions/actionTypes";

const initialState = {
  result: null,
  loading: false,
  error: null,

  actionLoading: false
};

const accountReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ACCOUNT_PROFILE_START:
      return { ...state, loading: true, result: null, error: null };
    case actionTypes.ACCOUNT_PROFILE_SUCCESS:
      return { ...state, loading: false, result: action.result, error: null };
    case actionTypes.ACCOUNT_PROFILE_FAIL:
      return { ...state, loading: false, result: null, error: action.error };
    case actionTypes.ACCOUNT_ACTION_LOADING:
      return { ...state, actionLoading: action.loading };
    default:
      return state;
  }
};

export default accountReducer;
