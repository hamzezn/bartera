import * as actionTypes from "../../actions/actionTypes";

const initialState = {
  result: null,
  loading: false,
  error: null
};

const contactUsReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CONTACT_US_START:
      return { ...state, loading: true, result: null, error: null };
    case actionTypes.CONTACT_US_SUCCESS:
      return { ...state, loading: false, result: action.result, error: null };
    case actionTypes.CONTACT_US_FAIL:
      return { ...state, loading: false, result: null, error: action.error };
    default:
      return state;
  }
};

export default contactUsReducer;
