import * as actionTypes from "../../actions/actionTypes";

const initialState = {
  result: null,
  loading: false,
  error: null
};

const FAQReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FAQ_START:
      return { ...state, loading: true, result: null, error: null };
    case actionTypes.FAQ_SUCCESS:
      return { ...state, loading: false, result: action.result, error: null };
    case actionTypes.FAQ_FAIL:
      return { ...state, loading: false, result: null, error: action.error };
    default:
      return state;
  }
};

export default FAQReducer;
