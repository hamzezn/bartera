import * as actionTypes from "../../actions/actionTypes";

const initialState = {
  result: null,
  loading: false,
  error: null
};

const safeBarterReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SAFE_BARTER_START:
      return { ...state, loading: true, result: null, error: null };
    case actionTypes.SAFE_BARTER_SUCCESS:
      return { ...state, loading: false, result: action.result, error: null };
    case actionTypes.SAFE_BARTER_FAIL:
      return { ...state, loading: false, result: null, error: action.error };
    default:
      return state;
  }
};

export default safeBarterReducer;
