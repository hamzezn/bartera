import * as actionTypes from "../actions/actionTypes";

const initialState = {
  result: null,
  loading: false,
  error: null,

  // search query
  search: null
};

const swapOffersReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SWAP_OFFERS_START:
      return { ...state, loading: true, result: null, error: null };
    case actionTypes.SWAP_OFFERS_SUCCESS:
      return { ...state, loading: false, result: action.result, error: null };
    case actionTypes.SWAP_OFFERS_FAIL:
      return { ...state, loading: false, error: action.error };

    // filters
    case actionTypes.SWAP_OFFERS_SET_SEARCH:
      return { ...state, search: action.search };

    default:
      return state;
  }
};

export default swapOffersReducer;
