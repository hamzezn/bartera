import * as actionTypes from "../actions/actionTypes";

const initialState = {
  result: null,
  loading: false,
  error: null,
  actionLoading: false
};

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.PROFILE_START:
      return { ...state, loading: true, result: null, error: null };
    case actionTypes.PROFILE_SUCCESS:
      return { ...state, loading: false, result: action.result, error: null };
    case actionTypes.PROFILE_FAIL:
      return { ...state, loading: false, error: action.error };
    case actionTypes.PROFILE_ACTION_LOADING:
      return { ...state, actionLoading: action.loading };
    default:
      return state;
  }
};

export default profileReducer;
