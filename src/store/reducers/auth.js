import * as actionTypes from "../actions/actionTypes";

const initialState = {
  token: localStorage.getItem("token"),
  refreshToken: localStorage.getItem("refreshToken"),
  fullName: localStorage.getItem("fullName"),
  phoneNumber: null,
  error: null,
  loading: false,
  authRedirectPath: "/",
  id: localStorage.getItem("userID")
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_START:
      return {
        ...state,
        error: null,
        loading: true
      };
    case actionTypes.AUTH_SUCCESS:
      return {
        ...state,
        error: null,
        loading: false,
        phoneNumber: action.phoneNumber
      };
    case actionTypes.AUTH_FAIL:
      return {
        ...state,
        error: action.error,
        loading: false
      };
    case actionTypes.VERIFY_SUCCESS:
      return {
        ...state,
        error: null,
        loading: false,
        token: action.token,
        refreshToken: action.refreshToken
      };
    case actionTypes.SET_TOKEN:
      return {
        ...state,
        token: action.token
      };
    case actionTypes.SET_FULL_NAME:
      return {
        ...state,
        fullName: action.fullName
      };
    case actionTypes.SET_USER_ID:
      return {
        ...state,
        id: action.id
      };
    case actionTypes.AUTH_LOGOUT:
      return {
        ...state,
        token: null,
        refreshToken: null
      };
    case actionTypes.SET_AUTH_REDIRECT_PATH:
      return {
        ...state,
        authRedirectPath: action.path
      };
    default:
      return state;
  }
};

export default reducer;
