import * as actionTypes from "../actions/actionTypes";

const initialState = {
  result: null,
  loading: false,
  error: null,
  actionLoading: false
};

const orderReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_ORDERS_START:
      return { ...state, loading: true, result: null, error: null };
    case actionTypes.FETCH_ORDERS_SUCCESS:
      return { ...state, loading: false, result: action.result, error: null };
    case actionTypes.FETCH_ORDERS_FAIL:
      return { ...state, loading: false, error: action.error };
    case actionTypes.ORDERS_ACTION_LOADING:
      return { ...state, actionLoading: action.loading };
    default:
      return state;
  }
};

export default orderReducer;
