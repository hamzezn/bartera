import * as actionTypes from "../actions/actionTypes";

const initialState = {
  result: null,
  loading: false,
  error: null,

  //actions
  actionLoading: false
};

const singleAssetReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_ASSET_START:
      return { ...state, loading: true, result: null, error: null };
    case actionTypes.FETCH_ASSET_SUCCESS:
      return { ...state, loading: false, result: action.result, error: null };
    case actionTypes.FETCH_ASSET_FAIL:
      return { ...state, loading: false, error: action.error };

    // actions
    case actionTypes.SINGLE_ASSET_ACTION_LOADING:
      return { ...state, actionLoading: action.loading };
    default:
      return state;
  }
};

export default singleAssetReducer;
