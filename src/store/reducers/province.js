import * as actionTypes from '../actions/actionTypes';

const initialState = {
    result: null,
    loading: false,
    error: null,
};

const assetReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_PROVINCES_START:
            return { loading: true, result: null, error: null };
        case actionTypes.FETCH_PROVINCES_SUCCESS:
            return { loading: false, result: action.result, error: null };
        case actionTypes.FETCH_PROVINCES_FAIL:
            return { loading: false, result: null, error: action.error };
        default:
            return state;
    }
};

export default assetReducer;