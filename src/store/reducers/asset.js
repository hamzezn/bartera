import * as actionTypes from "../actions/actionTypes";

const initialState = {
  // all assets
  result: null,
  loading: false,
  error: null,
  // filter assets
  city: null,
  category: null,
  has_image: null,
  ordering: null,
  search: null,
  //creating asset
  createLoading: false,
  createResult: null,
  createError: null,
  // actions
  actionLoading: false
};

const assetReducer = (state = initialState, action) => {
  switch (action.type) {
    // all assets
    case actionTypes.FETCH_ASSETS_START:
      return { ...state, loading: true, result: null, error: null };
    case actionTypes.FETCH_ASSETS_SUCCESS:
      return { ...state, loading: false, result: action.result, error: null };
    case actionTypes.FETCH_ASSETS_FAIL:
      return { ...state, loading: false, error: action.error };

    // filters
    case actionTypes.SET_ASSET_CITY:
      return { ...state, city: action.city };
    case actionTypes.SET_ASSET_CATEGORY:
      return { ...state, category: action.category };
    case actionTypes.SET_ASSET_HAS_IMAGE:
      return { ...state, has_image: action.has_image };
    case actionTypes.SET_ASSET_ORDERING:
      return { ...state, ordering: action.ordering };
    case actionTypes.SET_ASSET_SEARCH:
      return { ...state, search: action.search };

    // create
    case actionTypes.ASSET_CREATE_LOADING:
      return {
        ...state,
        createLoading: true,
        createResult: null,
        createError: null
      };
    case actionTypes.ASSET_CREATE:
      return {
        ...state,
        createLoading: false,
        createResult: action.result,
        createError: null
      };
    case actionTypes.ASSET_CREATE_FAIL:
      return {
        ...state,
        createLoading: false,
        createError: action.error
      };

    //actions
    case actionTypes.ASSET_ACTION_LOADING:
      return {
        ...state,
        actionLoading: action.loading
      };
    default:
      return state;
  }
};

export default assetReducer;
