import * as actionTypes from "../actions/actionTypes";

const initialState = {
  result: null,
  loading: false,
  error: null
};

const reasonReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_REASONS_START:
      return { ...state, loading: true, result: null, error: null };
    case actionTypes.FETCH_REASONS_SUCCESS:
      return { ...state, loading: false, result: action.result, error: null };
    case actionTypes.FETCH_REASONS_FAIL:
      return { ...state, loading: false, result: null, error: action.error };
    default:
      return state;
  }
};

export default reasonReducer;
