import * as actionTypes from "../actions/actionTypes";

const initialState = {
  result: null,
  loading: false,
  error: null
};

const statisticReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_STATS_START:
      return { ...state, loading: true, result: null, error: null };
    case actionTypes.FETCH_STATS_SUCCESS:
      return { ...state, loading: false, result: action.result, error: null };
    case actionTypes.FETCH_STATS_FAIL:
      return { ...state, loading: false, result: null, error: action.error };
    default:
      return state;
  }
};

export default statisticReducer;
