import * as actionTypes from "../actions/actionTypes";

const initialState = {
  result: null,
  loading: false,
  error: null,
  actionLoading: false
};

const barterReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_BARTER_START:
      return { ...state, loading: true, result: null, error: null };
    case actionTypes.FETCH_BARTER_SUCCESS:
      return { ...state, loading: false, result: action.result, error: null };
    case actionTypes.FETCH_BARTER_FAIL:
      return { ...state, loading: false, error: action.error };
    case actionTypes.FETCH_ACTION_BARTER_START:
      return { ...state, actionLoading: action.loading };
    default:
      return state;
  }
};

export default barterReducer;
