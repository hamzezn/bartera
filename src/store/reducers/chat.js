import * as actionTypes from "../actions/actionTypes";

const initialState = {
  result: null,
  loading: false,
  error: null,
  actionLoading: false,
  singleResult: null,
  singleLoading: false,
  singleError: null
};

const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_CHATS_START:
      return { ...state, loading: true, result: null, error: null };
    case actionTypes.GET_CHATS_SUCCESS:
      return { ...state, loading: false, result: action.result, error: null };
    case actionTypes.GET_CHATS_FAIL:
      return { ...state, loading: false, error: action.error };
    case actionTypes.CHATS_ACTION_LOADING:
      return { ...state, actionLoading: action.loading };

    case actionTypes.SINGLE_CHAT_START:
      return {
        ...state,
        singleLoading: true,
        singleResult: null,
        singleError: null
      };
    case actionTypes.SINGLE_CHAT_SUCCESS:
      return {
        ...state,
        singleLoading: false,
        singleResult: action.result,
        singleError: null
      };
    case actionTypes.SINGLE_CHAT_FAIL:
      return { ...state, singleLoading: false, singleError: action.error };
    default:
      return state;
  }
};

export default chatReducer;
