import * as actionTypes from "../actions/actionTypes";

const initialState = {
  result: null,
  loading: false,
  error: null,

  subResult: null,
  subLoading: false,
  subError: null
};

const categoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_CATEGORY_START:
      return { loading: true, result: null, error: null };
    case actionTypes.FETCH_CATEGORY_SUCCESS:
      return { loading: false, result: action.result, error: null };
    case actionTypes.FETCH_CATEGORY_FAIL:
      return { loading: false, result: null, error: action.error };

    case actionTypes.FETCH_SUB_CATEGORY_START:
      return { subLoading: true, subResult: null, subError: null };
    case actionTypes.FETCH_SUB_CATEGORY_SUCCESS:
      return { subLoading: false, subResult: action.result, subError: null };
    case actionTypes.FETCH_SUB_CATEGORY_FAIL:
      return { subLoading: false, subResult: null, subError: action.error };

    default:
      return state;
  }
};

export default categoryReducer;
