import React, { useEffect } from "react";

import { makeStyles } from "@material-ui/styles";
import { Link } from "react-router-dom";

import BaseLayout from "../layouts/BaseLayout";

// components
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import ListGroup from "react-bootstrap/ListGroup";
import LoadingBox from "../components/LoadingBox";

// redux
import { connect } from "react-redux";
import * as actions from "../store/actions/index";

const useStyles = makeStyles({
  root: {
    minHeight: "75vh",
    marginTop: 100,
    maxWidth: 500,
    marginRight: "auto",
    marginLeft: "auto"
  },
  item: {
    paddingTop: 15,
    paddingBottom: 15,
    verticalAlign: "middle",
    color: "#007600"
  }
});

const ChooseCategoryPage = ({ result, loading, getCategories, location }) => {
  const classes = useStyles();
  let slug = null;
  if (location && location.state) slug = location.state.slug;

  useEffect(() => {
    getCategories();
  }, [getCategories]);

  return (
    <BaseLayout>
      <Container className={classes.root}>
        <Col xs={12} className="border rounded px-4 py-5 bg-white">
          <h6 className="mb-3">لطفا دسته‌بندی مورد نظر خود را انتخاب کنید</h6>
          {!loading && result ? (
            <ListGroup>
              {result.map((category) => {
                return (
                  <ListGroup.Item
                    key={category.id}
                    action
                    className={classes.item}
                    as={Link}
                    to={
                      category.children.length
                        ? {
                            pathname: `/choose_subcategory/${category.id}`,
                            state: { rootParent: category.title, slug }
                          }
                        : {
                            pathname: slug
                              ? `/advertise_edit/${slug}`
                              : `/advertise_registration/`,
                            state: {
                              rootParent: category.title,
                              category
                            }
                          }
                    }
                  >
                    {category.title}
                  </ListGroup.Item>
                );
              })}
            </ListGroup>
          ) : (
            <LoadingBox />
          )}
        </Col>
      </Container>
    </BaseLayout>
  );
};

const mapStateToProps = (state) => {
  return {
    result: state.category.result,
    loading: state.category.loading
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCategories: () => dispatch(actions.getCategories())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChooseCategoryPage);
