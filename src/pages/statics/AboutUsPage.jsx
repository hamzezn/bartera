import React, { useEffect } from "react";

// redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// actions
import { getAboutUsContent } from "../../store/actions";

import BaseLayout from "../../layouts/BaseLayout";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import LoadingBox from "../../components/LoadingBox";

const AboutUsPage = ({ aboutUs, getAboutUsContent }) => {
  useEffect(() => {
    getAboutUsContent();
  }, [getAboutUsContent]);

  return (
    <BaseLayout>
      <Container className="my-4 text-right">
        <Row>
          <Col className="col-12">
            <h4 className="text-purple font-weight-bolder mb-2">درباره ما</h4>
          </Col>
          <Col className="col-12 bg-white py-4">
            {!aboutUs.loading && aboutUs.result ? (
              <div
                dangerouslySetInnerHTML={{ __html: aboutUs.result?.content }}
              />
            ) : (
              <LoadingBox />
            )}
          </Col>
        </Row>
      </Container>
    </BaseLayout>
  );
};

const mapStateToProps = ({ aboutUs }) => ({ aboutUs });

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getAboutUsContent
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(AboutUsPage);
