import React, { useEffect } from "react";

// redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// actions
import { getTermsContent } from "../../store/actions";

import BaseLayout from "../../layouts/BaseLayout";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import LoadingBox from "../../components/LoadingBox";

const TermsPage = ({ terms, getTermsContent }) => {
  useEffect(() => {
    getTermsContent();
  }, [getTermsContent]);
  return (
    <BaseLayout>
      <Container className="my-4 text-right">
        <Row>
          <Col className="col-12">
            <h4 className="text-purple font-weight-bolder mb-2">
              شرایط و قوانین
            </h4>
          </Col>
          <Col className="col-12 bg-white py-4">
            {!terms.loading && terms.result ? (
              <div
                dangerouslySetInnerHTML={{ __html: terms.result?.content }}
              />
            ) : (
              <LoadingBox />
            )}
          </Col>
        </Row>
      </Container>
    </BaseLayout>
  );
};

const mapStateToProps = ({ terms }) => ({ terms });

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getTermsContent
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(TermsPage);
