import React, { useEffect } from "react";

// redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// actions
import { getPrivacyContent } from "../../store/actions";

import BaseLayout from "../../layouts/BaseLayout";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import LoadingBox from "../../components/LoadingBox";

const PrivacyPage = ({ privacy, getPrivacyContent }) => {
  useEffect(() => {
    getPrivacyContent();
  }, [getPrivacyContent]);
  return (
    <BaseLayout>
      <Container className="my-4 text-right">
        <Row>
          <Col className="col-12">
            <h4 className="text-purple font-weight-bolder mb-2">حریم خصوصی</h4>
          </Col>
          <Col className="col-12 bg-white py-4">
            {!privacy.loading && privacy.result ? (
              <div
                dangerouslySetInnerHTML={{ __html: privacy.result?.content }}
              />
            ) : (
              <LoadingBox />
            )}
          </Col>
        </Row>
      </Container>
    </BaseLayout>
  );
};

const mapStateToProps = ({ privacy }) => ({ privacy });

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getPrivacyContent
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(PrivacyPage);
