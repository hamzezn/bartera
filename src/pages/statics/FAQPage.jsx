import React, { useEffect } from "react";

// redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// actions
import { getFAQContent } from "../../store/actions";

// components
import BaseLayout from "../../layouts/BaseLayout";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import LoadingBox from "../../components/LoadingBox";

const FAQPage = ({ FAQ, getFAQContent }) => {
  useEffect(() => {
    getFAQContent();
  }, [getFAQContent]);

  return (
    <BaseLayout>
      <Container className="my-4 text-right">
        <Row>
          <Col className="col-12">
            <h4 className="text-purple font-weight-bolder mb-2">
              سوالات متداول
            </h4>
          </Col>
          <Col className="col-12 bg-white py-4">
            {!FAQ.loading && FAQ.result ? (
              <div dangerouslySetInnerHTML={{ __html: FAQ.result?.content }} />
            ) : (
              <LoadingBox />
            )}
          </Col>
        </Row>
      </Container>
    </BaseLayout>
  );
};

const mapStateToProps = ({ FAQ }) => ({ FAQ });

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getFAQContent
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(FAQPage);
