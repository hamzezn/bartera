import React, { useEffect } from "react";

// redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// actions
import { getSafeBarterContent } from "../../store/actions";

import BaseLayout from "../../layouts/BaseLayout";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import LoadingBox from "../../components/LoadingBox";

const SafeSwapPage = ({ safeBarter, getSafeBarterContent }) => {
  useEffect(() => {
    getSafeBarterContent();
  }, [getSafeBarterContent]);
  return (
    <BaseLayout>
      <Container className="my-4 text-right">
        <Row>
          <Col className="col-12">
            <h4 className="text-purple font-weight-bolder mb-2">معاوضه امن</h4>
          </Col>
          <Col className="col-12 bg-white py-4">
            {!safeBarter.loading && safeBarter.result ? (
              <div
                dangerouslySetInnerHTML={{ __html: safeBarter.result?.content }}
              />
            ) : (
              <LoadingBox />
            )}
          </Col>
        </Row>
      </Container>
    </BaseLayout>
  );
};

const mapStateToProps = ({ safeBarter }) => ({ safeBarter });

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getSafeBarterContent
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(SafeSwapPage);
