import React from "react";

import { Link } from "react-router-dom";

import BaseLayout from "../../layouts/BaseLayout";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import img1 from "../../img/404_icon2.png";

const NotFoundPage = () => {
  return (
    <BaseLayout>
      <Container className="my-5">
        <Row className="justify-content-around align-items-center">
          <Col xs={12} md={5} className="text-right">
            <h1 className="text-purple font-weight-bolder display-4 mb-2">
              ۴۰۴
            </h1>
            <h6 className="mb-3">متاسفانه خطائی رخ داده است!</h6>
            <p className="mb-4 small text-dark">
              صفحه‌ای که شما به دنبال آن هستید یا پاک شده است و یا اینکه آدرس
              صفحه مورد نظر را اشتباه وارد کرده‌اید.
            </p>
            <Link className="btn btn_green" to="/">
              صفحه اصلی
            </Link>
          </Col>
          <Col xs={12} md={6}>
            <img className="img-fluid" src={img1} alt="404_Error" width="500" />
          </Col>
        </Row>
      </Container>
    </BaseLayout>
  );
};

export default NotFoundPage;
