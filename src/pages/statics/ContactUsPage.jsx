import React, { useEffect } from "react";

// redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// actions
import { getContactUsContent } from "../../store/actions";

import BaseLayout from "../../layouts/BaseLayout";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import LoadingBox from "../../components/LoadingBox";

const ContactUsPage = ({ contactUs, getContactUsContent }) => {
  useEffect(() => {
    getContactUsContent();
  }, [getContactUsContent]);
  return (
    <BaseLayout>
      <Container className="my-4 text-right">
        <Row>
          <Col className="col-12">
            <h4 className="text-purple font-weight-bolder mb-2">تماس با ما</h4>
          </Col>
          <Col className="col-12 bg-white py-4">
            {!contactUs.loading && contactUs.result ? (
              <div
                dangerouslySetInnerHTML={{ __html: contactUs.result?.content }}
              />
            ) : (
              <LoadingBox />
            )}
          </Col>
        </Row>
      </Container>
    </BaseLayout>
  );
};

const mapStateToProps = ({ contactUs }) => ({ contactUs });

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getContactUsContent
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactUsPage);
