import React, { useEffect } from "react";

// redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// actions
import { getBarterStoryContent } from "../../store/actions";

// components
import BaseLayout from "../../layouts/BaseLayout";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import LoadingBox from "../../components/LoadingBox";

const BarterStoryPage = ({ barterStory, getBarterStoryContent }) => {
  useEffect(() => {
    getBarterStoryContent();
  }, [getBarterStoryContent]);

  return (
    <BaseLayout>
      <Container className="my-4 text-right">
        <Row>
          <Col className="col-12">
            <h4 className="text-purple font-weight-bolder mb-2">
              داستان تهاتر
            </h4>
          </Col>
          <Col className="col-12 bg-white py-4">
            {!barterStory.loading && barterStory.result ? (
              <div
                dangerouslySetInnerHTML={{
                  __html: barterStory.result?.content
                }}
              />
            ) : (
              <LoadingBox />
            )}
          </Col>
        </Row>
      </Container>
    </BaseLayout>
  );
};

const mapStateToProps = ({ barterStory }) => ({ barterStory });

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getBarterStoryContent
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(BarterStoryPage);
