import React, { useEffect } from "react";

// redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// actions
import { getManualContent } from "../../store/actions";

import BaseLayout from "../../layouts/BaseLayout";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import LoadingBox from "../../components/LoadingBox";

const ManualPage = ({ manual, getManualContent }) => {
  useEffect(() => {
    getManualContent();
  }, [getManualContent]);
  return (
    <BaseLayout>
      <Container className="my-4 text-right">
        <Row>
          <Col className="col-12">
            <h4 className="text-purple font-weight-bolder mb-2">
              راهنمای استفاده
            </h4>
          </Col>
          <Col className="col-12 bg-white py-4">
            {!manual.loading && manual.result ? (
              <div
                dangerouslySetInnerHTML={{ __html: manual.result?.content }}
              />
            ) : (
              <LoadingBox />
            )}
          </Col>
        </Row>
      </Container>
    </BaseLayout>
  );
};

const mapStateToProps = ({ manual }) => ({ manual });

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getManualContent
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ManualPage);
