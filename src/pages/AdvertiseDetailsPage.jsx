import React, { useState, useEffect } from "react";

import { makeStyles } from "@material-ui/styles";
import { Link, useParams, useHistory } from "react-router-dom";

import Swiper from "react-id-swiper";
import "swiper/css/swiper.css";

// toast notifications
import { toast } from "react-toastify";

import BaseLayout from "../layouts/BaseLayout";

// map component
import ShowMap from "../components/MyMap/ShowMap";

//helpers
import { getLocation } from "../helpers";

// redux
import { connect } from "react-redux";
import * as actions from "../store/actions/index";

// components
import ReportAdvertiseModal from "../components/Modals/ReportAdvertiseModal";
import SwapRequestModal from "../components/Modals/SwapRequestModal";
import Breadcrumb from "../components/Breadcrumb";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Tooltip from "../components/Tooltip";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import LoadingBox from "../components/LoadingBox";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFlag, faBookmark } from "@fortawesome/free-solid-svg-icons";
import { faBookmark as faBookmarkRegular } from "@fortawesome/free-regular-svg-icons";

// images
import avatar from "../img/avatar2.png";
import noImage from "../img/404.png";

const useStyles = makeStyles({
  galleryContainer: {
    marginRight: "1rem"
  },
  galleryWrapper: {
    width: "100%",
    maxHeight: 330
  },
  gallerySlide: {
    flexShrink: 0,
    position: "relative",
    transitionProperty: "transform",
    width: "100%",
    height: 330,
    marginRight: 10,
    borderRadius: 5,
    textAlign: "center",
    paddingTop: 5,
    paddingBottom: 5,
    "& img": {
      maxWidth: "95%",
      height: "100%"
    }
  },
  thumbnailSlide: {
    height: 80,
    width: 100,
    marginTop: 10,
    border: "1px solid #b2beba",
    textAlign: "center",
    cursor: "pointer",
    "& img": {
      maxHeight: "100%",
      maxWidth: "100%"
    }
  },
  bookmarkIcon: {
    cursor: "pointer",
    "&:hover": {
      color: "#005e38"
    }
  },
  reportAndBookmarkIcon: {
    position: "absolute",
    top: 5,
    right: 0,
    zIndex: 10
  },
  map: {
    width: "100%",
    border: 0,
    height: 400,
    "& > .leaflet-container": {
      height: "100%"
    }
  },
  callToAction: {
    marginTop: 110,
    textAlign: "center"
  },
  avatar: {
    width: 50,
    height: 50,
    backgroundColor: "#fcae91",
    borderRadius: "50%"
  },
  card: {
    "&:hover": {
      boxShadow: "0px 0px 10px 3px rgba(235,235,235,1)",
      transition: ".2s all",
      zIndex: 10
    }
  },
  imgContainer: {
    height: 220,
    width: 220,
    borderRadius: 0,
    position: "relative",
    margin: "auto"
  },
  cardImgTop: {
    maxHeight: 190,
    maxWidth: 190,
    borderRadius: 0,
    position: "absolute",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)"
  }
});

const AdvertiseDetailsPage = ({
  result,
  loading,
  getSingleAsset,
  reportAsset,
  addAssetToFavorite,
  removeFromFavorite,
  getMyAssets,
  auth,
  userID,
  addToChat,
  chat
}) => {
  let history = useHistory();
  const { slug } = useParams();

  useEffect(() => {
    getSingleAsset(slug);
  }, [getSingleAsset, slug]);

  const [myAssetsRes, setMyAssetsRes] = useState(null);
  useEffect(() => {
    if (auth) {
      getMyAssets(1, {
        onSuccess: res => {
          setMyAssetsRes(res);
        }
      });
    }
  }, [auth, getMyAssets]);

  const classes = useStyles();
  const [openReportModal, setOpenReportModal] = useState(false);
  const [openSwapModal, setOpenSwapModal] = useState(false);

  const handleOpenReportModal = () => {
    setOpenReportModal(true);
  };

  const handleCloseReportModal = () => {
    setOpenReportModal(false);
  };

  const handleOpenSwapModal = () => {
    if (!auth) {
      toast.error("برای معاوضه ابتدا باید دارایی خود را در باترا ثبت نمایید");
      return;
    }
    if (myAssetsRes && myAssetsRes.count > 0) setOpenSwapModal(true);
    else {
      toast.warn("شما هنوز هیچ آگهی ثبت نکرده اید");
      return;
    }
  };

  const handleCloseSwapModal = () => {
    setOpenSwapModal(false);
  };

  const reportAction = (slug, reason) => {
    reportAsset(slug, reason);
    handleCloseReportModal();
  };

  const [gallerySwiper, getGallerySwiper] = useState(null);
  const [thumbnailSwiper, getThumbnailSwiper] = useState(null);
  const gallerySwiperParams = {
    getSwiper: getGallerySwiper,
    spaceBetween: 10,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    }
  };
  const thumbnailSwiperParams = {
    getSwiper: getThumbnailSwiper,
    slideClass: classes.thumbnailSlide,
    spaceBetween: 10,
    centeredSlides: true,
    slidesPerView: "auto",
    touchRatio: 0.2,
    slideToClickedSlide: true
  };
  useEffect(() => {
    if (
      gallerySwiper !== null &&
      gallerySwiper.controller &&
      thumbnailSwiper !== null &&
      thumbnailSwiper.controller
    ) {
      gallerySwiper.controller.control = thumbnailSwiper;
      thumbnailSwiper.controller.control = gallerySwiper;
    }
  }, [gallerySwiper, thumbnailSwiper]);

  const params = {
    slidesPerView: 4,
    spaceBetween: 20,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    breakpoints: {
      1024: {
        slidesPerView: 4,
        spaceBetween: 20
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 15
      },
      640: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      320: {
        slidesPerView: 1,
        spaceBetween: 10
      }
    }
  };

  const handleMessageButton = slug => {
    if (slug)
      addToChat(slug, {
        onSuccess: room => {
          if (room.is_blocked)
            toast.warn("این گفتگو توسط شما یا مخاطب شما مسدود شده است");
          else
            history.push({
              pathname: "/dashboard/messages",
              state: { room }
            });
        }
      });
  };

  return (
    <BaseLayout>
      {!loading && result ? (
        <React.Fragment>
          <Container className="my-4">
            {/* breadcrumb  */}
            <Breadcrumb
              category={result?.category}
              category_id={result?.category_id}
              category_parents={result?.category_parents}
            />

            {/* header */}
            <Row>
              <h6 className="col-12 py-3 my-0 border bg-white">
                {result.title}
              </h6>
            </Row>
            {/* /header */}
            <Row className="justify-content-between bg-white border py-5">
              <Col className="col-12" md={7} lg={6}>
                <div className="pb-lg-3 mb-lg-3">
                  <Swiper {...gallerySwiperParams}>
                    {result?.images?.length > 0 ? (
                      result.images.map((image, index) => {
                        return (
                          <div className={classes.gallerySlide} key={index}>
                            <img src={image.image} alt={`slide-${index + 1}`} />
                          </div>
                        );
                      })
                    ) : (
                      <div className={classes.gallerySlide}>
                        <img src={noImage} alt="slide-404" />
                      </div>
                    )}
                  </Swiper>
                  <Swiper {...thumbnailSwiperParams}>
                    {result?.images?.length > 0 ? (
                      result.images.map((image, index) => {
                        return (
                          <img
                            src={image.thumbnail}
                            alt={`thumbnail-${index + 1}`}
                            key={index}
                          />
                        );
                      })
                    ) : (
                      <img src={noImage} alt="thumbnail-404" />
                    )}
                  </Swiper>

                  {/* bookmark & report for desktop view  */}
                  {auth && (
                    <div
                      className={`${classes.reportAndBookmarkIcon} mr-2 mr-lg-3 d-none d-lg-block`}
                    >
                      <Tooltip
                        placement="top"
                        id="reportAdvertise"
                        title="گزارش تخلف"
                      >
                        <FontAwesomeIcon
                          icon={faFlag}
                          size="lg"
                          className={`${classes.bookmarkIcon} d-lg-block mb-2`}
                          onClick={handleOpenReportModal}
                        />
                      </Tooltip>

                      {/* report modal: */}
                      <ReportAdvertiseModal
                        show={openReportModal}
                        handleClose={handleCloseReportModal}
                        loading={loading}
                        action={reportAction}
                        slug={slug}
                      />

                      {result.is_favorite ? (
                        <Tooltip
                          placement="top"
                          id="unbookmark"
                          title="شما به این آگهی علاقه‌مندید"
                        >
                          <FontAwesomeIcon
                            icon={faBookmark}
                            size="lg"
                            className={`${classes.bookmarkIcon} d-lg-block mb-2`}
                            onClick={() => removeFromFavorite(slug)}
                          />
                        </Tooltip>
                      ) : (
                        <Tooltip
                          placement="top"
                          id="bookmark"
                          title="افزودن به علاقه‌مندی‌ها"
                        >
                          <FontAwesomeIcon
                            icon={faBookmarkRegular}
                            size="lg"
                            className={`${classes.bookmarkIcon} d-lg-block mb-2`}
                            onClick={() => addAssetToFavorite(slug)}
                          />
                        </Tooltip>
                      )}
                    </div>
                  )}
                  {/* /bookmark & report for desktop view  */}
                </div>

                {/* advertise location  */}
                <h6 className="small mb-2 mt-2 font-weight-bold">مکان آگهی</h6>
                <div className={classes.map}>
                  <ShowMap
                    center={
                      result.coordinates
                        ? getLocation(result.coordinates)
                        : [33.483721, 48.353323]
                    }
                    hasLocation={result.coordinates ? true : false}
                  />
                </div>
                {/* /advertise location  */}
              </Col>

              {/* advertise info  */}
              <Col md={5} lg={6} className="col-12 px-4 mt-4 mt-md-0">
                {/* category  */}
                <p className="small font-weight-bold">دسته‌بندی</p>
                {result?.category_parents?.map(item => (
                  <Button
                    key={item.id}
                    className="tag_btn mb-2 mx-1"
                    as={Link}
                    to={{
                      pathname: "/",
                      state: { cat_id: item.id }
                    }}
                  >
                    {item.title}
                  </Button>
                ))}

                <Button
                  className="tag_btn mb-2 mx-1"
                  as={Link}
                  to={{ pathname: "/", state: { cat_id: result?.category_id } }}
                >
                  {result?.category}
                </Button>

                {/* /category  */}

                {/* dynamic fields */}
                <Row className="mt-3">
                  {result?.properties?.map(item => (
                    <Col lg={6} key={item.id}>
                      <p className="small font-weight-bold mt-3 mb-2">
                        {item.title}
                      </p>
                      <Row>
                        <Col md={7} xs={8}>
                          {item.fields.map(field => {
                            return (
                              <p
                                className="small mb-2 text-muted"
                                key={field.id}
                                style={{ width: "15em" }}
                              >
                                {`${field.title}:`}
                              </p>
                            );
                          })}
                        </Col>
                        <Col md={5} xs={4}>
                          {item.fields.map(field => {
                            return (
                              <p
                                className="small mb-2 text-dark"
                                key={field.id}
                              >
                                {field.type === "checkbox"
                                  ? field.defaultValue
                                    ? "بله"
                                    : "خیر"
                                  : field.defaultValue === ""
                                  ? "--"
                                  : field.defaultValue}
                              </p>
                            );
                          })}
                        </Col>
                      </Row>
                    </Col>
                  ))}
                </Row>
                {/* /dynamic fields */}

                {/* extra description  */}
                <p className="small font-weight-bold mt-4 mb-2">توضیحات</p>
                <p className="small text_dark">{result.description}</p>
                {/* /extra description  */}

                {/* swap choices  */}
                <div className="mb-5">
                  <p className="small font-weight-bold mt-4 mb-2">
                    معاوضه میکنم با
                  </p>
                  {result?.swap_offers?.length > 0 ? (
                    result.swap_offers.map((item, index) => (
                      <p className="small mb-2" key={index}>
                        {item.title}
                      </p>
                    ))
                  ) : (
                    <p className="small mb-2">هیچ مورد معاوضه ای ندارد</p>
                  )}
                </div>
                {/* /swap choices  */}

                {/* call to action  */}
                <div className="mb-5">
                  <div className={classes.callToAction}>
                    <Row className="align-items-center justify-content-center">
                      <Tooltip
                        placement="top"
                        id="ad-owner"
                        title="صاحب این آگهی"
                      >
                        <Link
                          to={`/other_profiles/${result.owner.id}`}
                          className="d-flex align-items-center mb-3 ml-4"
                          style={{ textDecoration: "none" }}
                        >
                          <img
                            className={classes.avatar}
                            src={
                              result.owner.avatar_thumbnail
                                ? result.owner.avatar_thumbnail
                                : avatar
                            }
                            alt="avatar"
                          />
                          <p className="mr-2 pt-3 small text_dark">
                            {result.owner ? result.owner.name : null}
                          </p>
                        </Link>
                      </Tooltip>
                      {/* address */}
                      <p className="small mx-1">
                        {result.city ? result.city : null}
                      </p>
                      {/* /address */}
                    </Row>

                    {auth && userID !== result.owner.id && (
                      <Tooltip
                        placement="top"
                        id="msgbtn"
                        title="پیامی به صاحب آگهی ارسال کنید"
                      >
                        <Button
                          className="btn_outline_green my-1 ml-1"
                          style={{ width: 120 }}
                          onClick={() => handleMessageButton(result?.slug)}
                          disabled={chat?.actionLoading}
                        >
                          ارسال پیام
                        </Button>
                      </Tooltip>
                    )}

                    {userID !== result.owner.id && (
                      <Button
                        className="btn_green small"
                        style={{ width: 120 }}
                        onClick={handleOpenSwapModal}
                      >
                        درخواست معاوضه
                      </Button>
                    )}

                    {userID === result.owner.id && (
                      <Button
                        className="btn_green small"
                        style={{ width: 120 }}
                        as={Link}
                        to={`/advertise_edit/${result?.slug}`}
                      >
                        ویرایش آگهی
                      </Button>
                    )}

                    {/* swap modal: */}
                    <SwapRequestModal
                      show={openSwapModal}
                      handleClose={handleCloseSwapModal}
                      requested={result ? result.slug : null}
                    />
                  </div>
                </div>

                {/* bookmark & report for mobile view  */}

                {auth && (
                  <div className="col-12 d-flex justify-content-center mt-4 d-lg-none">
                    <Button
                      variant="outline-secondary"
                      className="btn_outline_orange_small ml-2"
                      onClick={handleOpenReportModal}
                    >
                      <FontAwesomeIcon icon={faFlag} className="ml-1" />
                      گزارش تخلف
                    </Button>
                    {result.is_favorite ? (
                      <Button
                        className="btn_outline_green_small"
                        onClick={() => removeFromFavorite(slug)}
                      >
                        <FontAwesomeIcon icon={faBookmark} className="ml-1" />
                        حذف از علاقه‌مندی
                      </Button>
                    ) : (
                      <Button
                        className="btn_outline_green_small ml-2"
                        onClick={() => addAssetToFavorite(slug)}
                      >
                        <FontAwesomeIcon
                          icon={faBookmarkRegular}
                          className="ml-1"
                        />
                        افزودن به علاقه‌مندی
                      </Button>
                    )}
                  </div>
                )}

                {/* /bookmark & report for mobile view  */}
              </Col>
            </Row>
          </Container>

          {/* similar advertisements  */}
          <Container>
            {/* header  */}
            {result?.related_items?.length > 0 && (
              <Row className="justify-content-between px-3">
                <h6>آگهی‌های مشابه</h6>
                <Link to="##" className="text-success small">
                  مشاهده همه
                </Link>
              </Row>
            )}

            {/* /header  */}
            {result?.related_items?.length > 0 && (
              <div className="bg-white border py-3 px-3">
                <Swiper {...params}>
                  {result.related_items.map((item, index) => {
                    return (
                      <Card key={index} className={classes.card}>
                        <div className={classes.imgContainer}>
                          <Link to={`/advertise_details/${item.slug}`}>
                            <img
                              className={classes.cardImgTop}
                              src={item.thumbnail || noImage}
                              alt="book"
                            />
                          </Link>
                        </div>
                        <Card.Body>
                          <div className="mt-1">
                            <Link
                              className="text_dark small"
                              to={`/advertise_details/${item.slug}`}
                            >
                              <h6 className="mb-4">{item.title}</h6>
                            </Link>
                          </div>
                          <div className="d-flex align-items-center">
                            <Link to={`/other_profiles/${item.owner.id}`}>
                              <img
                                className={classes.avatar}
                                src={item.owner.avatar_thumbnail || avatar}
                                alt="avatar"
                              />
                            </Link>

                            <Link
                              to={`/other_profiles/${item.owner.id}`}
                              className="mr-2 pt-3 small"
                            >
                              {item.owner.name}
                            </Link>

                            <span
                              className="mr-5 pt-3 small"
                              style={{
                                color: "#616161"
                              }}
                            >
                              {item.city_name}
                            </span>
                          </div>
                        </Card.Body>
                      </Card>
                    );
                  })}
                </Swiper>
              </div>
            )}
          </Container>
        </React.Fragment>
      ) : (
        <LoadingBox />
      )}
    </BaseLayout>
  );
};

const mapStateToProps = state => {
  return {
    result: state.singleAsset.result,
    loading: state.singleAsset.loading,
    auth: state.auth.token !== null && state.auth.refreshToken !== null,
    userID: state.auth.id,
    chat: state.chat
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getSingleAsset: (slug, callback) =>
      dispatch(actions.getSingleAsset(slug, callback)),
    reportAsset: (slug, reason) => dispatch(actions.reportAsset(slug, reason)),
    addAssetToFavorite: slug =>
      dispatch(actions.addSingleAssetToFavorite(slug)),
    removeFromFavorite: slug =>
      dispatch(actions.removeSingleFromFavorite(slug)),
    getMyAssets: (page, callback) =>
      dispatch(actions.getMyAssets(page, callback)),
    addToChat: (slug, callback) => dispatch(actions.addToChat(slug, callback))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdvertiseDetailsPage);
