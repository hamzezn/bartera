import React, { useEffect } from "react";

import { makeStyles } from "@material-ui/styles";
import { Link, useParams, Redirect } from "react-router-dom";

import BaseLayout from "../layouts/BaseLayout";

// components
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import ListGroup from "react-bootstrap/ListGroup";
import LoadingBox from "../components/LoadingBox";

// redux
import * as actions from "../store/actions/index";
import { connect } from "react-redux";

const useStyles = makeStyles({
  root: {
    minHeight: "75vh",
    marginTop: 100,
    maxWidth: 500,
    marginRight: "auto",
    marginLeft: "auto"
  },
  item: {
    paddingTop: 15,
    paddingBottom: 15,
    verticalAlign: "middle",
    color: "#007600",
    "&.disabled": {
      textAlign: "center"
    }
  }
});
const ChooseSubCategoryPage = ({
  result,
  loading,
  getSubCategory,
  location
}) => {
  const classes = useStyles();

  const { id } = useParams();

  const { rootParent, slug } = location.state;

  useEffect(() => {
    getSubCategory(id);
  }, [getSubCategory, id]);

  return (
    <BaseLayout>
      <Container className={classes.root}>
        <Col className="col-12 border rounded px-4 py-5 bg-white">
          <h6 className="mb-3">لطفا زیردسته‌ مورد نظر خود را انتخاب کنید</h6>
          {!loading && result ? (
            <ListGroup>
              {result.children.length ? (
                result.children.map((subCategory) => (
                  <ListGroup.Item
                    key={subCategory.id}
                    action
                    className={classes.item}
                    as={Link}
                    to={{
                      pathname: `/choose_subcategory/${subCategory.id}`,
                      state: {
                        rootParent,
                        category: result.title,
                        slug
                      }
                    }}
                  >
                    {subCategory.title}
                  </ListGroup.Item>
                ))
              ) : (
                <Redirect
                  to={{
                    pathname: slug
                      ? `/advertise_edit/${slug}`
                      : `/advertise_registration/`,
                    state: {
                      rootParent,
                      category: result
                    }
                  }}
                />
              )}
            </ListGroup>
          ) : (
            <LoadingBox />
          )}
        </Col>
      </Container>
    </BaseLayout>
  );
};

const mapStateToProps = (state) => {
  return {
    result: state.category.subResult,
    loading: state.category.subLoading
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSubCategory: (id) => dispatch(actions.getSubCategory(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChooseSubCategoryPage);
