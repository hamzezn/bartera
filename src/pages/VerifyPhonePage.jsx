import React, { useState } from "react";

import { makeStyles } from "@material-ui/styles";

import { Redirect } from "react-router-dom";

import { connect } from "react-redux";
import * as actions from "../store/actions/index";

// form validation
import { Formik } from "formik";

// helpers
import { numbersToEnglish } from "../helpers";

// components
import BaseLayout from "../layouts/BaseLayout";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import Timer from "../components/Timer";
import Spinner from "../components/Spinner";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faKey } from "@fortawesome/free-solid-svg-icons";

// styles
const useStyles = makeStyles({
  root: {
    margin: "130px auto",
    maxWidth: 400
  },
  header: {
    textAlign: "center",
    borderBottom: "2px solid #a8cca8",
    padding: "12px 0px"
  },
  inputIcon: {
    fontSize: "1.3rem",
    color: "#b2beba"
  },
  inputGroup: {
    borderRadius: 0,
    borderTopRightRadius: ".25rem !important",
    borderBottomRightRadius: ".25rem !important"
  },
  textInput: {
    fontSize: ".9rem"
  }
});

const VerifyPhonePage = ({
  loading,
  onVerify,
  onLogin,
  isAuthenticated,
  authRedirectPath,
  phoneNumber,
  location
}) => {
  const classes = useStyles();
  const [showTimer, setShowTimer] = useState(true);

  const handleShowTimer = (e) => {
    e.preventDefault();
    onLogin(phoneNumber);
    setShowTimer(true);
  };

  const handleCloseTimer = () => {
    setShowTimer(false);
  };

  const validateCode = (values) => {
    const errors = {};
    let code = numbersToEnglish(values?.code);
    const pattern = /^[0-9]{4}$/;
    if (!code) errors.code = "لطفا کد ۴ رقمی دریافتی را وارد نمائید";
    else if (code && !pattern.test(code)) errors.code = "کد باید 4 رقم باشد";

    // auto submit form if validate is successful
    // stackoverflow is a life saver :)
    if (Object.keys(errors).length === 0 && errors.constructor === Object)
      handleFormSubmit(values);

    return errors;
  };

  const handleFormSubmit = (values) => {
    onVerify(phoneNumber, numbersToEnglish(values.code), location?.state?.from);
  };

  let authRedirect = null;
  if (isAuthenticated) authRedirect = <Redirect to={authRedirectPath} />;

  return (
    <BaseLayout>
      {authRedirect}
      <Container className="my-4">
        <div className={`${classes.root} bg-white pb-4 shadow-sm`}>
          <h5 className={classes.header}>تائید شماره همراه</h5>
          <Formik
            validate={validateCode}
            onSubmit={handleFormSubmit}
            initialValues={{
              code: ""
            }}
          >
            {({ handleSubmit, handleChange, values, errors }) => (
              <Form onSubmit={handleSubmit} className="mt-5 px-4">
                <Form.Group controlId="code">
                  <Form.Label className="float-right small">
                    کد ۴ رقمی دریافتی از طریق پیامک را در کادر زیر وارد نمائید
                  </Form.Label>
                  <InputGroup>
                    <InputGroup.Prepend>
                      <InputGroup.Text className={classes.inputGroup}>
                        <FontAwesomeIcon
                          icon={faKey}
                          className={classes.inputIcon}
                        />
                      </InputGroup.Text>
                    </InputGroup.Prepend>

                    <Form.Control
                      className={classes.textInput}
                      type="tel"
                      placeholder="رمز یک‌بار مصرف دریافتی را وارد نمائيد"
                      name="code"
                      autoComplete="false"
                      value={values.code}
                      onChange={handleChange}
                      isInvalid={!!errors.code}
                    ></Form.Control>
                    <Form.Control.Feedback
                      type="invalid"
                      className="text-right"
                    >
                      {errors.code}
                    </Form.Control.Feedback>
                  </InputGroup>
                  <div className="d-flex justify-content-end mt-1">
                    {showTimer ? (
                      <div className="small">
                        ارسال دوباره رمز در{" "}
                        <Timer handleCloseTimer={handleCloseTimer} /> ثانیه دیگر
                      </div>
                    ) : (
                      <a
                        href="##"
                        className="small text-success"
                        onClick={handleShowTimer}
                      >
                        ارسال دوباره رمز یک‌بار مصرف
                      </a>
                    )}
                  </div>
                </Form.Group>
                <Button
                  className="btn btn_green d-block w-100"
                  type="submit"
                  disabled={loading}
                >
                  تائید {loading ? <Spinner /> : null}
                </Button>
              </Form>
            )}
          </Formik>
        </div>
      </Container>
    </BaseLayout>
  );
};

const mapStateToProps = (state) => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated:
      state.auth.token !== null && state.auth.refreshToken !== null,
    authRedirectPath: state.auth.authRedirectPath,
    phoneNumber: state.auth.phoneNumber,
    token: state.auth.token,
    refreshToken: state.auth.refreshToken
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onVerify: (phoneNumber, code, from) =>
      dispatch(actions.verify(phoneNumber, code, from)),
    onLogin: (phoneNumber) => dispatch(actions.login(phoneNumber))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(VerifyPhonePage);
