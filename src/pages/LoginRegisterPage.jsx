import React, { useState } from "react";

import { makeStyles } from "@material-ui/styles";

import { connect } from "react-redux";
import * as actions from "../store/actions/index";

// toastify
// import { toast } from "react-toastify";

// helpers
import { numbersToEnglish } from "../helpers";

// form validation
import { Formik } from "formik";

import { Redirect, Link } from "react-router-dom";

// components
import BaseLayout from "../layouts/BaseLayout";
import Container from "react-bootstrap/Container";
import Tab from "react-bootstrap/Tab";
import Nav from "react-bootstrap/Nav";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import InputGroup from "react-bootstrap/InputGroup";
import FormCheck from "react-bootstrap/FormCheck";
import Spinner from "../components/Spinner";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPhone, faUser } from "@fortawesome/free-solid-svg-icons";

// styles
const useStyles = makeStyles({
  root: {
    margin: "150px auto",
    maxWidth: 400
  },
  navPills: {
    borderBottom: "2px solid #a8cca8",
    marginBottom: "1rem"
  },
  navLinks: {
    borderRadius: 0,
    textAlign: "center",
    padding: "12px 0px",
    "&.active": {
      backgroundColor: "#159e15",
      color: "white",
      borderRadius: 0,
      "&:hover": {
        backgroundColor: "#007600"
      }
    }
  },
  inputIcon: {
    fontSize: "1.3rem",
    color: "#b2beba"
  },
  inputGroup: {
    borderRadius: 0,
    borderTopRightRadius: ".25rem !important",
    borderBottomRightRadius: ".25rem !important"
  },
  textInput: {
    fontSize: ".9rem"
  },
  checkbox: {
    marginTop: "1rem",
    paddingRight: 0,
    fontSize: ".9rem",
    "& label": {
      cursor: "pointer",
      paddingRight: "1.25rem"
    }
  }
});

const validateLogin = (values) => {
  const errors = {};
  let phone = numbersToEnglish(values?.phone);
  const pattern = /^(\+98|0)?9\d{9}$/;
  if (!phone) errors.phone = "لطفا شماره همراه خود را وارد نمائید";
  else if (phone && !pattern.test(phone)) errors.phone = "شماره صحیح نمی باشد";

  return errors;
};

const validateRegister = (values) => {
  const errors = {};
  let phone = numbersToEnglish(values?.phone);
  let fullName = values?.fullName;
  let rules = values?.rules;
  const pattern = /^(\+98|0)?9\d{9}$/;

  // rules validation
  if (!rules) errors.rules = "باید قوانین سایت را تایید کنید";

  // fullname validation
  if (!fullName) errors.fullName = "لطفا نام و نام خانوادگی خود را وارد نمائید";
  else if (fullName && fullName.length > 30)
    errors.fullName = "حداکثر تا 30 کاراکتر";

  // phone number validation
  if (!phone) errors.phone = "لطفا شماره همراه خود را وارد نمائید";
  else if (phone && !pattern.test(phone)) errors.phone = "شماره صحیح نمی باشد";

  return errors;
};

const LoginRegisterPage = ({
  loading,
  onRegister,
  onLogin,
  isAuthenticated,
  authRedirectPath,
  phoneNumber,
  error
}) => {
  const [confirmation, setConfirmation] = useState({
    status: false,
    from: null
  });

  const classes = useStyles();
  let authRedirect = null;
  if (isAuthenticated) authRedirect = <Redirect to={authRedirectPath} />;
  else if (phoneNumber && confirmation.status)
    authRedirect = (
      <Redirect
        to={{
          pathname: "/verify_phone",
          state: {
            from: confirmation.from
          }
        }}
      />
    );

  return (
    <BaseLayout>
      {authRedirect}
      <Container className="my-4">
        <div className={`${classes.root} bg-white shadow-sm pb-4`}>
          <Tab.Container defaultActiveKey="login">
            <Nav variant="pills" className={classes.navPills}>
              <Nav.Item className="col-6 mx-0 px-0">
                <Nav.Link eventKey="login" className={classes.navLinks}>
                  ورود
                </Nav.Link>
              </Nav.Item>
              <Nav.Item className="col-6 mx-0 px-0">
                <Nav.Link eventKey="register" className={classes.navLinks}>
                  ثبت نام
                </Nav.Link>
              </Nav.Item>
            </Nav>

            <Tab.Content className="px-4">
              <Tab.Pane eventKey="login">
                <Formik
                  validate={validateLogin}
                  onSubmit={(values) => {
                    onLogin(numbersToEnglish(values.phone));
                    setConfirmation({ status: true, from: "login" });
                  }}
                  initialValues={{
                    phone: ""
                  }}
                >
                  {({ handleSubmit, handleChange, values, errors }) => (
                    <Form onSubmit={handleSubmit}>
                      <Form.Group controlId="loginPhoneNumber">
                        <Form.Label className="float-right">
                          شماره همراه
                        </Form.Label>
                        <InputGroup>
                          <InputGroup.Prepend>
                            <InputGroup.Text className={classes.inputGroup}>
                              <FontAwesomeIcon
                                icon={faPhone}
                                className={classes.inputIcon}
                              />
                            </InputGroup.Text>
                          </InputGroup.Prepend>

                          <Form.Control
                            className={classes.textInput}
                            type="tel"
                            placeholder="شماره همراه خود را وارد نمائید"
                            name="phone"
                            autoComplete="false"
                            value={values.phone}
                            onChange={handleChange}
                            isInvalid={!!errors.phone}
                          ></Form.Control>
                          <Form.Control.Feedback
                            type="invalid"
                            className="text-right"
                          >
                            {errors.phone}
                          </Form.Control.Feedback>
                        </InputGroup>
                      </Form.Group>
                      <Button
                        className="btn btn_green d-block w-100"
                        type="submit"
                        disabled={loading}
                      >
                        تائید {loading ? <Spinner /> : null}
                      </Button>
                      <Form.Check
                        className={classes.checkbox}
                        custom
                        type="checkbox"
                        id="custom-checkbox"
                        label="مرا به خاطر بسپار"
                      />
                    </Form>
                  )}
                </Formik>
              </Tab.Pane>
              <Tab.Pane eventKey="register">
                <Formik
                  validate={validateRegister}
                  onSubmit={(values) => {
                    onRegister(values.fullName, numbersToEnglish(values.phone));
                    setConfirmation({ status: true, from: "register" });
                  }}
                  initialValues={{
                    phone: "",
                    fullName: "",
                    rules: false
                  }}
                >
                  {({
                    handleSubmit,
                    handleChange,
                    values,
                    errors,
                    touched
                  }) => (
                    <Form onSubmit={handleSubmit}>
                      <Form.Group controlId="fullName">
                        <Form.Label className="float-right">
                          نام و نام خانوادگی
                        </Form.Label>
                        <InputGroup>
                          <InputGroup.Prepend>
                            <InputGroup.Text className={classes.inputGroup}>
                              <FontAwesomeIcon
                                icon={faUser}
                                className={classes.inputIcon}
                              />
                            </InputGroup.Text>
                          </InputGroup.Prepend>

                          <Form.Control
                            className={classes.textInput}
                            type="text"
                            placeholder="نام و نام خانوادگی خود را وارد نمائید"
                            name="fullName"
                            autoComplete="false"
                            value={values.fullName}
                            onChange={handleChange}
                            isInvalid={!!errors.fullName && !!touched.fullName}
                          ></Form.Control>
                          <Form.Control.Feedback
                            type="invalid"
                            className="text-right"
                          >
                            {errors.fullName}
                          </Form.Control.Feedback>
                        </InputGroup>
                      </Form.Group>
                      <Form.Group controlId="registerPhoneNumber">
                        <Form.Label className="float-right">
                          شماره همراه
                        </Form.Label>
                        <InputGroup>
                          <InputGroup.Prepend>
                            <InputGroup.Text className={classes.inputGroup}>
                              <FontAwesomeIcon
                                icon={faPhone}
                                className={classes.inputIcon}
                              />
                            </InputGroup.Text>
                          </InputGroup.Prepend>

                          <Form.Control
                            className={classes.textInput}
                            type="tel"
                            placeholder="شماره همراه خود را وارد نمائید"
                            name="phone"
                            autoComplete="false"
                            value={values.phone}
                            onChange={handleChange}
                            isInvalid={!!errors.phone && !!touched.phone}
                          ></Form.Control>
                          <Form.Control.Feedback
                            type="invalid"
                            className="text-right"
                          >
                            {errors.phone}
                          </Form.Control.Feedback>
                        </InputGroup>
                      </Form.Group>
                      <Button
                        disabled={loading}
                        className="btn btn_green d-block w-100"
                        type="submit"
                      >
                        تائید {loading ? <Spinner /> : null}
                      </Button>

                      <FormCheck
                        custom
                        id="rules"
                        className={classes.checkbox}
                        feedback={errors.rules}
                      >
                        <FormCheck.Input
                          id="rules"
                          name="rules"
                          checked={values.rules}
                          onChange={handleChange}
                          isInvalid={!!errors.rules && !!touched.rules}
                        />
                        <FormCheck.Label htmlFor="rules">
                          با <Link to="/terms_and_conditions">قوانین سایت</Link>{" "}
                          موافقم
                        </FormCheck.Label>
                      </FormCheck>
                      <span
                        className="text-right small"
                        style={{ color: "#dc3545" }}
                      >
                        {errors?.rules}
                      </span>
                    </Form>
                  )}
                </Formik>
              </Tab.Pane>
            </Tab.Content>
          </Tab.Container>
        </div>
      </Container>
    </BaseLayout>
  );
};

const mapStateToProps = (state) => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated:
      state.auth.token !== null && state.auth.refreshToken !== null,
    authRedirectPath: state.auth.authRedirectPath,
    phoneNumber: state.auth.phoneNumber
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onRegister: (fullName, phoneNumber) =>
      dispatch(actions.register(fullName, phoneNumber)),
    onLogin: (phoneNumber) => dispatch(actions.login(phoneNumber))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginRegisterPage);
