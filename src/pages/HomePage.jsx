import React, { useEffect } from "react";

import { makeStyles } from "@material-ui/styles";

import BaseLayout from "../layouts/BaseLayout";

import history from "../shared/history";

// components
import SearchBox from "../components/SearchBox";
import SideBarFilter from "../components/SideBarFilter";
import Advertisements from "../components/Advertisements";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

const useStyles = makeStyles({
  mainRow: {
    maxWidth: 1400,
    margin: "auto"
  }
});

export default function HomePage() {
  const classes = useStyles();

  useEffect(() => {
    if (history?.location?.state?.cat_id) {
      let state = { ...history.location.state };
      delete state.cat_id;
      history.replace({ ...history.location, state });
    }
  }, []);

  const openSidebar = () => {
    document.getElementById("sidebarFilter").style.width = "100%";
  };

  const closeSidebar = () => {
    document.getElementById("sidebarFilter").style.width = "0";
  };

  return (
    <BaseLayout>
      <SearchBox cat_id={history?.location?.state?.cat_id} />
      <Container fluid className="my-4 pl-0">
        <Row className={classes.mainRow}>
          <SideBarFilter
            closeSidebar={closeSidebar}
            cat_id={history?.location?.state?.cat_id}
          />

          {/* advertisements section  */}
          <Advertisements openSidebar={openSidebar} />
        </Row>
      </Container>
    </BaseLayout>
  );
}
