import React, { useState, useEffect } from "react";

import { makeStyles } from "@material-ui/styles";

import { Link } from "react-router-dom";

// redux
import { connect } from "react-redux";
import {
  getMyAssets,
  deleteMyAsset,
  myAssetBartered,
  myAssetOrders
} from "../../store/actions";

import DashboardLayout from "../../layouts/DashboardLayout";

// components
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Tooltip from "../../components/Tooltip";
import Button from "react-bootstrap/Button";
import Pagination from "../../components/Pagination";
import LoadingBox from "../../components/LoadingBox";

// modals
import DeleteAdvertiseModal from "../../components/Modals/DeleteAdvertiseModal";
import DeactivateAdvertiseModal from "../../components/Modals/DeactivateAdvertiseModal";
import OrdersModal from "../../components/Modals/OrdersModal";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileDownload } from "@fortawesome/free-solid-svg-icons";

// images
import noImage from "../../img/404.png";

const useStyles = makeStyles({
  noAdvertise: {
    height: 250,
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: "3rem 2rem",
    marginLeft: "auto",
    marginRight: "auto"
  },
  card: {
    maxWidth: 400,
    maxHeight: 182,
    transition: ".2s all",
    margin: "0 auto",
    "&:hover": {
      border: "1px solid #A5A2A2"
    },
    "@media only screen and (max-width: 768px)": {
      maxWidth: 400,
      maxHeight: 450
    }
  },
  imgContainer: {
    position: "relative",
    width: 200,
    height: 200,
    "@media only screen and (max-width: 768px)": {
      width: 200,
      height: 230
    }
  },
  btnStatus: {
    width: 100,
    float: "inline-start",
    marginRight: "60%"
  },
  cardImg: {
    position: "absolute",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)",
    maxWidth: "80%",
    maxHeight: "80%",
    "@media only screen and (max-width: 768px)": {
      marginTop: 20,
      maxHeight: 230,
      maxWidth: 180
    }
  },
  dropdown: {
    marginBottom: ".5rem",
    "& button": {
      fontSize: ".7rem",
      padding: ".375rem .75rem"
    },
    "& a": {
      fontSize: ".8rem"
    }
  },
  deleteBtn: {}
});

const MyAdvertisementsPage = ({
  result,
  loading,
  actionLoading,
  getMyAssets,
  deleteMyAsset,
  myAssetBartered,
  myAssetOrders
}) => {
  const classes = useStyles();
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [openDeactivateModal, setOpenDeactivateModal] = useState(false);
  const [openOrdersModal, setOpenOrdersModal] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [slug, setSlug] = useState(null);

  useEffect(() => {
    getMyAssets();
  }, [getMyAssets]);

  const handleOpenDeleteModal = () => {
    setOpenDeleteModal(true);
  };

  const handleCloseDeleteModal = () => {
    setOpenDeleteModal(false);
  };

  const handleOpenDeactivateModal = (e) => {
    e.preventDefault();
    setOpenDeactivateModal(true);
  };

  const handleCloseDeactivateModal = () => {
    setOpenDeactivateModal(false);
  };

  const handleOpenOrdersModal = () => {
    setOpenOrdersModal(true);
  };

  const handleCloseOrdersModal = () => {
    setOpenOrdersModal(false);
  };

  const handleDeleteButton = (slug) => {
    setSlug(slug);
    handleOpenDeleteModal();
  };

  const handleDeleteAction = (slug) => {
    deleteMyAsset(slug, currentPage, {
      onSuccess: () => {
        handleCloseDeleteModal();
        setSlug(null);
      }
    });
  };

  const handleDeactiveButton = (e, slug) => {
    setSlug(slug);
    handleOpenDeactivateModal(e);
  };

  const handleDeactiveAction = (slug) => {
    myAssetBartered(slug, {
      onSuccess: () => {
        handleCloseDeactivateModal();
        setSlug(null);
        setCurrentPage(1);
      }
    });
  };

  const handleOrdersButton = (slug) => {
    handleOpenOrdersModal();
    myAssetOrders(slug);
  };

  let results;
  if (result && result.results) {
    results = result.results.map((item, index) => (
      <Col
        md={6}
        lg={4}
        key={index}
        className="justify-content-center col-12 p-1"
      >
        {item.bartered ? (
          <Card className={classes.card} style={{ opacity: "0.6" }}>
            <Row noGutters>
              <Col md={4} className={`${classes.imgContainer} col-12`}>
                <Link to={`/advertise_details/${item.slug}`}>
                  <Card.Img
                    src={item.thumbnail || noImage}
                    className={classes.cardImg}
                  />
                </Link>
              </Col>
              <Col md={8} className="col-12">
                <Card.Body className="card-body">
                  <Link
                    to={`/advertise_details/${item.slug}`}
                    className="text_dark"
                  >
                    <p className="card-text mt-3 small">{item.title}</p>
                  </Link>
                  <div className="d-flex align-items-center mt-3 mt-xl-4">
                    <p className="mr-2 small disable">معاوضه شده</p>
                  </div>
                  <div className="d-flex justify-content-end mt-2">
                    <Button
                      variant="outline-dark"
                      size="sm"
                      className="px-3"
                      onClick={() => handleDeleteButton(item.slug)}
                    >
                      حذف آگهی
                    </Button>
                  </div>
                </Card.Body>
              </Col>
            </Row>
          </Card>
        ) : (
          <Card className={classes.card}>
            <Row noGutters>
              <Col md={4} className={`${classes.imgContainer} col-12`}>
                <Link to={`/advertise_details/${item.slug}`}>
                  <Card.Img
                    src={item.thumbnail || noImage}
                    className={classes.cardImg}
                  />
                </Link>
              </Col>
              <Col md={8} className="col-12">
                <Card.Body className="card-body">
                  <Link
                    to={`/advertise_details/${item.slug}`}
                    className="text_dark"
                  >
                    <h6 className="card-text mt-3">{item.title}</h6>
                  </Link>
                  <div className="d-flex align-items-center mt-3 mt-xl-4">
                    <button
                      id="dropdown-basic-button"
                      onClick={(e) => handleDeactiveButton(e, item.slug)}
                      style={{
                        width: 100,
                        float: "inline-start",
                        marginRight: "30%",
                        color: "#02abb2",
                        border: "1px solid #02abb2"
                      }}
                      className={`btnStatus btn  btn-sm ${classes.dropdown}`}
                    >
                      معاوضه شده
                    </button>
                  </div>
                  <div className="d-flex justify-content-end mt-2">
                    <Tooltip id="mytool" title="مشاهده سفارشات این آگهی">
                      <button className="btn text-muted">
                        <FontAwesomeIcon
                          icon={faFileDownload}
                          size="lg"
                          onClick={() => handleOrdersButton(item.slug)}
                        />
                      </button>
                    </Tooltip>

                    <Button
                      variant="outline-dark"
                      size="sm"
                      className="px-3 mx-1"
                      onClick={() => handleDeleteButton(item.slug)}
                    >
                      حذف
                    </Button>
                    <Button
                      as={Link}
                      to={`/advertise_edit/${item.slug}`}
                      size="sm"
                      className="px-3"
                    >
                      ویرایش
                    </Button>
                  </div>
                </Card.Body>
              </Col>
            </Row>
          </Card>
        )}
      </Col>
    ));
  }

  return (
    <DashboardLayout>
      <Container className="py-4">
        <Row className="justify-content-center justify-content-md-start">
          <h5 className="text_dark col-12 pb-2 text-center text-md-right px-0 mb-0">
            آگهی‌های من
          </h5>
          {/* no Advertise message  */}
          {result && result.results.length === 0 && (
            <div
              className={`${classes.noAdvertise} col-11 col-md-8 border rounded`}
            >
              <div>
                <Link to="/choose_category" className="text_dark">
                  شما آگهی ثبت شده‌ای ندارید، برای ثبت آگهی کلیک کنید.
                </Link>
              </div>
            </div>
          )}
          {/* /no Advertise message  */}

          {/* my advertise list */}
          {result && !loading ? results : <LoadingBox />}
          {/* /my advertise list */}
        </Row>
        {/* pagination */}
        {!loading && result && (result.next || result.previous) ? (
          <Pagination
            current={currentPage}
            setCurrent={setCurrentPage}
            count={result.count}
            getData={getMyAssets}
          />
        ) : null}
      </Container>

      {/* modals: */}
      <DeleteAdvertiseModal
        show={openDeleteModal}
        handleClose={handleCloseDeleteModal}
        loading={actionLoading}
        action={() => handleDeleteAction(slug)}
      />

      <DeactivateAdvertiseModal
        show={openDeactivateModal}
        handleClose={handleCloseDeactivateModal}
        loading={actionLoading}
        action={() => handleDeactiveAction(slug)}
      />

      <OrdersModal
        show={openOrdersModal}
        handleClose={handleCloseOrdersModal}
      />
    </DashboardLayout>
  );
};

const mapStateToProps = (state) => {
  return {
    result: state.myAsset.result,
    loading: state.myAsset.loading,
    actionLoading: state.myAsset.actionLoading
  };
};

const mapDispatchToProps = {
  getMyAssets,
  deleteMyAsset,
  myAssetBartered,
  myAssetOrders
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyAdvertisementsPage);
