import React, { useState, useEffect } from "react";

import { makeStyles } from "@material-ui/styles";
import { Link } from "react-router-dom";

import DashboardLayout from "../../layouts/DashboardLayout";

// redux
import { connect } from "react-redux";
import * as actions from "../../store/actions/index";

// helpers
import { getPhoneNumber } from "../../helpers";

// components
import RequestCancelModal from "../../components/Modals/RequestCancelModal";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";
import Pagination from "../../components/Pagination";
import LoadingBox from "../../components/LoadingBox";

// images
import avatar from "../../img/avatar1.png";
import noImage from "../../img/404.png";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faExchangeAlt,
  faExclamationCircle
} from "@fortawesome/free-solid-svg-icons";
import {
  faCheckCircle,
  faTimesCircle,
  faQuestionCircle
} from "@fortawesome/free-regular-svg-icons";

const useStyles = makeStyles({
  noRequestMessage: {
    height: 250,
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: "3rem 2rem",
    marginLeft: "auto",
    marginRight: "auto"
  },
  card: {
    minHeight: 310,
    "@media only screen and (max-width: 450px)": {
      height: 330,
      fontSize: ".85rem",
      "& .card-body": {
        padding: "0px 5px"
      }
    }
  },
  imgContainer: {
    width: 120,
    height: 180,
    margin: "0 auto",
    position: "relative",
    display: "flex",
    justifyContent: "center",
    marginTop: ".5rem",
    "@media only screen and (max-width: 450px)": {
      width: 100,
      height: 160
    }
  },
  cardImg: {
    maxWidth: 120,
    maxHeight: 180,
    position: "absolute",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)",
    "@media only screen and (max-width: 450px)": {
      maxWidth: 90,
      maxHeight: 150
    }
  },
  avatar: {
    width: 35,
    height: 35,
    borderRadius: "50%",
    "@media only screen and (max-width: 450px)": {
      width: 25,
      height: 25
    }
  }
});

const MyRequestsPage = ({
  result,
  loading,
  listMyRequests,
  cancelRequest,
  actionLoading
}) => {
  const classes = useStyles();
  const [showCancelModal, setShowCancelModal] = useState(false);
  const [showNumber, setShowNumber] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [id, setId] = useState(null);

  useEffect(() => {
    listMyRequests();
  }, [listMyRequests]);

  const toggleShowNumber = () => {
    setShowNumber(!showNumber);
  };

  const handleOpenCancelModal = () => {
    setShowCancelModal(true);
  };

  const handleCloseCancelModal = () => {
    setShowCancelModal(false);
  };

  const handleCancelButton = (id) => {
    setId(id);
    handleOpenCancelModal();
  };

  const handleCancelAction = (id) => {
    cancelRequest(id, {
      onSuccess: () => {
        handleCloseCancelModal();
        setId(null);
      }
    });
  };

  let results;
  if (result && result.results) {
    results = result.results.map((item, index) => (
      <Col lg={6} className="px-4" key={index}>
        <Row
          className="bg-white pt-3 px-0 px-md-3 shadow-sm pb-0"
          style={{ height: "99%" }}
        >
          <Col className="d-flex align-items-center justify-content-center px-4 px-sm-3 px-md-1">
            <Col xs={6} className="px-1 px-sm-2">
              <Card className={classes.card}>
                <div className={classes.imgContainer}>
                  <Link to={`/advertise_details/${item.applicant_asset.slug}`}>
                    <Card.Img
                      variant="top"
                      className={classes.cardImg}
                      src={item.applicant_asset.thumbnail || noImage}
                      alt="book1"
                    />
                  </Link>
                </div>
                <Card.Body>
                  <div>
                    <Link
                      className="text_dark small"
                      to={`/advertise_details/${item.applicant_asset.slug}`}
                    >
                      <h6>{item.applicant_asset.title}</h6>
                    </Link>
                  </div>
                </Card.Body>
              </Card>
            </Col>
            <FontAwesomeIcon
              icon={faExchangeAlt}
              size="lg"
              style={{ opacity: ".5" }}
            />
            <Col xs={6} className="px-1 px-sm-2">
              <Card className={classes.card}>
                <div className={classes.imgContainer}>
                  <Link to={`/advertise_details/${item.requested_asset.slug}`}>
                    <Card.Img
                      variant="top"
                      className={classes.cardImg}
                      src={item.requested_asset.thumbnail || noImage}
                      alt="book"
                    />
                  </Link>
                </div>
                <Card.Body>
                  <div>
                    <Link
                      className="text_dark small"
                      to={`/advertise_details/${item.requested_asset.slug}`}
                    >
                      <h6>{item.requested_asset.title}</h6>
                    </Link>
                  </div>
                  <div className="d-flex align-items-center">
                    <Link
                      to={`/other_profiles/${item.requested_asset?.owner_id}`}
                    >
                      <img
                        className={classes.avatar}
                        src={
                          item.requested_asset.owner_avatar
                            ? item.requested_asset.owner_avatar
                            : avatar
                        }
                        alt="avatar"
                      />
                    </Link>

                    <Link
                      to={`/other_profiles/${item.requested_asset?.owner_id}`}
                      className="mr-2 pt-3 small"
                    >
                      {item.requested_asset.owner_name}
                    </Link>

                    <span className="mr-5 pt-3 small">
                      {item.requested_asset?.city_name}
                    </span>
                  </div>
                </Card.Body>
              </Card>
            </Col>
          </Col>
          {/* swap messages (4 types) */}
          {/* درخواست شما قبول شده است */}
          {!item.bartered && item.accept === true && (
            <Col xs={12} className="mt-3 px-3 px-sm-2 px-md-0">
              <Alert variant="success" className="pb-3">
                <div className="d-flex">
                  <FontAwesomeIcon
                    icon={faCheckCircle}
                    size="2x"
                    style={{ opacity: ".4" }}
                  />

                  <p className="mr-2 small mt-1">
                    درخواست شما برای معاوضه پذیرفته شد. هم اکنون از دو طریق زیر
                    میتوانید برای انجام معاوضه اقدام نمائید.
                  </p>
                </div>
                <div className="d-flex justify-content-end">
                  <Button
                    as={Link}
                    to="/dashboard/messages"
                    variant="outline-success"
                    size="sm"
                    className="mx-1"
                  >
                    ارسال پیام
                  </Button>
                  <Button
                    variant="success"
                    size="sm"
                    className="mx-1"
                    onClick={toggleShowNumber}
                  >
                    <p className="px-1 py-0 my-0">
                      {showNumber
                        ? getPhoneNumber(item.requested_asset.owner_phone)
                        : "تماس"}
                    </p>
                  </Button>
                </div>
              </Alert>
            </Col>
          )}

          {/* درخواست شما رد شده است */}
          {!item.bartered && item.accept === false && (
            <Col xs={12} className="mt-3 px-3 px-sm-2 px-md-0">
              <Alert variant="warning" className="pb-3">
                <div className="d-flex">
                  <FontAwesomeIcon
                    icon={faTimesCircle}
                    size="2x"
                    style={{ opacity: ".4" }}
                  />

                  <p className="mr-2 small mt-1">
                    درخواست شما برای معاوضه به دلیل زیر رد شد:
                  </p>
                </div>
                <p className="small text-center font-weight-bold">
                  {item.reason ? item.reason : "هیج دلیلی ثبت نشده است"}
                </p>
                <div className="d-flex justify-content-end">
                  <Button
                    variant="warning"
                    size="sm"
                    className="mb-3 mr-2 font-weight-bold"
                    onClick={() => handleCancelButton(item.id)}
                  >
                    لغو درخواست
                  </Button>
                </div>
              </Alert>
            </Col>
          )}

          {/* درخواست شما درانتظار تایید است */}
          {!item.bartered && item.accept === null && (
            <Col xs={12} className="mt-3 px-3 px-sm-2 px-md-0">
              <Alert variant="info" className="pb-3">
                <div className="d-flex">
                  <FontAwesomeIcon
                    icon={faQuestionCircle}
                    size="2x"
                    style={{ opacity: ".4" }}
                  />

                  <p className="mr-2 small mt-2">
                    درخواست شما در انتظار تائید است.
                  </p>
                </div>

                <div className="d-flex justify-content-end align-items-center">
                  <Button
                    variant="info"
                    size="sm"
                    className="mb-3 mr-2 font-weight-bold"
                    onClick={() => handleCancelButton(item.id)}
                  >
                    لغو درخواست
                  </Button>
                </div>
              </Alert>
            </Col>
          )}

          {/* آگهی مدنظر با کاربری دیگر معاوضه شده است */}
          {item.bartered === true && (
            <Col xs={12} className="mt-3 px-3 px-sm-2 px-md-0">
              <Alert variant="dark" className="pb-3">
                <div className="d-flex">
                  <FontAwesomeIcon
                    icon={faExclamationCircle}
                    size="2x"
                    style={{ opacity: ".4" }}
                  />

                  <p className="mr-2 small mt-2">
                    آگهی مد نظر شما برای معاوضه، در حال حاظر معاوضه شده است.
                  </p>
                </div>

                <div className="d-flex justify-content-end align-items-center">
                  <Button
                    variant="dark"
                    size="sm"
                    className="mb-3 mr-2 font-weight-bold"
                    onClick={() => handleCancelButton(item.id)}
                  >
                    لغو درخواست
                  </Button>
                </div>
              </Alert>
            </Col>
          )}
          {/* swap messages */}
        </Row>
      </Col>
    ));
  }

  return (
    <DashboardLayout>
      <Container className="my-4">
        <Row>
          <h5 className="text_dark col-12 pb-2 text-center text-md-right pr-2 mb-2">
            درخواست‌های من
          </h5>
          {/* no Request message */}
          {!loading && result && result.results.length === 0 && (
            <Col
              xs={11}
              md={8}
              className={`${classes.noRequestMessage} border rounded mt-3`}
            >
              <div className="text-center">
                <p>شما در حال حاظر درخواست معاوضه‌ای ارسال نکرده‌اید.</p>
              </div>
            </Col>
          )}
          {/* /no Request message */}

          {result && !loading ? results : <LoadingBox />}
        </Row>

        {/* pagination */}
        {!loading && result && (result.next || result.previous) ? (
          <Pagination
            current={currentPage}
            setCurrent={setCurrentPage}
            count={result.count}
            getData={listMyRequests}
          />
        ) : null}
      </Container>

      {/* modals: */}
      <RequestCancelModal
        show={showCancelModal}
        handleClose={handleCloseCancelModal}
        loading={actionLoading}
        action={() => handleCancelAction(id)}
      />
    </DashboardLayout>
  );
};

const mapStateToProps = (state) => {
  return {
    result: state.barter.result,
    loading: state.barter.loading,
    actionLoading: state.barter.actionLoading
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    listMyRequests: (page) => dispatch(actions.listMyRequests(page)),
    cancelRequest: (id, callback) =>
      dispatch(actions.cancelRequest(id, callback))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyRequestsPage);
