import React, { useState, useEffect } from "react";

import { makeStyles } from "@material-ui/styles";
import { Link } from "react-router-dom";

// redux
import { connect } from "react-redux";
import * as actions from "../../store/actions/index";

// helpers
import { getPhoneNumber } from "../../helpers";

import DashboardLayout from "../../layouts/DashboardLayout";

// components
import RejectRequestModal from "../../components/Modals/RejectRequestModal";
import AcceptRequestModal from "../../components/Modals/AcceptRequestModal";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";
import Pagination from "../../components/Pagination";
import LoadingBox from "../../components/LoadingBox";

// images
import avatar from "../../img/avatar1.png";
import noImage from "../../img/404.png";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExchangeAlt } from "@fortawesome/free-solid-svg-icons";

const useStyles = makeStyles({
  noRequestMessage: {
    height: 250,
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: "3rem 2rem",
    marginLeft: "auto",
    marginRight: "auto"
  },
  card: {
    minHeight: 310,
    "@media only screen and (max-width: 450px)": {
      height: 330,
      fontSize: ".85rem",
      "& .card-body": {
        padding: "0px 5px"
      }
    }
  },
  imgContainer: {
    width: 120,
    height: 180,
    margin: "0 auto",
    position: "relative",
    display: "flex",
    justifyContent: "center",
    marginTop: ".5rem",
    "@media only screen and (max-width: 450px)": {
      width: 100,
      height: 160
    }
  },
  cardImg: {
    maxWidth: 120,
    maxHeight: 180,
    position: "absolute",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)",
    "@media only screen and (max-width: 450px)": {
      maxWidth: 90,
      maxHeight: 150
    }
  },
  avatar: {
    width: 35,
    height: 35,
    borderRadius: "50%",
    "@media only screen and (max-width: 450px)": {
      width: 25,
      height: 25
    }
  }
});

const ReceivedOrdersPage = ({
  result,
  loading,
  listOrders,
  acceptOrder,
  rejectOrder,
  actionLoading
}) => {
  const classes = useStyles();
  const [showRejectModal, setShowRejectModal] = useState(false);
  const [showAcceptModal, setShowAcceptModal] = useState(false);
  const [showNumber, setShowNumber] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [id, setId] = useState(null);

  useEffect(() => {
    listOrders();
  }, [listOrders]);

  const toggleShowNumber = () => {
    setShowNumber(!showNumber);
  };

  const handleOpenRejectModal = () => {
    setShowRejectModal(true);
  };

  const handleCloseRejectModal = () => {
    setShowRejectModal(false);
  };

  const handleOpenAcceptModal = () => {
    setShowAcceptModal(true);
  };

  const handleCloseAcceptModal = () => {
    setShowAcceptModal(false);
  };

  const handleRejectButton = (id) => {
    setId(id);
    handleOpenRejectModal();
  };

  const handleRejectAction = (id, reason) => {
    rejectOrder(id, reason, {
      onSuccess: () => {
        handleCloseRejectModal();
        setId(null);
      }
    });
  };

  const handleAcceptButton = (id) => {
    setId(id);
    handleOpenAcceptModal();
  };

  const handleAcceptAction = (id) => {
    acceptOrder(id, {
      onSuccess: () => {
        handleCloseAcceptModal();
        setId(null);
      }
    });
  };

  let results;
  if (result && result.results) {
    results = result.results.map((item, index) => (
      <Col lg={6} className="px-4" key={index}>
        <Row
          className="bg-white py-3 px-0 px-md-3 shadow-sm"
          style={{ height: "99%" }}
        >
          <Col className="d-flex align-items-center justify-content-center px-4 px-sm-3 px-md-1">
            <Col xs={6} className="px-1 px-sm-2">
              <Card className={classes.card}>
                <div className={classes.imgContainer}>
                  <Link to={`/advertise_details/${item.requested_asset.slug}`}>
                    <Card.Img
                      variant="top"
                      className={classes.cardImg}
                      src={item.requested_asset.thumbnail || noImage}
                      alt="book1"
                    />
                  </Link>
                </div>
                <Card.Body>
                  <div>
                    <Link
                      className="text_dark small"
                      to={`/advertise_details/${item.requested_asset.slug}`}
                    >
                      <h6>{item.requested_asset.title}</h6>
                    </Link>
                  </div>
                </Card.Body>
              </Card>
            </Col>
            <FontAwesomeIcon
              icon={faExchangeAlt}
              size="lg"
              style={{ opacity: ".5" }}
            />
            <Col xs={6} className="px-1 px-sm-2">
              <Card className={classes.card}>
                <div className={classes.imgContainer}>
                  <Link to={`/advertise_details/${item.applicant_asset.slug}`}>
                    <Card.Img
                      variant="top"
                      className={classes.cardImg}
                      src={item.applicant_asset.thumbnail || noImage}
                      alt="book"
                    />
                  </Link>
                </div>
                <Card.Body>
                  <div className="d-flex align-items-center">
                    <Link
                      className="text_dark small"
                      to={`/advertise_details/${item.applicant_asset.slug}`}
                    >
                      <h6> {item.applicant_asset.title}</h6>
                    </Link>
                  </div>

                  <div className="d-flex align-items-center">
                    <Link
                      to={`/other_profiles/${item?.applicant_asset?.owner_id}`}
                    >
                      <img
                        className={classes.avatar}
                        src={
                          item.applicant_asset.owner_avatar
                            ? item.applicant_asset.owner_avatar
                            : avatar
                        }
                        alt="avatar"
                      />
                    </Link>
                    <Link
                      to={`/other_profiles/${item?.applicant_asset?.owner_id}`}
                      className="mr-2 pt-3 small"
                    >
                      {item.applicant_asset.owner_name}
                    </Link>

                    <span className="mr-5 pt-3 small">
                      {item.applicant_asset?.city_name}
                    </span>
                  </div>
                </Card.Body>
              </Card>
            </Col>
          </Col>
          {/* سفارش توسط شما رد شده */}
          {item.accept === false && (
            <Col xs={12} className="mt-4 mb-3 px-md-0">
              <div className="d-flex justify-content-center align-items-center">
                <Alert variant="danger" className="my-0 py-2 w-100 text-center">
                  رد شده توسط شما
                </Alert>
              </div>
            </Col>
          )}

          {/* سفارش توسط شما تایید شده  */}
          {item.accept === true && (
            <Col xs={12} className="mt-4 mb-3 px-md-0">
              <div className="d-flex justify-content-end">
                <Button
                  as={Link}
                  to="/dashboard/messages"
                  variant="outline-secondary"
                  size="sm"
                  className="mx-1"
                >
                  ارسال پیام
                </Button>
                <Button size="sm" className="mx-1" onClick={toggleShowNumber}>
                  <p className="px-1 py-0 my-0">
                    {showNumber
                      ? getPhoneNumber(item.applicant_asset.owner_phone)
                      : "تماس"}
                  </p>
                </Button>
              </div>
            </Col>
          )}

          {/* سفارش منتظر تایید یا رد توسط شماست */}
          {!item.bartered && item.accept === null && (
            <Col xs={12} className="mt-4 mb-3 px-md-0">
              <div className="d-flex justify-content-end">
                <Button
                  variant="outline-secondary"
                  size="sm"
                  className="mx-1"
                  onClick={() => handleRejectButton(item.id)}
                >
                  رد درخواست
                </Button>
                <Button
                  size="sm"
                  className="mx-1"
                  onClick={() => handleAcceptButton(item.id)}
                >
                  تائید درخواست
                </Button>
              </div>
            </Col>
          )}
        </Row>
      </Col>
    ));
  }

  return (
    <DashboardLayout>
      <Container className="my-4">
        <Row>
          <h5 className="text_dark col-12 pb-2 text-center text-md-right pr-2 mb-2">
            سفارشات دریافتی
          </h5>
          {/* no Request message */}
          {!loading && result && result.count === 0 && (
            <Col
              xs={11}
              md={8}
              className={`${classes.noRequestMessage} border rounded`}
            >
              <div>
                <p>شما در حال حاظر هیچ سفارشی دریافت نکرده‌اید.</p>
              </div>
            </Col>
          )}
          {/* /no Request message */}

          {result && !loading ? results : <LoadingBox />}
        </Row>
        {!loading && result && (result.next || result.previous) ? (
          <Pagination
            current={currentPage}
            setCurrent={setCurrentPage}
            count={result.count}
            getData={listOrders}
          />
        ) : null}
      </Container>

      {/* modals: */}
      <RejectRequestModal
        show={showRejectModal}
        handleClose={handleCloseRejectModal}
        loading={actionLoading}
        action={handleRejectAction}
        id={id}
      />

      <AcceptRequestModal
        show={showAcceptModal}
        handleClose={handleCloseAcceptModal}
        loading={actionLoading}
        action={() => handleAcceptAction(id)}
      />
    </DashboardLayout>
  );
};

const mapStateToProps = (state) => {
  return {
    result: state.order.result,
    loading: state.order.loading,
    actionLoading: state.order.actionLoading
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    listOrders: (page) => dispatch(actions.listOrders(page)),
    acceptOrder: (id, callback) => dispatch(actions.acceptOrder(id, callback)),
    rejectOrder: (id, reason, callback) =>
      dispatch(actions.rejectOrder(id, reason, callback))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReceivedOrdersPage);
