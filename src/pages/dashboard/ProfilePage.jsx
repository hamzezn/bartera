import React, { useEffect } from "react";

import { makeStyles } from "@material-ui/styles";
import { Link } from "react-router-dom";

import DashboardLayout from "../../layouts/DashboardLayout";

// redux
import { connect } from "react-redux";
import * as actions from "../../store/actions/index";

// helper
import { getPhoneNumber } from "../../helpers";

// components
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import ListGroup from "react-bootstrap/ListGroup";
import Spinner from "../../components/Spinner";
import LoadingBox from "../../components/LoadingBox";

// images
import avatarMale from "../../img/avatar4.png";
import avatarFemale from "../../img/avatar3.png";
import noImage from "../../img/404.png";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit } from "@fortawesome/free-solid-svg-icons";

const useStyles = makeStyles({
  avatar: {
    width: 103,
    height: 103,
    borderRadius: "50%",
    backgroundColor: "tan"
  },
  editProfileLink: {
    borderTop: "2px solid #b7c9b7"
  },
  advertiseImgContainer: {
    width: 60,
    height: 60,
    position: "relative"
  },
  advertiseImg: {
    maxWidth: "100%",
    maxHeight: "100%",
    position: "absolute",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)"
  }
});

const ProfilePage = ({
  result,
  loading,
  myAssetResult,
  myAssetLoading,
  favResult,
  favLoading,
  statsResult,
  statsLoading,
  getProfileInfo,
  getMyAssets,
  getMyFavorites,
  getBarterStatistics
}) => {
  const classes = useStyles();

  useEffect(() => {
    getProfileInfo();
    getMyAssets();
    getMyFavorites();
    getBarterStatistics();
  }, [getProfileInfo, getMyAssets, getMyFavorites, getBarterStatistics]);

  return (
    <DashboardLayout>
      <Container className="my-4">
        <Row>
          {/* user info section  */}
          <Col lg={6} className="col-12 px-3 user-info">
            <h6 className="mb-2 text-muted text-center text-md-right">
              مشخصات کاربری
            </h6>
            <div className="bg-white border rounded">
              <Row className="justify-content-center my-4">
                {result && !loading ? (
                  <img
                    src={
                      result.avatar
                        ? result.avatar
                        : result.gender === "F"
                        ? avatarFemale
                        : avatarMale
                    }
                    alt={result.name}
                    className={classes.avatar}
                  />
                ) : (
                  <Spinner className={classes.avatar} animation="grow" />
                )}
              </Row>
              <Row className="justify-content-around align-items-center">
                <div>
                  <p className="small mb-2 text_light font-size-smaller">
                    نام و نام خانوادگی:
                  </p>
                  {result && !loading ? (
                    <p className="text_dark font-size-small">{result.name}</p>
                  ) : (
                    <Spinner animation="grow" />
                  )}
                </div>
                <div>
                  <p className="small mb-2 text_light font-size-smaller">
                    شماره همراه:
                  </p>
                  {result && !loading ? (
                    <p className="text_dark font-size-small">
                      {getPhoneNumber(result.phone_number)}
                    </p>
                  ) : (
                    <Spinner animation="grow" />
                  )}
                </div>
                <div>
                  <p className="small mb-2 text_light font-size-smaller">
                    آدرس:
                  </p>
                  {result && !loading ? (
                    <p className="text_dark font-size-small">
                      {result.address || "بدون آدرس"}
                    </p>
                  ) : (
                    <Spinner animation="grow" />
                  )}
                </div>
              </Row>
              {/* edit profile link  */}
              <div
                className={`${classes.editProfileLink} col-12
                bg-white
                small
                text-center
                py-3
                mt-2`}
              >
                <div className="d-flex justify-content-center">
                  <Link to="/dashboard/edit_profile">
                    <FontAwesomeIcon
                      icon={faEdit}
                      size="lg"
                      style={{ marginLeft: 10 }}
                    />
                    ویرایش مشخصات کاربری
                  </Link>
                </div>
              </div>
              {/* /edit profile link */}
            </div>
          </Col>
          {/*  /user info section  */}

          {/* swap info  */}
          <Col lg={6} className="col-12 px-3">
            <h6 className="mb-2 text-muted mt-4 mt-lg-0">آمار معاوضه‌ها</h6>

            <div className="bg-white rounded">
              <ListGroup>
                <ListGroup.Item
                  as={Link}
                  to="/dashboard/myRequests"
                  action
                  className="text_dark"
                  style={{ padding: "25px 20px" }}
                >
                  <div className="d-flex align-items-center">
                    <p className="small mb-0">تعداد درخواست‌ها:</p>
                    {!statsLoading && statsResult ? (
                      <p
                        className="badge mr-3 mb-0"
                        style={{ backgroundColor: "#68BBE3", color: "#fff" }}
                      >
                        {statsResult["Number of my requests"]}
                      </p>
                    ) : (
                      <Spinner animation="grow" />
                    )}
                  </div>
                </ListGroup.Item>

                <ListGroup.Item
                  as={Link}
                  to="/dashboard/received_orders"
                  action
                  className="text_dark"
                  style={{ padding: "25px 20px" }}
                >
                  <div className="d-flex align-items-center">
                    <p className="small mb-0">تعداد سفارشات:</p>
                    {!statsLoading && statsResult ? (
                      <p className="badge badge-secondary mr-3 mb-0">
                        {statsResult["Number of orders received"]}
                      </p>
                    ) : (
                      <Spinner animation="grow" />
                    )}
                  </div>
                </ListGroup.Item>

                <ListGroup.Item
                  as={Link}
                  to="/dashboard/myRequests"
                  action
                  className="text_dark"
                  style={{ padding: "25px 20px" }}
                >
                  <div className="d-flex align-items-center">
                    <p className="small mb-0">معاوضه های در انتطار تایید:</p>
                    {!statsLoading && statsResult ? (
                      <p className="badge badge-secondary mr-3 mb-0">
                        {statsResult["Number of requests pending my approval"] +
                          statsResult[
                            "Number of orders received pending approval"
                          ]}
                      </p>
                    ) : (
                      <Spinner animation="grow" />
                    )}
                  </div>
                </ListGroup.Item>

                <ListGroup.Item
                  action
                  disabled
                  className="text_dark"
                  style={{ padding: "25px 20px" }}
                >
                  <div className="d-flex align-items-center">
                    <p className="small mb-0">تعداد معاوضه ها:</p>
                    {!statsLoading && statsResult ? (
                      <p className="badge badge-secondary mr-3 mb-0">
                        {statsResult["Number of bartered"]}
                      </p>
                    ) : (
                      <Spinner animation="grow" />
                    )}
                  </div>
                </ListGroup.Item>
              </ListGroup>
            </div>
          </Col>
          {/* /swap info  */}

          {/* last advertisements */}
          <Col lg={6} className="col-12 px-3">
            <h6 className="mb-2 mt-4 text-muted">آخرین آگهی‌ها</h6>
            <div className="bg-white border rounded">
              <div className="px-4 py-3">
                {myAssetResult && !myAssetLoading ? (
                  myAssetResult.results.slice(0, 3).map((item, index) => (
                    <React.Fragment key={index}>
                      <div className="d-flex align-items-center">
                        <div className={classes.advertiseImgContainer}>
                          <img
                            className={classes.advertiseImg}
                            src={item.thumbnail || noImage}
                            alt={`advertise-${index + 1}`}
                          />
                        </div>
                        <Link
                          to={`/advertise_details/${item.slug}`}
                          className="text_dark"
                        >
                          <p className="mr-3" style={{ fontSize: ".75rem" }}>
                            {item.title}
                          </p>
                        </Link>
                      </div>
                      {index < 2 && <hr className="my-3" />}
                    </React.Fragment>
                  ))
                ) : (
                  <LoadingBox />
                )}
              </div>
              {/* show and edit advertisement link  */}
              <div
                className={`${classes.editProfileLink} col-12 bg-white small py-3`}
              >
                <div className="d-flex justify-content-center">
                  <Link to="/dashboard/myAdvertisements">
                    <FontAwesomeIcon
                      icon={faEdit}
                      size="lg"
                      style={{ marginLeft: 10 }}
                    />
                    مشاهده و ویرایش آگهی‌ها
                  </Link>
                </div>
              </div>
              {/* /show and edit advertisement link  */}
            </div>
          </Col>
          {/* /last advertisements  */}

          {/* last bookmarks  */}
          <Col lg={6} className="col-12 px-3">
            <h6 className="mb-2 mt-4 text-muted">آخرین علاقه‌مندی‌ها</h6>

            <div className="bg-white border rounded">
              <div className="px-4 py-3">
                {!favLoading && favResult ? (
                  favResult.results.slice(0, 3).map((item, index) => (
                    <React.Fragment key={index}>
                      <div className="d-flex align-items-center">
                        <div className={classes.advertiseImgContainer}>
                          <img
                            className={classes.advertiseImg}
                            src={item.thumbnail || noImage}
                            alt={`fav-${index + 1}`}
                          />
                        </div>
                        <Link
                          to={`/advertise_details/${item.slug}`}
                          className="text_dark"
                        >
                          <p className="mr-3" style={{ fontSize: ".75rem" }}>
                            {item.title}
                          </p>
                        </Link>
                      </div>
                      {index < 2 && <hr className="my-3" />}
                    </React.Fragment>
                  ))
                ) : (
                  <LoadingBox />
                )}
              </div>
              {/* show and edit advertisement link  */}
              <div
                className={`${classes.editProfileLink} col-12 bg-white small py-3`}
              >
                <div className="d-flex justify-content-center">
                  <Link to="/dashboard/bookmarks">
                    <FontAwesomeIcon
                      icon={faEdit}
                      size="lg"
                      style={{ marginLeft: 10 }}
                    />
                    مشاهده و ویرایش علاقه‌مندی‌ها
                  </Link>
                </div>
              </div>
              {/* /show and edit advertisement link  */}
            </div>
          </Col>
          {/* /last bookmarks */}
        </Row>
      </Container>
    </DashboardLayout>
  );
};

const mapStateToProps = (state) => {
  return {
    result: state.account.result,
    loading: state.account.loading,

    myAssetResult: state.myAsset.result,
    myAssetLoading: state.myAsset.loading,

    favResult: state.favorite.result,
    favLoading: state.favorite.loading,

    statsResult: state.statistic.result,
    statsLoading: state.statistic.loading
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getProfileInfo: () => dispatch(actions.getProfileInfo()),
    getMyAssets: () => dispatch(actions.getMyAssets()),
    getMyFavorites: () => dispatch(actions.getMyFavorites()),
    getBarterStatistics: () => dispatch(actions.getBarterStatistics())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
