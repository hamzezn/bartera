import React, { useEffect, useState, useCallback } from "react";

import { makeStyles } from "@material-ui/styles";

import { toast } from "react-toastify";

import DashboardLayout from "../../layouts/DashboardLayout";

// websocket
import useWebSocket from "react-use-websocket";

// image compression before upload
import imageCompression from "browser-image-compression";

// redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// helpers
import { randomNumber, setIntervalX } from "../../helpers";

// actions
import {
  getAllChats,
  getChatRoom,
  reportChatRoom,
  uploadChatImage,
  readMessages
} from "../../store/actions";

// components
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import ListGroup from "react-bootstrap/ListGroup";
import Dropdown from "react-bootstrap/Dropdown";
import Tooltip from "../../components/Tooltip";
import LoadingBox from "../../components/LoadingBox";

// modals
import BlockUserModal from "../../components/Modals/BlockUserModal";
import ChatUploadModal from "../../components/Modals/ChatUploadModal";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEllipsisV,
  faPaperclip,
  faBan
} from "@fortawesome/free-solid-svg-icons";
import { faTelegramPlane } from "@fortawesome/free-brands-svg-icons";

// images
import avatar2 from "../../img/avatar2.png";

const useStyles = makeStyles({
  contactsList: {
    maxHeight: "72vh",
    overflowY: "auto",
    "@media only screen and (max-width: 768px)": {
      marginBottom: 15
    },
    "& img": {
      width: 40,
      height: 40,
      border: "1px solid #555",
      borderRadius: "50%",
      marginLeft: 10
    },
    "& p": { color: "#6c757d !important" }
  },
  chatDate: {
    position: "absolute",
    left: 10,
    top: 5,
    fontSize: ".7rem",
    color: "#858383"
  },
  blockedIcon: {
    color: "#DC143C",
    position: "absolute",
    bottom: 10,
    left: 10
  },
  chatArea: {
    borderBottomLeftRadius: "0 !important",
    borderBottomRightRadius: "0 !important",
    display: "flex",
    flexDirection: "column"
  },
  headImg: {
    width: 40,
    height: 40,
    border: "1px solid #555",
    borderRadius: "50%",
    marginLeft: 10
  },
  dropdown: {
    "& button": {
      background: "#fff",
      borderColor: "#fff",
      "&:hover, &:active, &:focus, &.show": {
        background: "#fff !important",
        borderColor: "#fff !important"
      },
      "&:after": {
        content: "none"
      }
    }
  },
  messages: {
    display: "flex",
    flexDirection: "column-reverse"
  },
  messageBox: {
    width: "100%",
    margin: "5px 0px",
    display: "flex"
  },
  contactMessage: {
    borderRadius: 5,
    fontSize: ".8rem",
    backgroundColor: "#e7e7e7",
    minWidth: 200,
    maxWidth: 400,
    padding: "1rem",
    marginRight: "auto",
    "& img": {
      width: "100%",
      height: "auto"
    },
    "@media only screen and (max-width: 768px)": {
      maxWidth: "80%"
    }
  },
  myMessage: {
    borderRadius: 5,
    fontSize: ".8rem",
    backgroundColor: "#2a852a",
    minWidth: 200,
    maxWidth: 400,
    padding: "1rem",
    marginLeft: "auto",
    color: "white",
    "& img": {
      width: "100%",
      height: "auto"
    },
    "@media only screen and (max-width: 768px)": {
      maxWidth: "80%"
    }
  },
  messageInput: {
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    paddingRight: 80,
    overflowY: "hidden"
  },
  btnFile: {
    position: "absolute",
    top: 5,
    right: 35
  },
  btnSend: {
    position: "absolute",
    right: 0,
    top: 5
  }
});

const compressOptions = {
  maxSizeMB: 1,
  maxWidthOrHeight: 900,
  useWebWorker: true,
  fileType: "image/jpeg",
  onProgress: () => null
};

const MessagesPage = ({
  chat,
  auth,
  image,
  getAllChats,
  getChatRoom,
  reportChatRoom,
  location,
  uploadChatImage,
  readMessages
}) => {
  const classes = useStyles();

  const [currentRoom, setCurrentRoom] = useState();
  const [messages, setMessages] = useState([]);

  const [message, setMessage] = useState("");
  const handleMessageChange = e => {
    setMessage(e.target.value);
  };

  const [openBlockModal, setOpenBlockModal] = useState(false);

  const handleOpenBlockModal = () => {
    setOpenBlockModal(true);
  };
  const handleCloseBlockModal = () => {
    setOpenBlockModal(false);
  };

  const handleNewMessage = res => {
    setMessages(messages => [JSON.parse(res.data), ...messages]);
  };

  const [socketUrl, setSocketUrl] = useState(null);

  const { sendJsonMessage, readyState, getWebSocket } = useWebSocket(
    socketUrl,
    {
      onMessage: handleNewMessage
      // onOpen: () => console.log("opened"),
      // onClose: () => console.log("closed")
    }
  );

  const handleFormSubmit = e => {
    e.preventDefault();
    if (message === "") return;
    if (readyState !== 1) {
      toast.warn("ارتباط با سرور برقرار نیست، لطفا بعدا تلاش کنید");
      return;
    }
    const obj = {
      type: "send_to_websocket",
      message_type: "text",
      message: message
    };

    sendJsonMessage(obj);
    setMessage("");
  };

  const setChatRoom = useCallback(
    room => {
      if (room?.room_id === currentRoom?.room_id) return;
      if (readyState === 1) getWebSocket().close();
      setCurrentRoom(room);
      setSocketUrl(
        `ws://batra.ir:8000/ws/chat/${room?.room_id}/?token=${auth.token}`
      );
      getChatRoom(room.room_id, {
        onSuccess: data => {
          setMessages(data?.results);
        }
      });
      readMessages(room.room_id);
    },
    [currentRoom, auth, getWebSocket, getChatRoom, readyState, readMessages]
  );

  // get all chats list in the begining of render:
  useEffect(() => {
    const { state: { room } = { room: null } } = location;
    getAllChats({
      onSuccess: () => {
        if (room) {
          setChatRoom(room);
          location.state = {};
        }
      }
    });
  }, [getAllChats, setChatRoom, location]);

  const handleReport = id => {
    reportChatRoom(id, {
      onSuccess: data => {
        toast.info(data?.message);
        setCurrentRoom(null);
        getAllChats();
      }
    });
  };

  const handleKeyPress = e => {
    if (e.key === "Enter") handleFormSubmit(e);
  };

  const [openChatUploadModal, setOpenChatUploadModal] = useState(false);

  const handleOpenChatUploadModal = () => setOpenChatUploadModal(true);

  const handleCloseChatUploadModal = () => setOpenChatUploadModal(false);

  const [imageForUpload, setImageForUpload] = useState(null);

  const handleFileChange = event => {
    if (event.target.files[0]) {
      if (readyState !== 1) {
        toast.warn("ارتباط با سرور برقرار نیست، لطفا بعدا تلاش کنید");
        return;
      }
      let imageFile = event.target.files[0];
      setImageForUpload(imageFile);
      handleOpenChatUploadModal();
    }
  };

  const handleFileUpload = image => {
    imageCompression(image, compressOptions)
      .then(file => {
        let data = new FormData();
        data.append("image", file, `pic${randomNumber()}.jpg`);
        uploadChatImage(data, {
          onSuccess: res => {
            handleCloseChatUploadModal();
            const obj = {
              type: "send_to_websocket",
              message_type: "image",
              message: message.length > 0 ? message : "--",
              image_id: res?.id,
              image_url: res?.image
            };
            sendJsonMessage(obj);
            setMessage("");
          }
        });
      })
      .catch(e => {
        console.log(e);
      });
  };

  let dummyDiv = document.getElementById("dummyDiv");

  // const scrollToBottom = () => {};

  useEffect(() => {
    if (dummyDiv)
      setIntervalX(
        () => {
          dummyDiv.scrollIntoView({
            behavior: "smooth",
            block: "end",
            inline: "nearest"
          });
        },
        1000,
        2
      );
  }, [messages, dummyDiv]);

  return (
    <DashboardLayout>
      <Container className="py-4">
        <Row className="rounded py-2">
          {/* chat contancts  */}
          <Col xs={12} lg={4}>
            {/* search contacts  */}
            <Form.Control
              className="my-1"
              id="myContactsInput"
              type="text"
              placeholder="مخاطب خود را جستجو کنید"
            ></Form.Control>
            {/* /search contacts */}

            {/* contacts list  */}
            <ListGroup className={classes.contactsList}>
              {chat?.result?.map((item, index) => {
                return (
                  <ListGroup.Item
                    key={index}
                    action
                    onClick={() => setChatRoom(item)}
                  >
                    <div className="d-flex">
                      <img
                        src={item?.audience_avatar || avatar2}
                        alt="avatar"
                      />
                      <div>
                        <h6 className="small text-dark contactName font-weight-bold">
                          {item?.audience_name}
                        </h6>
                        <p className="small mb-0">
                          {item?.last_message?.length > 25
                            ? item?.last_message?.substring(0, 25) + "..."
                            : item?.last_message}
                        </p>
                      </div>
                      <span className={classes.chatDate}>
                        {item?.last_message_date}
                      </span>
                      {item?.is_blocked && (
                        <Tooltip
                          id={`blocked-${index}`}
                          title="توسط شما بلاک شده است"
                        >
                          <FontAwesomeIcon
                            icon={faBan}
                            size="lg"
                            className={classes.blockedIcon}
                          />
                        </Tooltip>
                      )}
                    </div>
                  </ListGroup.Item>
                );
              }) || <LoadingBox />}
            </ListGroup>
            {/* /contacts list */}
          </Col>
          {/* /chat contancts  */}

          {/* chat box */}
          <Col xs={12} lg={8}>
            {currentRoom ? (
              <>
                <Container
                  className={`${classes.chatArea} bg-white mt-1 border rounded`}
                >
                  {/* header  */}
                  <div className="d-flex justify-content-between align-items-center">
                    <div className="d-flex align-items-center py-2">
                      <img
                        src={currentRoom?.audience_avatar || avatar2}
                        alt="av2"
                        className={classes.headImg}
                      />
                      <h6 className="small text-dark contactName">
                        {currentRoom?.audience_name}
                      </h6>
                    </div>
                    {currentRoom?.is_blocked && (
                      <Tooltip id="blocked-user" title="توسط شما بلاک شده است">
                        <FontAwesomeIcon
                          icon={faBan}
                          size="lg"
                          style={{ color: "#DC143C" }}
                        />
                      </Tooltip>
                    )}

                    {/* drop down */}
                    <Dropdown className={classes.dropdown}>
                      <Dropdown.Toggle className="mr-3 mt-3 mt-md-0 text-muted">
                        <FontAwesomeIcon icon={faEllipsisV} size="lg" />
                      </Dropdown.Toggle>
                      <Dropdown.Menu className="py-0 text-right">
                        <Dropdown.Item
                          className="py-2 small"
                          as="button"
                          onClick={handleOpenBlockModal}
                          disabled={currentRoom?.is_blocked}
                        >
                          بلاک و ریپورت
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>

                    <BlockUserModal
                      show={openBlockModal}
                      handleClose={handleCloseBlockModal}
                      loading={chat.actionLoading}
                      action={() => handleReport(currentRoom?.room_id)}
                    />
                    {/* /drop down  */}
                  </div>
                  {/* /header */}
                  <hr className="my-0" />
                  {/* messages  */}
                  <div style={{ height: "60vh", overflowY: "auto" }}>
                    <div className={classes.messages}>
                      <div
                        id="dummyDiv"
                        style={{ float: "left", clear: "both" }}
                      ></div>

                      {messages?.map((item, index) => {
                        if (item.sender === 1) {
                          if (item.images?.[0])
                            return (
                              <div key={index} className={classes.messageBox}>
                                <div className={classes.myMessage}>
                                  <div>
                                    <img
                                      src={item.images?.[0]?.image}
                                      alt={index}
                                    />
                                  </div>
                                  <p className="pt-1">{item.text}</p>
                                  <span>{item.created}</span>
                                </div>
                              </div>
                            );
                          else
                            return (
                              <div key={index} className={classes.messageBox}>
                                <div className={classes.myMessage}>
                                  <p>{item.text}</p>
                                  <span>{item.created}</span>
                                </div>
                              </div>
                            );
                        } else if (item.sender === 2) {
                          if (item.images?.[0])
                            return (
                              <div key={index} className={classes.messageBox}>
                                <div className={classes.contactMessage}>
                                  <div>
                                    <img
                                      src={item.images?.[0]?.image}
                                      alt={index}
                                    />
                                  </div>
                                  <p className="pt-1">{item.text}</p>
                                  <span>{item.created}</span>
                                </div>
                              </div>
                            );
                          else
                            return (
                              <div key={index} className={classes.messageBox}>
                                <div className={classes.contactMessage}>
                                  <p>{item.text}</p>
                                  <span>{item.created}</span>
                                </div>
                              </div>
                            );
                        }
                      })}
                    </div>
                  </div>
                  {/* /messages */}
                </Container>
                {/* message input form */}
                <form onSubmit={handleFormSubmit}>
                  <Form.Group style={{ position: "relative" }}>
                    <Form.Control
                      as="textarea"
                      rows="2"
                      name="message"
                      value={message}
                      className={classes.messageInput}
                      onChange={handleMessageChange}
                      disabled={currentRoom?.is_blocked}
                      onKeyPress={handleKeyPress}
                    ></Form.Control>
                    <Tooltip id="send-file" title="یک فایل پیوست کنید">
                      <label className={`${classes.btnFile} btn`}>
                        <FontAwesomeIcon icon={faPaperclip} size="lg" />
                        <input
                          type="file"
                          accept="image/*"
                          style={{ display: "none" }}
                          disabled={currentRoom?.is_blocked}
                          onChange={handleFileChange}
                        />
                      </label>
                    </Tooltip>

                    <Tooltip id="send-button" title="ارسال پیام">
                      <button
                        type="submit"
                        className={`${classes.btnSend} btn`}
                      >
                        <FontAwesomeIcon icon={faTelegramPlane} size="lg" />
                      </button>
                    </Tooltip>
                  </Form.Group>
                </form>
                {/* /message input form  */}
              </>
            ) : (
              <Container
                className={`${classes.chatArea} bg-white mt-1 border rounded`}
              >
                <div
                  className="d-flex align-items-center justify-content-center py-4"
                  style={{ minHeight: 250 }}
                >
                  <h2 className="text-dark">لطفا یک چت انتخاب کنید...</h2>
                </div>
              </Container>
            )}
          </Col>
          {/* /chat box */}
        </Row>
      </Container>
      {/* modals */}
      <ChatUploadModal
        show={openChatUploadModal}
        handleClose={handleCloseChatUploadModal}
        imageForUpload={imageForUpload}
        message={message}
        handleMessageChange={handleMessageChange}
        action={() => handleFileUpload(imageForUpload)}
        loading={image?.loading}
      ></ChatUploadModal>
    </DashboardLayout>
  );
};

const mapStateToProps = ({ chat, auth, image }) => ({
  chat,
  auth,
  image
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getAllChats,
      getChatRoom,
      reportChatRoom,
      uploadChatImage,
      readMessages
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(MessagesPage);
