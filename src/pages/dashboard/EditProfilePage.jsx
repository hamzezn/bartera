import React, { useRef, useState, useEffect } from "react";

import { makeStyles } from "@material-ui/styles";

import DashboardLayout from "../../layouts/DashboardLayout";

// image cropper
import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css";

// form validation
import { Formik } from "formik";
import * as Yup from "yup";

// redux
import { connect } from "react-redux";
import * as actions from "../../store/actions/index";

// helpers
import { getPhoneNumber, randomNumber } from "../../helpers";

// components
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import CityChoose from "../../components/CityChoose";
import Radio from "../../components/Radio";
import CropModal from "../../components/Modals/CropModal";
import LoadingBox from "../../components/LoadingBox";
import Spinner from "../../components/Spinner";

// images
import avatarLight from "../../img/avatar-light.png";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faCheck } from "@fortawesome/free-solid-svg-icons";

// validation schema:
const mySchema = Yup.object({
  name: Yup.string()
    .max(30, "نام شما نباید بیشتر از 30 حرف باشد")
    .required("لطفا نام و نام خانوادگی خود را وارد نمایید"),
  address: Yup.string().max(1000, "حداکثر 1000 کاراکتر")
});

const useStyles = makeStyles({
  textInput: {
    height: 45
  },
  city: {
    height: 45,
    marginTop: ".5rem"
  },
  avatarImgCrop: {
    backgroundColor: "#f1f1f1",
    marginBottom: 5,
    marginRight: 5,
    backgroundPosition: "center center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "contain",
    cursor: "pointer",
    width: 180,
    height: 180,
    borderRadius: "50%",
    border: "2px solid #ff4500"
  },
  fileInput: {
    MozOpacity: 0,
    opacity: 0,
    height: "100%",
    width: "100%",
    cursor: "pointer",
    borderRadius: "50%"
  }
});

const EditProfilePage = ({
  result,
  loading,
  actionLoading,
  getProfileInfo,
  sendProfileInfo
}) => {
  useEffect(() => {
    getProfileInfo();
  }, [getProfileInfo]);

  const [img, setImg] = useState(null);
  const [cropResult, setCropResult] = useState(null);
  const [openCropModal, setOpenCropModal] = useState(false);
  const cropper = useRef(null);
  const imgInput = useRef(null);
  const myForm = useRef(null);
  const classes = useStyles();
  const [city, setCity] = useState(null);

  const handleOpenCropModal = () => {
    setOpenCropModal(true);
  };

  const handleCloseCropModal = () => {
    setOpenCropModal(false);
  };

  const cropImage = () => {
    if (typeof cropper.current.getCroppedCanvas() === "undefined") {
      console.log("crop failed");
      return;
    }
    let dataUri = cropper.current.getCroppedCanvas().toDataURL("image/jpeg");
    setCropResult(dataUri);
    imgInput.current.value = null;
    handleCloseCropModal();
  };

  const cancelCrop = () => {
    imgInput.current.value = null;
    setCropResult(null);
    handleCloseCropModal();
  };

  const handleChangeInput = (event) => {
    if (event.target.files[0]) {
      setImg(URL.createObjectURL(event.target.files[0]));
      handleOpenCropModal();
    } else {
      setCropResult(null);
    }
  };

  const handleCityChoose = (city_id) => {
    setCity(city_id);
  };

  const handleFormSubmit = () => {
    let fd = new FormData(myForm.current);
    fd.append("national_code", "0123456789");
    if (city) fd.append("city_id", parseInt(city));
    if (cropResult)
      fd.append(
        "avatar",
        dataURItoBlob(cropResult),
        `avatar${randomNumber()}.jpg`
      );

    sendProfileInfo(fd);
  };

  const dataURItoBlob = (dataURI) => {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(",")[0].indexOf("base64") >= 0)
      byteString = atob(dataURI.split(",")[1]);
    else byteString = unescape(dataURI.split(",")[1]);

    // separate out the mime component
    var mimeString = dataURI.split(",")[0].split(":")[1].split(";")[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], { type: mimeString });
  };

  return (
    <DashboardLayout>
      {!loading && result ? (
        <Container className="py-4">
          <Formik
            validationSchema={mySchema}
            onSubmit={handleFormSubmit}
            initialValues={{
              name: result.name || "",
              address: result.address || "",
              gender: result.gender || ""
            }}
          >
            {({ handleSubmit, handleChange, values, touched, errors }) => (
              <form onSubmit={handleSubmit} ref={myForm}>
                <h5
                  className="text_dark text-center text-md-right"
                  style={{ marginRight: -15 }}
                >
                  ویرایش اطلاعات کاربری
                </h5>
                <Row className="bg-white pt-5 pb-3 px-3 rounded justify-content-md-around justify-content-center border">
                  <Col xs={12} md={5} className="mb-5 mb-md-0">
                    {/* fullName */}
                    <Form.Group controlId="fullName">
                      <Form.Label className="float-right small">
                        نام و نام خانوادگی
                      </Form.Label>
                      <Form.Control
                        className={classes.textInput}
                        type="text"
                        placeholder="نام و نام خانوادگی خود را وارد نمائید"
                        name="name"
                        value={values.name}
                        autoComplete="false"
                        onChange={handleChange}
                        isInvalid={!!errors.name && !!touched.name}
                      ></Form.Control>
                      <Form.Control.Feedback
                        type="invalid"
                        className="text-right"
                      >
                        {errors.name}
                      </Form.Control.Feedback>
                    </Form.Group>
                    {/* /fullName */}
                    {/* phone */}
                    <Form.Group controlId="PhoneNumber" className="mt-4">
                      <Form.Label className="float-right small">
                        شماره همراه
                      </Form.Label>
                      <Form.Control
                        className={classes.textInput}
                        plaintext
                        readOnly
                        defaultValue={getPhoneNumber(result.phone_number)}
                      ></Form.Control>
                    </Form.Group>
                    {/* /phone */}

                    {/* address */}
                    <Form.Group controlId="address">
                      <Form.Label className="float-right small">
                        آدرس
                      </Form.Label>
                      <Form.Control
                        className={classes.textInput}
                        type="text"
                        placeholder="آدرس خود را وارد نمایید"
                        name="address"
                        autoComplete="false"
                        value={values.address}
                        onChange={handleChange}
                        isInvalid={!!errors.address && !!touched.address}
                      ></Form.Control>
                      <Form.Control.Feedback
                        type="invalid"
                        className="text-right"
                      >
                        {errors.address}
                      </Form.Control.Feedback>
                    </Form.Group>
                    {/* address */}

                    {/* province and city choose */}
                    <h6 className="small mt-4">شهر و استان</h6>
                    <CityChoose
                      classes={classes.city}
                      handleCityChoose={handleCityChoose}
                    />
                    {/* /province and city choose */}
                  </Col>
                  <Col
                    xs={12}
                    md={5}
                    className="text-center text-md-right mb-4"
                  >
                    <div className="d-flex justify-content-center">
                      <div
                        className={classes.avatarImgCrop}
                        style={{
                          backgroundImage: cropResult
                            ? `url(${URL.createObjectURL(
                                dataURItoBlob(cropResult)
                              )})`
                            : result.avatar
                            ? `url(${result.avatar})`
                            : `url(${avatarLight})`
                        }}
                      >
                        <input
                          type="file"
                          ref={imgInput}
                          className={classes.fileInput}
                          onChange={handleChangeInput}
                        />
                      </div>
                      {/* modal for crop image */}
                      <CropModal
                        show={openCropModal}
                        handleClose={handleCloseCropModal}
                      >
                        <Cropper
                          ref={cropper}
                          src={img}
                          style={{ height: 400, width: "100%" }}
                          // Cropper.js options
                          aspectRatio={1}
                          guides={false}
                        />
                        <div className="mt-3 d-flex justify-content-around">
                          <Button variant="success" onClick={cropImage}>
                            <FontAwesomeIcon icon={faCheck} size="lg" />
                          </Button>
                          <Button variant="danger" onClick={cancelCrop}>
                            <FontAwesomeIcon icon={faTimes} size="lg" />
                          </Button>
                        </div>
                      </CropModal>
                      {/* /modal for crop image */}
                    </div>
                    <p className="small mt-2 text-center">
                      برای انتخاب تصویر پروفایل روی لوگوی بالا کلیک کنید
                    </p>
                    <h6 className="mt-5 small text-center">جنسیت:</h6>
                    <div className="mr-2 text-center">
                      <Radio
                        id="sexChoose-1"
                        name="gender"
                        value="M"
                        inline
                        checked={values.gender === "M"}
                        onChange={handleChange}
                      >
                        مرد
                      </Radio>
                      <Radio
                        id="sexChoose-2"
                        name="gender"
                        value="F"
                        inline
                        checked={values.gender === "F"}
                        onChange={handleChange}
                      >
                        زن
                      </Radio>
                    </div>
                  </Col>
                  <Col xs={12} className="d-flex justify-content-end mt-5">
                    <Button
                      type="submit"
                      style={{ fontSize: "1rem", minWidth: 200 }}
                      disabled={actionLoading}
                    >
                      اعمال تغییرات
                      {actionLoading && <Spinner />}
                    </Button>
                  </Col>
                </Row>
              </form>
            )}
          </Formik>
        </Container>
      ) : (
        <LoadingBox />
      )}
    </DashboardLayout>
  );
};

const mapStateToProps = (state) => {
  return {
    result: state.account.result,
    loading: state.account.loading,
    actionLoading: state.account.actionLoading
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getProfileInfo: () => dispatch(actions.getProfileInfo()),
    sendProfileInfo: (data) => dispatch(actions.sendProfileInfo(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfilePage);
