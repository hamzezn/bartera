import React, { useState, useEffect } from "react";

import { makeStyles } from "@material-ui/styles";

import { Link } from "react-router-dom";

// redux
import { connect } from "react-redux";
import * as actions from "../../store/actions";

import DashboardLayout from "../../layouts/DashboardLayout";

// components
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Tooltip from "../../components/Tooltip";
import Pagination from "../../components/Pagination";
import LoadingBox from "../../components/LoadingBox";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

// images
import avatar4 from "../../img/avatar4.png";
import noImage from "../../img/404.png";

const useStyles = makeStyles({
  root: {
    justifyContent: "center",
    padding: ".25rem",
    "&.item-enter": {
      opacity: 0
    },
    "&.item-enter-active": {
      opacity: 1,
      transition: "opacity 500ms ease-in"
    },
    "&.item-exit": {
      opacity: 1
    },
    "&.item-exit-active": {
      opacity: 0,
      transition: "opacity 500ms ease-in"
    }
  },
  noAdvertiseMessage: {
    height: 250,
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: "3rem 2rem",
    marginLeft: "auto",
    marginRight: "auto"
  },
  card: {
    maxWidth: 340,
    maxHeight: 160,
    transition: ".2s all",
    margin: "0 auto",
    "&:hover": { border: "1px solid #A5A2A2" },
    "& a": { textDecoration: "none" },
    "@media only screen and (max-width: 768px)": {
      maxWidth: 400,
      maxHeight: 450
    }
  },
  closeBtn: {
    position: "absolute",
    left: 5,
    top: 5,
    zIndex: 1,
    cursor: "pointer",
    fontSize: "2.5rem",
    padding: 7,
    borderRadius: "50%",
    width: 30,
    height: 30,
    "&:hover": { backgroundColor: "rgb(255, 223, 223)" }
  },
  imgContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
    width: 200,
    height: 200,
    "@media only screen and (max-width: 768px)": {
      width: 200,
      height: 230
    },
    "& img": {
      position: "absolute",
      left: "50%",
      top: "40%",
      transform: "translate(-50%, -50%)",
      maxWidth: "80%",
      maxHeight: "50%",
      "@media only screen and (max-width: 768px)": {
        marginTop: 20,
        maxHeight: 230,
        maxWidth: 180
      }
    }
  },
  avatar: {
    width: 40,
    height: 40,
    backgroundColor: "#fcae91",
    borderRadius: "50%",
    padding: 2,
    border: "2px solid #ffffff"
  }
});

const BookmarksPage = ({ result, loading, getMyFavorites, deleteFavorite }) => {
  const classes = useStyles();
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    getMyFavorites();
  }, [getMyFavorites]);

  let results;
  if (!loading && result && result.results) {
    results = result.results.map((item, index) => {
      return (
        <Col xs={12} md={6} lg={4} key={index} className={classes.root}>
          <Card className={classes.card}>
            <Tooltip id="mytool" title="حذف از علاقه‌مندی">
              <FontAwesomeIcon
                icon={faTimes}
                className={classes.closeBtn}
                onClick={() => deleteFavorite(item.slug, currentPage)}
              />
            </Tooltip>
            <Row noGutters>
              <Col xs={12} md={4} className={classes.imgContainer}>
                <Link to={`/advertise_details/${item.slug}`}>
                  <img src={item?.thumbnail || noImage} alt="fav" />
                </Link>
              </Col>
              <Col xs={12} md={8}>
                <Card.Body>
                  <Link
                    to={`/advertise_details/${item.slug}`}
                    className="text_dark"
                  >
                    <h6 className="card-text mt-4">{item.title}</h6>
                  </Link>
                  <div className="d-flex align-items-center mt-3">
                    <Link to={`/other_profiles/${item.owner_id}`}>
                      <img
                        className={classes.avatar}
                        src={item?.owner_avatar || avatar4}
                        alt="avatar"
                      />
                    </Link>

                    <Link
                      to={`/other_profiles/${item.owner_id}`}
                      className="mr-2 pt-3 small"
                    >
                      {item.owner_name}
                    </Link>
                    <p className="mr-2 pt-3 small">{item?.city_name}</p>
                  </div>
                </Card.Body>
              </Col>
            </Row>
          </Card>
        </Col>
      );
    });
  }
  return (
    <DashboardLayout>
      <Container className="my-4">
        <Row className="justify-content-center justify-content-md-start">
          <h5 className="text_dark col-12 pb-2 text-center text-md-right px-0 mb-0">
            علاقه‌مندی‌های شما
          </h5>

          {/* no bookmark message  */}
          {!loading && result && result.results.length === 0 && (
            <Col
              xs={11}
              md={8}
              className={`${classes.noAdvertiseMessage} border rounded`}
            >
              <div>
                <p>شما هنوز به هیچ آگهی علاقه‌مند نیستید!</p>
              </div>
            </Col>
          )}
          {/* /no bookmark message  */}

          {/* items: */}
          {!loading && result ? results : <LoadingBox />}
          {/* /items */}
        </Row>

        {/* Pagination */}
        {!loading && result && (result.next || result.previous) ? (
          <Pagination
            current={currentPage}
            setCurrent={setCurrentPage}
            count={result.count}
            getData={getMyFavorites}
          />
        ) : null}
        {/* /pagination */}
      </Container>
    </DashboardLayout>
  );
};

const mapStateToProps = (state) => {
  return {
    result: state.favorite.result,
    loading: state.favorite.loading
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getMyFavorites: (page) => dispatch(actions.getMyFavorites(page)),
    deleteFavorite: (slug, page) => dispatch(actions.deleteFavorite(slug, page))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BookmarksPage);
