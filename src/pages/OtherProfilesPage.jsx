import React, { useEffect } from "react";

import { makeStyles } from "@material-ui/styles";
import { Link, useParams } from "react-router-dom";

import BaseLayout from "../layouts/BaseLayout";

// redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// actions
import { getOtherProfile } from "../store/actions";

// components
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import LoadingBox from "../components/LoadingBox";

// images
import avatarMale from "../img/avatar4.png";
import avatarFemale from "../img/avatar3.png";
import noImage from "../img/404.png";

const useStyles = makeStyles({
  avatar: {
    height: 150,
    width: 150,
    borderRadius: "50%",
    backgroundColor: "#fcae91"
  },
  imgContainer: {
    width: 120,
    height: 120,
    position: "relative",
    "& img": {
      maxWidth: 120,
      maxHeight: 120,
      position: "absolute",
      left: "50%",
      top: "50%",
      transform: "translate(-50%, -50%)"
    }
  }
});

const OtherProfilesPage = ({ profile, getOtherProfile }) => {
  const classes = useStyles();
  const { id } = useParams();

  useEffect(() => {
    getOtherProfile(id);
  }, [id, getOtherProfile]);

  return (
    <BaseLayout>
      {!profile.loading && profile.result ? (
        <Container className="my-4">
          <Row className="justify-content-center">
            <Col xs={12} lg={12} className="px-0">
              <div className="bg-white border rounded" style={{paddingTop: '30px',}}>
                {/* user avatar image */}
                <Row className="justify-content-center align-items-center">
                  <img
                    className={classes.avatar}
                    src={
                      profile.result.avatar
                        ? profile.result.avatar
                        : profile.result.gender === "F"
                        ? avatarFemale
                        : avatarMale
                    }
                    alt="user-avatar"
                  />
                </Row>
                <div className="text-center px-2">
                  <p className="text_dark mt-4 font-size-small">
                    {profile.result.name}
                  </p>
                </div>
                <hr
                  className="mb-0"
                  style={{ border: "0.5px solid #c2c2c2" }}
                />
                {/* /user avatar image */}
                {/* user info  */}
                <Row className="justify-content-around">
                  <Col
                    style={{ borderLeft: "2px solid #c2c2c2" }}
                    className="text-center px-2 pt-4"
                  >
                    <p
                      style={{ color: "gray" }}
                      className="small mb-2 text_light font-size-smaller"
                    >
                      تعداد معاوضه‌ها:
                    </p>
                    <p className="text_dark font-size-small">
                      {profile.result.bartered_count}
                    </p>
                  </Col>
                  <Col
                    style={{ borderLeft: "2px solid #c2c2c2" }}
                    className="text-center px-2 pt-4"
                  >
                    <p
                      style={{ color: "gray" }}
                      className="small mb-2 text_light font-size-smaller"
                    >
                      تعداد آگهی‌ها:
                    </p>
                    <p className="text_dark font-size-small">
                      {profile.result.asset_count}
                    </p>
                  </Col>
                  <Col className="text-center px-2 pt-4">
                    <p
                      style={{ color: "gray" }}
                      className="small mb-2 text_light font-size-smaller"
                    >
                      آدرس:
                    </p>
                    <p className="text_dark font-size-small">
                      {profile.result.address ? profile.result.address : "--"}
                    </p>
                  </Col>
                </Row>
                {/* /user info */}
              </div>
            </Col>
          </Row>

          {/* user advertisement  */}
          {profile.result.owner_asset?.length > 0 && (
            <Row className="mt-5">
              <Col xs={12}>
                <div className="d-flex justify-content-between align-items-center">
                  <h6 className="text_dark">آگهی‌های {profile.result.name}</h6>
                  {profile.result.owner_asset?.length > 6 && (
                    <Link className="small" to="##">
                      مشاهده همه آگهی‌ها
                    </Link>
                  )}
                </div>
              </Col>
              <Col xs={12}>
                <Row>
                  {/* items */}
                  {profile.result.owner_asset
                    ?.slice(0, 6)
                    ?.map((item, index) => {
                      return (
                        <Col
                          key={index}
                          xs={12}
                          sm={6}
                          md={4}
                          lg={3}
                          xl={2}
                          className="px-1 my-1"
                        >
                          <div
                            className="border rounded bg-white py-3 px-2"
                            style={{ height: 250 }}
                          >
                            <Container className={classes.imgContainer}>
                              <Link
                                to={`/advertise_details/${item.slug}`}
                                className="text-center"
                              >
                                <img
                                  src={
                                    item.thumbnail ? item.thumbnail : noImage
                                  }
                                  className="rounded"
                                  alt={`asset${index}`}
                                />
                              </Link>
                            </Container>
                            <div className="small mt-4">
                              <Link
                                to={`/advertise_details/${item.slug}`}
                                className="text_dark"
                              >
                                <p>{item.title}</p>
                              </Link>
                            </div>
                          </div>
                        </Col>
                      );
                    })}

                  {/* /items */}
                </Row>
              </Col>
            </Row>
          )}
        </Container>
      ) : (
        <LoadingBox />
      )}
    </BaseLayout>
  );
};

const mapStateToProps = ({ profile }) => ({ profile });

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getOtherProfile
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(OtherProfilesPage);
