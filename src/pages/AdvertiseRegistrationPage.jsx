import React, { useState, useRef } from "react";

import { makeStyles } from "@material-ui/styles";
import { Link } from "react-router-dom";

import BaseLayout from "../layouts/BaseLayout";

// image compression before upload
import imageCompression from "browser-image-compression";

// react select
import ReactSelect from "react-select/async-creatable";

// lodash debounce
import debounce from "lodash.debounce";

// toast notif
import { toast } from "react-toastify";

// redux
import { connect } from "react-redux";
import * as actions from "../store/actions";

// form validation
import { Formik } from "formik";

// helpers
import { mapToSelectItems, randomNumber } from "../helpers";

// map component
import GetMap from "../components/MyMap/GetMap";

// components
import CityChoose from "../components/CityChoose";
import Checkbox from "../components/Checkbox";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Spinner from "../components/Spinner";
import ErrorFocus from "../components/ErrorFocus";

// images
import camera from "../img/cameraIcon.png";

// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";

const useStyles = makeStyles({
  select: {
    marginBottom: ".5rem"
  },
  map: {
    width: "90%",
    border: 0,
    height: 400,
    "& > .leaflet-container": {
      height: "100%"
    }
  },
  imageUpload: {
    width: 140,
    height: 140,
    position: "relative",
    borderRadius: 10,
    backgroundColor: "white",
    margin: ".25rem"
  },
  imageUploadSpan: {
    cursor: "pointer",
    display: "inline-block",
    width: 140,
    height: 140,
    position: "relative"
  },
  imageSpinner: {
    width: 140,
    height: 140,
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  fileInput: {
    cursor: "pointer",
    height: "100%",
    width: "100%",
    MozOpacity: 0,
    opacity: 0,
    position: "absolute",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)"
  },
  imagePreview: {
    position: "absolute",
    maxWidth: "100%",
    maxHeight: "100%",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    borderRadius: 10
  },
  cameraIcon: {
    width: 140,
    height: 140
  },
  removeBtn: {
    position: "absolute",
    top: 5,
    right: 5,
    padding: "3px 7px",
    display: "none",
    "&.active": {
      display: "block"
    }
  },
  fieldset: {
    display: "block",
    marginLeft: 2,
    marginRight: 2,
    paddingTop: "0.35em",
    paddingBottom: "0.625em",
    paddingLeft: "0.75em",
    paddingRight: "0.75em",
    border: "1px groove #e7e6e7",
    borderRadius: 5,
    "& legend": {
      maxWidth: 150,
      fontSize: "1rem",
      paddingRight: 5
    }
  },
  cancelBtn: {
    minWidth: 100,
    backgroundColor: "#ffffff",
    padding: "8px 10px",
    fontSize: ".8rem",
    color: "#159e15",
    border: "1px solid #159e15",
    margin: ".25rem"
  },
  submitBtn: {
    minWidth: 180,
    padding: "8px 10px",
    fontSize: ".8rem",
    margin: ".25rem"
  }
});

const compressOptions = {
  maxSizeMB: 0.5,
  maxWidthOrHeight: 700,
  useWebWorker: true,
  fileType: "image/jpeg",
  onProgress: () => null
};

const AdvertiseRegistrationPage = ({
  location,
  loading,
  createAsset,
  getSwapOffers,
  setSwapSearch,
  swapOffers,
  image,
  uploadImage
}) => {
  const classes = useStyles();
  const { category, rootParent } = location.state;
  let schema_schema = category.schema_schema;

  const [selectedSwaps, setSelectedSwaps] = useState([]);

  const handleReactSelectChange = values => {
    setSelectedSwaps(values);
  };

  const searchForTheResult = debounce((query, callback) => {
    setSwapSearch(query);
    getSwapOffers({
      onSuccess: data => {
        callback(mapToSelectItems(data?.results));
      }
    });
  }, 500);

  const loadSwapOptions = (inputValue, callback) => {
    searchForTheResult(inputValue, callback);
  };

  let initialValues = {
    title: "",
    description: ""
  };

  for (let item of schema_schema) {
    for (let field of item.fields) {
      initialValues = { ...initialValues, [field.name]: field.defaultValue };
    }
  }

  const validateForm = values => {
    const errors = {};

    // title
    if (!values.title) errors.title = "نام آگهی را وارد نمائید";
    else if (values.title.length > 200) errors.title = "حداکثر 200 کاراکتر";

    // description
    if (values.description && values.description.length > 3000)
      errors.description = "حداکثر 3000 کاراکتر";

    // dynamic fields from server
    for (let item of schema_schema) {
      for (let field of item.fields) {
        let x = field.name;
        // convert pattern string to regex:
        let pattern = null;
        if (field.pattern) pattern = new RegExp(field.pattern);

        // text and textarea fields
        if (field.type === "text" || field.type === "textarea") {
          if (field.required && !values[x]) errors[x] = field.errors.required;
          else if (values[x] && field.min && values[x].length < field.min)
            errors[x] = field.errors.min;
          else if (values[x] && field.max && values[x].length > field.max)
            errors[x] = field.errors.max;
          else if (values[x] && pattern && !pattern.test(values[x]))
            errors[x] = field.errors.pattern;
        }

        // number fields
        else if (field.type === "number") {
          if (field.required && !values[x]) errors[x] = field.errors.required;
          else if (values[x] && field.min && values[x] < field.min)
            errors[x] = field.errors.min;
          else if (values[x] && field.max && values[x] > field.max)
            errors[x] = field.errors.max;
        }
      }
    }

    return errors;
  };

  const [loc, setLoc] = useState(null);
  const [city, setCity] = useState(null);

  const input1 = useRef(null);
  const input2 = useRef(null);
  const input3 = useRef(null);
  const input4 = useRef(null);

  const [chosenInput, setChosenInput] = useState(0);

  const [file1, setFile1] = useState(null);
  const [file2, setFile2] = useState(null);
  const [file3, setFile3] = useState(null);
  const [file4, setFile4] = useState(null);

  const handleFile1Change = event => {
    setChosenInput(1);
    if (event.target.files[0]) {
      let imageFile = event.target.files[0];
      imageCompression(imageFile, compressOptions)
        .then(file => {
          let data = new FormData();
          data.append("image", file, `pic${randomNumber()}.jpg`);
          uploadImage(data, {
            onSuccess: res => {
              setFile1(res);
            }
          });
        })
        .catch(e => {
          console.log(e);
        });
    } else {
      input1.current.value = null;
      setFile1(null);
    }
  };

  const handleFile2Change = event => {
    setChosenInput(2);
    if (event.target.files[0]) {
      let imageFile = event.target.files[0];
      imageCompression(imageFile, compressOptions)
        .then(file => {
          let data = new FormData();
          data.append("image", file, `pic${randomNumber()}.jpg`);
          uploadImage(data, {
            onSuccess: res => {
              setFile2(res);
            }
          });
        })
        .catch(e => {
          console.log(e);
        });
    } else {
      input2.current.value = null;
      setFile2(null);
    }
  };

  const handleFile3Change = event => {
    setChosenInput(3);
    if (event.target.files[0]) {
      let imageFile = event.target.files[0];
      imageCompression(imageFile, compressOptions)
        .then(file => {
          let data = new FormData();
          data.append("image", file, `pic${randomNumber()}.jpg`);
          uploadImage(data, {
            onSuccess: res => {
              setFile3(res);
            }
          });
        })
        .catch(e => {
          console.log(e);
        });
    } else {
      input3.current.value = null;
      setFile3(null);
    }
  };

  const handleFile4Change = event => {
    setChosenInput(4);
    if (event.target.files[0]) {
      let imageFile = event.target.files[0];
      imageCompression(imageFile, compressOptions)
        .then(file => {
          let data = new FormData();
          data.append("image", file, `pic${randomNumber()}.jpg`);
          uploadImage(data, {
            onSuccess: res => {
              setFile4(res);
            }
          });
        })
        .catch(e => {
          console.log(e);
        });
    } else {
      input4.current.value = null;
      setFile4(null);
    }
  };

  const removeImage1 = () => {
    input1.current.value = null;
    setFile1(null);
  };

  const removeImage2 = () => {
    input2.current.value = null;
    setFile2(null);
  };

  const removeImage3 = () => {
    input3.current.value = null;
    setFile3(null);
  };

  const removeImage4 = () => {
    input4.current.value = null;
    setFile4(null);
  };

  const getPosition = pos => {
    setLoc(pos);
  };

  const handleCityChoose = city_id => {
    setCity(city_id);
  };

  const handleFormSubmit = values => {
    if (!city) {
      toast.warn("لطفا شهر و استان را انتخاب کنید");
      window.scrollTo(0, document.getElementById("city_choose").offsetTop);
      return;
    }

    if (loc) {
      let str = `SRID=4326;POINT (${loc.lng} ${loc.lat})`;
      values = { ...values, coordinates: str };
    }

    // adding city_id & category_id to final data
    values = { ...values, city_id: parseInt(city), category_id: category.id };

    // adding uploaded images to final data
    let images = [];
    if (file4) images.push({ id: file4.id });
    if (file3) images.push({ id: file3.id });
    if (file2) images.push({ id: file2.id });
    if (file1) images.push({ id: file1.id });

    if (images.length > 0) values.images = images;

    // adding 'properties' field to final data
    let extra_fields = [];
    if (schema_schema.length > 0) {
      for (let item of schema_schema) {
        let newItem = [];
        for (let field of item.fields) {
          field.defaultValue = values[field.name];
          const {
            required,
            min,
            max,
            pattern,
            errors,
            placeholder,
            options,
            ...wantedKeys
          } = field;
          newItem.push(wantedKeys);
          extra_fields.push(field.name);
        }
        item.fields = newItem;
      }
      values = { ...values, properties: schema_schema };
    } else values = { ...values, properties: [] };

    // adding swap offers to final data:
    if (selectedSwaps?.length > 0) {
      let swap_offers = [];
      for (let item of selectedSwaps) {
        if (item.__isNew__) swap_offers.push({ title: item.value });
        else swap_offers.push({ slug: item.value });
      }
      values = { ...values, swap_offers };
    }

    // removing extra fields from final data:
    for (let item of extra_fields) delete values[item];

    createAsset(values);
  };

  return (
    <BaseLayout>
      <Container className="my-4">
        {/* header */}
        <div className="d-flex justify-content-between align-items-center border py-3 px-3 bg-white">
          <div className="d-flex align-items-center">
            <h6 className="text-left my-0 mx-2">{rootParent}</h6>
            <span className="text-muted">_</span>
            <h6 className="text-left my-0 mx-2">{category.title}</h6>
          </div>
          <Link to="/choose_category" className="text-success small">
            تغییر دسته‌بندی
          </Link>
        </div>
        {/* /header  */}

        {/* advertise Registration form */}
        <Formik
          validate={validateForm}
          onSubmit={handleFormSubmit}
          initialValues={initialValues}
        >
          {({ handleSubmit, handleChange, values, errors }) => (
            <Form onSubmit={handleSubmit}>
              <Row className="bg-white m-0 border pt-5 pb-4">
                <Col className="col-12 pr-md-4" md={5}>
                  {/* advertise Title */}
                  <Form.Group controlId="title">
                    <Form.Label className="small">عنوان آگهی *</Form.Label>
                    <Form.Control
                      placeholder="نام دارایی خود را بنویسید"
                      name="title"
                      value={values.title}
                      onChange={handleChange}
                      isInvalid={!!errors.title}
                    ></Form.Control>
                    <Form.Control.Feedback
                      type="invalid"
                      className="text-right"
                    >
                      {errors.title}
                    </Form.Control.Feedback>
                  </Form.Group>
                  {/* /advertise Title */}

                  {/* province and city choose */}
                  <label id="city_choose" className="ml-sm-2 small">
                    انتخاب استان و شهر *
                  </label>
                  <CityChoose
                    classes={classes.select}
                    handleCityChoose={handleCityChoose}
                  />
                  {/* /province and city choose  */}

                  {/* dynamic fields */}
                  {schema_schema &&
                    schema_schema.map(item => {
                      return (
                        <React.Fragment key={item.id}>
                          <h6 className="mt-4">{item.title}</h6>
                          {item.fields.map(field => {
                            if (field.type === "checkbox") {
                              return (
                                <Checkbox
                                  key={field.id}
                                  id={field.name}
                                  name={field.name}
                                  checked={values[field.name]}
                                  onChange={handleChange}
                                >
                                  {field.title}
                                </Checkbox>
                              );
                            }
                            if (field.type === "textarea") {
                              return (
                                <Form.Group
                                  key={field.id}
                                  controlId={field.name}
                                >
                                  <Form.Label>
                                    {field.title}
                                    {field.required && "*"}
                                  </Form.Label>
                                  <Form.Control
                                    as="textarea"
                                    rows="4"
                                    name={field.name}
                                    value={values[field.name]}
                                    onChange={handleChange}
                                    isInvalid={!!errors[field.name]}
                                  ></Form.Control>
                                  <Form.Control.Feedback
                                    type="invalid"
                                    className="text-right"
                                  >
                                    {errors[field.name]}
                                  </Form.Control.Feedback>
                                </Form.Group>
                              );
                            }
                            if (field.type === "dropdown") {
                              return (
                                <Form.Group
                                  key={field.id}
                                  controlId={field.name}
                                >
                                  <Form.Label className="small">
                                    {field.title}
                                    {field.required && "*"}
                                  </Form.Label>
                                  <Form.Control
                                    as="select"
                                    custom={true ? 1 : 0}
                                    name={field.name}
                                    value={values[field.name]}
                                    onChange={handleChange}
                                  >
                                    {field.options?.map((option, i) => (
                                      <option key={i} value={option}>
                                        {option}
                                      </option>
                                    ))}
                                  </Form.Control>
                                </Form.Group>
                              );
                            }
                            return (
                              <Form.Group key={field.id} controlId={field.name}>
                                <Form.Label className="small">
                                  {field.title}
                                  {field.required && "*"}
                                </Form.Label>
                                <Form.Control
                                  type={field.type}
                                  placeholder={field.placeholder}
                                  name={field.name}
                                  value={values[field.name]}
                                  onChange={handleChange}
                                  isInvalid={!!errors[field.name]}
                                ></Form.Control>
                                <Form.Control.Feedback
                                  type="invalid"
                                  className="text-right"
                                >
                                  {errors[field.name]}
                                </Form.Control.Feedback>
                              </Form.Group>
                            );
                          })}
                        </React.Fragment>
                      );
                    })}
                  {/* /dynamic fields */}

                  {/* Extra Description  */}
                  <Form.Group
                    controlId="extraDescriptionInput"
                    className="mt-4"
                  >
                    <Form.Label>توضیحات تکمیلی</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows="4"
                      name="description"
                      value={values.description}
                      onChange={handleChange}
                      isInvalid={!!errors.description}
                    ></Form.Control>
                    <Form.Control.Feedback
                      type="invalid"
                      className="text-right"
                    >
                      {errors.description}
                    </Form.Control.Feedback>
                  </Form.Group>
                  {/* /Extra Description  */}
                </Col>
                <Col className="col-12 px-4" md={7}>
                  {/* location */}
                  <h6 className="small mb-2 mt-2">انتخاب لوکیشن</h6>
                  <h6 className="small mb-2 mt-2">
                    با کلیک روی نقشه مکان موردنظر خود را انتخاب کنید
                  </h6>
                  <div className={classes.map}>
                    <GetMap
                      center={[33.483721, 48.353323]}
                      zoom={13}
                      getPos={getPosition}
                    />
                  </div>
                  {/* /location  */}

                  {/* upload advertise images  */}
                  <h6 className="small mb-3 mt-4">بارگذاری تصویر آگهی</h6>
                  <Row className="justify-content-center justify-content-sm-start px-3">
                    <div className={classes.imageUpload}>
                      {chosenInput === 1 && image.loading ? (
                        <div className={classes.imageSpinner}>
                          <Spinner animation="grow" variant="primary" />
                        </div>
                      ) : (
                        <>
                          <span className={classes.imageUploadSpan}>
                            <input
                              ref={input1}
                              type="file"
                              accept="image/*"
                              className={classes.fileInput}
                              onChange={handleFile1Change}
                            />
                            <img
                              src={camera}
                              className={classes.cameraIcon}
                              alt="camera-icon"
                            />
                          </span>
                          <img
                            src={file1 ? file1.image : null}
                            className={classes.imagePreview}
                            alt=""
                          />
                          <Button
                            variant="danger"
                            className={`${classes.removeBtn} ${
                              file1 && "active"
                            }`}
                            onClick={removeImage1}
                          >
                            <FontAwesomeIcon icon={faTrashAlt} />
                          </Button>
                        </>
                      )}
                    </div>
                    <div className={classes.imageUpload}>
                      {chosenInput === 2 && image.loading ? (
                        <div className={classes.imageSpinner}>
                          <Spinner animation="grow" variant="primary" />
                        </div>
                      ) : (
                        <>
                          <span className={classes.imageUploadSpan}>
                            <input
                              ref={input2}
                              type="file"
                              accept="image/*"
                              className={classes.fileInput}
                              onChange={handleFile2Change}
                            />
                            <img
                              src={camera}
                              className={classes.cameraIcon}
                              alt="camera-icon"
                            />
                          </span>
                          <img
                            src={file2 ? file2.image : null}
                            className={classes.imagePreview}
                            alt=""
                          />
                          <Button
                            variant="danger"
                            className={`${classes.removeBtn} ${
                              file2 && "active"
                            }`}
                            onClick={removeImage2}
                          >
                            <FontAwesomeIcon icon={faTrashAlt} />
                          </Button>
                        </>
                      )}
                    </div>
                    <div className={classes.imageUpload}>
                      {chosenInput === 3 && image.loading ? (
                        <div className={classes.imageSpinner}>
                          <Spinner animation="grow" variant="primary" />
                        </div>
                      ) : (
                        <>
                          <span className={classes.imageUploadSpan}>
                            <input
                              ref={input3}
                              type="file"
                              accept="image/*"
                              className={classes.fileInput}
                              onChange={handleFile3Change}
                            />
                            <img
                              src={camera}
                              className={classes.cameraIcon}
                              alt="camera-icon"
                            />
                          </span>
                          <img
                            src={file3 ? file3.image : null}
                            className={classes.imagePreview}
                            alt=""
                          />
                          <Button
                            variant="danger"
                            className={`${classes.removeBtn} ${
                              file3 && "active"
                            }`}
                            onClick={removeImage3}
                          >
                            <FontAwesomeIcon icon={faTrashAlt} />
                          </Button>
                        </>
                      )}
                    </div>
                    <div className={classes.imageUpload}>
                      {chosenInput === 4 && image.loading ? (
                        <div className={classes.imageSpinner}>
                          <Spinner animation="grow" variant="primary" />
                        </div>
                      ) : (
                        <>
                          <span className={classes.imageUploadSpan}>
                            <input
                              ref={input4}
                              type="file"
                              accept="image/*"
                              className={classes.fileInput}
                              onChange={handleFile4Change}
                            />
                            <img
                              src={camera}
                              className={classes.cameraIcon}
                              alt="camera-icon"
                            />
                          </span>
                          <img
                            src={file4 ? file4.image : null}
                            className={classes.imagePreview}
                            alt=""
                          />
                          <Button
                            variant="danger"
                            className={`${classes.removeBtn} ${
                              file4 && "active"
                            }`}
                            onClick={removeImage4}
                          >
                            <FontAwesomeIcon icon={faTrashAlt} />
                          </Button>
                        </>
                      )}
                    </div>
                  </Row>
                  {/* I wanna swap to  */}
                  <fieldset className={`${classes.fieldset} my-5 px-4 py-3`}>
                    <legend>مایلم معاوضه کنم با</legend>
                    <ReactSelect
                      cacheOptions
                      defaultOptions
                      loadOptions={loadSwapOptions}
                      isMulti
                      value={selectedSwaps}
                      onChange={handleReactSelectChange}
                      isLoading={swapOffers.loading}
                      loadingMessage={() => "صبر کنید..."}
                      formatCreateLabel={i => `ساختن گزینه جدید: "${i}"`}
                      placeholder="دارایی موردنظر را انتخاب و یا به گزینه های موجود اضافه کنید..."
                    />
                  </fieldset>
                  {/* /I wanna swap to  */}
                </Col>

                <Row className="col-12 justify-content-end mt-4">
                  <Button
                    as={Link}
                    to="/"
                    className={`${classes.cancelBtn} ${loading && "disabled"}`}
                  >
                    انصراف
                  </Button>
                  <Button
                    type="submit"
                    className={classes.submitBtn}
                    disabled={loading || image?.loading}
                  >
                    ثبت آگهی
                    {loading && <Spinner />}
                  </Button>
                </Row>
              </Row>
              <ErrorFocus />
            </Form>
          )}
        </Formik>
      </Container>
    </BaseLayout>
  );
};

const mapStateToProps = state => {
  return {
    loading: state.asset.createLoading,
    swapOffers: state.swapOffers,
    image: state.image
  };
};

const mapDispatchToProps = dispatch => {
  return {
    createAsset: data => dispatch(actions.createAsset(data)),
    getSwapOffers: callback => dispatch(actions.getSwapOffers(callback)),
    setSwapSearch: search => dispatch(actions.setSwapSearch(search)),
    uploadImage: (data, callback) =>
      dispatch(actions.uploadImage(data, callback))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdvertiseRegistrationPage);
