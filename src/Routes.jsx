import React from "react";

import { Switch, Route, Router } from "react-router-dom";

// pages
import HomePage from "./pages/HomePage";
import ManualPage from "./pages/statics/ManualPage";
import FAQPage from "./pages/statics/FAQPage";
import LoginRegisterPage from "./pages/LoginRegisterPage";
import ChooseCategoryPage from "./pages/ChooseCategoryPage";
import ChooseSubCategoryPage from "./pages/ChooseSubCategoryPage";
import AdvertiseRegistrationPage from "./pages/AdvertiseRegistrationPage";
import AdvertiseDetailsPage from "./pages/AdvertiseDetailsPage";
import ProfilePage from "./pages/dashboard/ProfilePage";
import VerifyPhonePage from "./pages/VerifyPhonePage";
import MyAdvertisementsPage from "./pages/dashboard/MyAdvertisementsPage";
import MyRequestsPage from "./pages/dashboard/MyRequestsPage";
import ReceivedOrdersPage from "./pages/dashboard/ReceivedOrdersPage";
import NotFoundPage from "./pages/statics/NotFoundPage";
import AboutUsPage from "./pages/statics/AboutUsPage";
import ContactUsPage from "./pages/statics/ContactUsPage";
import PrivacyPage from "./pages/statics/PrivacyPage";
import SafeSwapPage from "./pages/statics/SafeSwapPage";
import TermsPage from "./pages/statics/TermsPage";
import OtherProfilesPage from "./pages/OtherProfilesPage";
import BookmarksPage from "./pages/dashboard/BookmarksPage";
import EditProfilePage from "./pages/dashboard/EditProfilePage";
import MessagesPage from "./pages/dashboard/MessagesPage";
import AdvertiseEditPage from "./pages/AdvertiseEditPage";
import BarterStoryPage from "./pages/statics/BarterStoryPage";

// middlewares
import PrivateRoute from "./components/shared/PrivateRoute";

import TestPage from "./pages/TestPage";

// history
import history from "./shared/history";

// Logout
import Logout from "./components/shared/Logout";

// ScrollToTop on route change
import ScrollToTop from "./components/ScrollToTop";

export default function Routes() {
  return (
    <Router history={history} basename={process.env.PUBLIC_URL}>
      <ScrollToTop />
      <Switch>
        <Route path="/logout" component={Logout} />
        <Route exact path="/" component={HomePage} />
        <Route exact path="/login_register" component={LoginRegisterPage} />
        <Route exact path="/verify_phone" component={VerifyPhonePage} />
        <Route exact path="/other_profiles/:id" component={OtherProfilesPage} />

        <Route exact path="/test" component={TestPage} />

        <Route
          exact
          path="/advertise_details/:slug"
          component={AdvertiseDetailsPage}
        />

        <PrivateRoute
          exact
          path="/choose_category"
          component={ChooseCategoryPage}
        />
        <PrivateRoute
          exact
          path="/choose_subcategory/:id"
          component={ChooseSubCategoryPage}
        />
        <PrivateRoute
          exact
          path="/advertise_registration"
          component={AdvertiseRegistrationPage}
        />
        <PrivateRoute
          exact
          path="/advertise_edit/:slug"
          component={AdvertiseEditPage}
        />

        {/* dashboard routes */}
        <PrivateRoute path="/dashboard/profile" component={ProfilePage} />

        <PrivateRoute
          exact
          path="/dashboard/myAdvertisements"
          component={MyAdvertisementsPage}
        />
        <PrivateRoute
          exact
          path="/dashboard/myRequests"
          component={MyRequestsPage}
        />
        <PrivateRoute
          exact
          path="/dashboard/received_orders"
          component={ReceivedOrdersPage}
        />
        <PrivateRoute
          exact
          path="/dashboard/bookmarks"
          component={BookmarksPage}
        />
        <PrivateRoute
          exact
          path="/dashboard/edit_profile"
          component={EditProfilePage}
        />
        <PrivateRoute
          exact
          path="/dashboard/messages"
          component={MessagesPage}
        />
        {/* end dashboard routes */}

        {/* static pages: */}
        <Route exact path="/manual" component={ManualPage} />
        <Route exact path="/FAQ" component={FAQPage} />
        <Route exact path="/about_us" component={AboutUsPage} />
        <Route exact path="/contact_us" component={ContactUsPage} />
        <Route exact path="/privacy" component={PrivacyPage} />
        <Route exact path="/safe_swap" component={SafeSwapPage} />
        <Route exact path="/terms_and_conditions" component={TermsPage} />
        <Route exact path="/barter_story" component={BarterStoryPage} />
        <Route component={NotFoundPage} />
        {/* end static pages */}
      </Switch>
    </Router>
  );
}
