import React from "react";

import { makeStyles } from "@material-ui/styles";
import { Provider } from "react-redux";

import store from "./store/store";

//themes
import "./themes/custom_bootstrap.scss";
import "./themes/bootstrap-rtl.css";

// routes
import Routes from "./Routes";

// toastify
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

toast.configure({
  position: "top-left",
  autoClose: 3000,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  rtl: true
});

// global styles
const useStyles = makeStyles({
  "@global": {
    html: {
      scrollBehavior: "smooth"
    },
    /* Chrome, Firefox, Opera, Safari 10.1+ */
    "::placeholder": {
      color: "#c2c2c2 !important",
      fontSize: ".9rem"
    },
    /* Internet Explorer 10-11 */
    ":-ms-input-placeholder": {
      color: "#c2c2c2 !important"
    },
    /* Microsoft Edge */
    "::-ms-input-placeholder": {
      color: "#c2c2c2 !important"
    },
    /* Chrome/Opera/Safari */
    "::-webkit-input-placeholder": {
      color: "#c2c2c2 !important"
    },
    /* Firefox 18- */
    ":-moz-placeholder": {
      color: "#c2c2c2 !important"
    },
    /* Firefox 19+ */
    "::-moz-placeholder": {
      color: "#c2c2c2 !important"
    },
    /* some additional clasess */
    ".text_light": {
      color: "#c2c2c2"
    },
    ".list_style_none": {
      listStyle: "none"
    },
    ".text_dark": {
      color: "#616161"
    },
    ".red_orange": {
      color: "#ff4500"
    },
    ".bg_red_orange": {
      backgroundColor: "#ff4500"
    },
    ".bg_green": {
      backgroundColor: "#159e15"
    },
    ".btn_outline_orange": {
      backgroundColor: "#ffffff",
      padding: "8px 10px",
      fontSize: ".8rem",
      color: "#ff4500",
      border: "1px solid #ff4500"
    },
    ".btn_outline_orange_small": {
      backgroundColor: "#ffffff",
      padding: "8px 10px",
      fontSize: ".6rem",
      color: "#ff4500",
      border: "1px solid #ff4500"
    },
    ".btn_outline_green": {
      backgroundColor: "#ffffff",
      padding: "8px 10px",
      fontSize: ".8rem",
      color: "#159e15",
      border: "1px solid #159e15"
    },
    ".btn_outline_green_small": {
      backgroundColor: "#ffffff",
      padding: "8px 10px",
      fontSize: ".6rem",
      color: "#159e15",
      border: "1px solid #159e15"
    },
    ".btn_green": {
      backgroundColor: "#159e15",
      padding: "8px 10px",
      fontSize: ".8rem",
      color: "#ffffff",
      "&:hover": {
        backgroundColor: "#007600",
        color: "#ffffff"
      }
    },
    ".tag_btn": {
      backgroundColor: "#ffffff",
      padding: "5px 8px",
      fontSize: ".7rem",
      color: "#858383",
      border: "1px solid #858383",
      "&:hover": {
        backgroundColor: "#ffffff",
        padding: "5px 8px",
        fontSize: ".7rem",
        color: "#159e15",
        border: "1px solid #159e15"
      }
    },
    ".Toastify__toast": {
      fontFamily: "IRANSans"
    },
    "@media only screen and (max-width: 450px)": {
      ".font-size-smaller": {
        fontSize: ".7rem",
        padding: "0 15"
      },
      ".font-size-small": {
        fontSize: ".8rem",
        padding: "0 15"
      }
    }
  }
});

function App() {
  useStyles();
  return (
    <Provider store={store}>
      <Routes />
    </Provider>
  );
}

export default App;
