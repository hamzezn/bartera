import axios from "axios";

import { toast } from "react-toastify";

// redux
import store from "../store/store";
import { setToken, logout } from "../store/actions/index";

// history
import history from "./history";

const instance = axios.create({
  baseURL: "http://batra.ir/api/v1/",
  timeout: 20000
});

instance.interceptors.request.use((request) => {
  const token = store.getState().auth.token;
  if (token) {
    request.headers.Authorization = "Bearer " + token;
  }
  return request;
});

instance.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response.status === 403) {
      const refreshToken = store.getState().auth.refreshToken;
      if (refreshToken) {
        return new Promise((resolve, reject) => {
          instance
            .post("/accounts/verification/token_refresh/", {
              refresh: refreshToken
            })
            .then((response) => {
              const request = error.config;
              const token = response.data.access;
              localStorage.setItem("token", token);
              store.dispatch(setToken(token));
              request.headers.Authorization =
                "Bearer " + store.getState().auth.token;
              resolve(axios(request));
            })
            .catch((error) => {
              store.dispatch(logout());
              history.push("/login_register");
            });
        });
      }
    } else if (error.response.status === 401) {
      store.dispatch(logout());
      toast.warn("ابتدا باید وارد سیستم شوید");
      history.push("/login_register");
    }
    return Promise.reject(error);
  }
);

export default instance;
